<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\EmailVerificationController;
use App\Http\Controllers\API\EmailVerificationNotificationController;
use App\Http\Controllers\API\EmailVerificationPromptController;
use App\Http\Controllers\API\PasswordResetLinkController;
use App\Http\Controllers\API\NewPasswordController;
use App\Http\Controllers\API\SubdomainDashboardController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\RoleController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\StageController;
use App\Http\Controllers\API\ContactController;
use App\Http\Controllers\API\LeadController;
use App\Http\Controllers\API\LeadTaskController;
use App\Http\Controllers\API\LeadEventController;
use App\Http\Controllers\API\LeadNoteController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::domain('{subdomain_name}.localhost.com')->middleware('tenant')->group(function ($router) {
    //without login routye
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('forgot-password', [PasswordResetLinkController::class, 'store']);
    Route::post('reset-password', [NewPasswordController::class, 'store'])->name('api.password.reset');
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth-api:api');
    Route::post('email/verification-notification', [EmailVerificationNotificationController::class, 'sendVerificationEmail'])->middleware(['auth-api:api','throttle:6,1'])->name('verification.send');
    Route::get('verify-email/{id}/{hash}', [EmailVerificationController::class, 'verify'])->name('api.verification.verify');
    Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->name('api.verification.notice');

    
    /////////After Login and Verified User Route////////////
    Route::middleware(['auth-api:api','subdomain.api.tenantverified'])->group(function(){
        
        Route::get('dashboard', [SubdomainDashboardController::class,'index'])->name('dashboard');

        //Write Route for User
        Route::get('user', [UserController::class,'index'])->name('user')->middleware('permission:user_list');
        Route::get('user/create', [UserController::class,'create'])->name('user-create')->middleware('permission:user_create');
        Route::post('user/store', [UserController::class,'store'])->name('user-store')->middleware('permission:user_create');
        Route::get('user/edit/{id}', [UserController::class,'edit'])->name('user-edit')->middleware('permission:user_edit');
        Route::get('user/view/{id}', [UserController::class,'view'])->name('user-view')->middleware('permission:user_view');
        Route::put('user/mode', [UserController::class,'changeStatus'])->name('user-mode')->middleware('permission:user_edit');
        Route::post('user/update', [UserController::class,'update'])->name('user-update')->middleware('permission:user_edit');
        Route::delete('user/destroy', [UserController::class,'delete'])->name('user-destroy')->middleware('permission:user_delete');
        Route::get('user/show/{id}', [UserController::class,'show'])->name('user-show')->middleware('permission:user_view');
        Route::get('profile', [UserController::class,'profile'])->name('profile');
        Route::post('update-profile', [UserController::class,'updateProfile'])->name('update-profile');
        Route::post('change-password', [UserController::class,'changePassword'])->name('change-password');
        Route::post('change-site-data', [UserController::class,'changeSiteData'])->name('change-site-data');
        Route::get('user/getPermission', [UserController::class,'getPermission'])->name('user-get-permission');


        //Write Route for Role
        Route::get('role', [RoleController::class,'index'])->name('role')->middleware('permission:role_list');
        Route::get('role/create', [RoleController::class,'create'])->name('role-create')->middleware('permission:role_create');
        Route::post('role/store', [RoleController::class,'store'])->name('role-store')->middleware('permission:role_create');
        Route::get('role/edit/{id}', [RoleController::class,'edit'])->name('role-edit')->middleware('permission:role_edit');
        Route::get('role/view/{id}', [RoleController::class,'view'])->name('role-view')->middleware('permission:role_view');
        Route::put('role/mode', [RoleController::class,'changeStatus'])->name('role-mode')->middleware('permission:role_edit');
        Route::post('role/update', [RoleController::class,'update'])->name('role-update')->middleware('permission:role_edit');
        Route::delete('role/destroy', [RoleController::class,'delete'])->name('role-destroy')->middleware('permission:role_delete');

        //Write Route for Product
        Route::get('product', [ProductController::class,'index'])->name('product')->middleware('permission:product_list');
        Route::get('product/create', [ProductController::class,'create'])->name('product-create')->middleware('permission:product_create');
        Route::post('product/store', [ProductController::class,'store'])->name('product-store')->middleware('permission:product_create');
        Route::get('product/edit/{id}', [ProductController::class,'edit'])->name('product-edit')->middleware('permission:product_edit');
        Route::post('product/update', [ProductController::class,'update'])->name('product-update')->middleware('permission:product_edit');
        Route::get('product/view/{id}', [ProductController::class,'view'])->name('product-view')->middleware('permission:product_view');
        Route::put('product/mode', [ProductController::class,'changeStatus'])->name('product-mode')->middleware('permission:product_edit');
        Route::delete('product/destroy', [ProductController::class,'delete'])->name('product-destroy')->middleware('permission:product_delete');


        //Write Route for Stage
        Route::get('stage', [StageController::class,'index'])->name('stage')->middleware('permission:stage_list');
        Route::get('stage/create', [StageController::class,'create'])->name('stage-create')->middleware('permission:stage_create');
        Route::post('stage/store', [StageController::class,'store'])->name('stage-store')->middleware('permission:stage_create');
        Route::get('stage/edit/{id}', [StageController::class,'edit'])->name('stage-edit')->middleware('permission:stage_edit');
        Route::post('stage/update', [StageController::class,'update'])->name('stage-update')->middleware('permission:stage_edit');
        Route::get('stage/view/{id}', [StageController::class,'view'])->name('stage-view')->middleware('permission:stage_view');
        Route::put('stage/mode', [StageController::class,'changeStatus'])->name('stage-mode')->middleware('permission:stage_edit');
        Route::delete('stage/destroy', [StageController::class,'delete'])->name('stage-destroy')->middleware('permission:stage_delete');


        //Write Route for Contact
        Route::get('contact', [ContactController::class,'index'])->name('contact')->middleware('permission:contact_list');
        Route::get('contact/create', [ContactController::class,'create'])->name('contact-create')->middleware('permission:contact_create');
        Route::post('contact/store', [ContactController::class,'store'])->name('contact-store')->middleware('permission:contact_create');
        Route::get('contact/edit/{id}', [ContactController::class,'edit'])->name('contact-edit')->middleware('permission:contact_edit');
        Route::post('contact/update', [ContactController::class,'update'])->name('contact-update')->middleware('permission:contact_edit');
        Route::get('contact/view/{id}', [ContactController::class,'view'])->name('contact-view')->middleware('permission:contact_view');
        Route::put('contact/mode', [ContactController::class,'changeStatus'])->name('contact-mode')->middleware('permission:contact_edit');
        Route::delete('contact/destroy', [ContactController::class,'delete'])->name('contact-destroy')->middleware('permission:contact_delete');

        
        //Write Route for Lead
        Route::get('lead', [LeadController::class,'index'])->name('lead')->middleware('permission:lead_list');
        Route::get('lead/create', [LeadController::class,'create'])->name('lead-create')->middleware('permission:lead_create');
        Route::post('lead/store', [LeadController::class,'store'])->name('lead-store')->middleware('permission:lead_create');
        Route::get('lead/edit/{id}', [LeadController::class,'edit'])->name('lead-edit')->middleware('permission:lead_edit');
        Route::post('lead/update', [LeadController::class,'update'])->name('lead-update')->middleware('permission:lead_edit');
        Route::get('lead/view/{id}', [LeadController::class,'view'])->name('lead-view')->middleware('permission:lead_view');
        Route::put('lead/mode', [LeadController::class,'changeStatus'])->name('lead-mode')->middleware('permission:lead_edit');
        Route::delete('lead/destroy', [LeadController::class,'delete'])->name('lead-destroy')->middleware('permission:lead_delete');


        //Write Route for Lead Task
        Route::get('lead-task', [LeadTaskController::class,'index'])->middleware('checkLeadId')->name('lead-task');
        Route::get('lead-task/create', [LeadTaskController::class,'create'])->middleware('checkLeadId')->name('lead-task-create');
        Route::post('lead-task/store', [LeadTaskController::class,'store'])->middleware('checkLeadId')->name('lead-task-store');
        Route::get('lead-task/edit/{id}', [LeadTaskController::class,'edit'])->middleware('checkLeadId')->name('lead-task-edit');
        Route::post('lead-task/update', [LeadTaskController::class,'update'])->middleware('checkLeadId')->name('lead-task-update');
        Route::get('lead-task/view/{id}', [LeadTaskController::class,'view'])->middleware('checkLeadId')->name('lead-task-view');
        Route::put('lead-task/mode', [LeadTaskController::class,'changeStatus'])->middleware('checkLeadId')->name('lead-task-mode');
        Route::delete('lead-task/destroy', [LeadTaskController::class,'delete'])->middleware('checkLeadId')->name('lead-task-destroy');

        //Write Route for Lead Event
        Route::get('lead-event', [LeadEventController::class,'index'])->middleware('checkLeadId')->name('lead-event');
        Route::get('lead-event/create', [LeadEventController::class,'create'])->middleware('checkLeadId')->name('lead-event-create');
        Route::post('lead-event/store', [LeadEventController::class,'store'])->middleware('checkLeadId')->name('lead-event-store');
        Route::get('lead-event/edit/{id}', [LeadEventController::class,'edit'])->middleware('checkLeadId')->name('lead-event-edit');
        Route::post('lead-event/update', [LeadEventController::class,'update'])->middleware('checkLeadId')->name('lead-event-update');
        Route::get('lead-event/view/{id}', [LeadEventController::class,'view'])->middleware('checkLeadId')->name('lead-event-view');
        Route::put('lead-event/mode', [LeadEventController::class,'changeStatus'])->middleware('checkLeadId')->name('lead-event-mode');
        Route::delete('lead-event/destroy', [LeadEventController::class,'delete'])->middleware('checkLeadId')->name('lead-event-destroy');


        //Write Route for Lead Notes
        Route::get('lead-note', [LeadNoteController::class,'index'])->middleware('checkLeadId')->name('lead-note');
        Route::get('lead-note/create', [LeadNoteController::class,'create'])->middleware('checkLeadId')->name('lead-note-create');
        Route::post('lead-note/store', [LeadNoteController::class,'store'])->middleware('checkLeadId')->name('lead-note-store');
        Route::get('lead-note/edit/{id}', [LeadNoteController::class,'edit'])->middleware('checkLeadId')->name('lead-note-edit');
        Route::post('lead-note/update', [LeadNoteController::class,'update'])->middleware('checkLeadId')->name('lead-note-update');
        Route::get('lead-note/view/{id}', [LeadNoteController::class,'view'])->middleware('checkLeadId')->name('lead-note-view');
        Route::put('lead-note/mode', [LeadNoteController::class,'changeStatus'])->middleware('checkLeadId')->name('lead-note-mode');
        Route::delete('lead-note/destroy', [LeadNoteController::class,'delete'])->middleware('checkLeadId')->name('lead-note-destroy');

    });
    
});


