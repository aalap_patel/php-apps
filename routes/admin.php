<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\TenantController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\PermissionController;


Route::get('dashboard',[DashboardController::class,'index'])->name('dashboard');
        
//Write Route for Create Tenant in Master Admin
Route::get('tenant', [TenantController::class,'index'])->name('tenant');
Route::get('tenant/create', [TenantController::class,'create'])->name('tenant-create');
Route::post('tenant/store', [TenantController::class,'store'])->name('tenant-store');
Route::get('tenant/edit/{id}', [TenantController::class,'edit'])->name('tenant-edit');
Route::get('tenant/view/{id}', [TenantController::class,'view'])->name('tenant-view');
Route::put('tenant/mode', [TenantController::class,'changeStatus'])->name('tenant-mode');
Route::post('tenant/update', [TenantController::class,'update'])->name('tenant-update');
Route::delete('tenant/destroy', [TenantController::class,'delete'])->name('tenant-destroy');
Route::get('tenant/show/{id}', [TenantController::class,'show'])->name('tenant-show');
Route::post('tenant/create-database', [TenantController::class,'createDatabase'])->name('tenant-create-batabase');

//Write Route for Create User in Admin
Route::get('user', [UserController::class,'index'])->name('user');
Route::get('user/create', [UserController::class,'create'])->name('user-create');
Route::post('user/store', [UserController::class,'store'])->name('user-store');
Route::get('user/edit/{id}', [UserController::class,'edit'])->name('user-edit');
Route::get('user/view/{id}', [UserController::class,'view'])->name('user-view');
Route::put('user/mode', [UserController::class,'changeStatus'])->name('user-mode');
Route::post('user/update', [UserController::class,'update'])->name('user-update');
Route::delete('user/destroy', [UserController::class,'delete'])->name('user-destroy');
Route::get('user/show/{id}', [UserController::class,'show'])->name('user-show');
Route::get('profile', [UserController::class,'profile'])->name('profile');
Route::post('update-profile', [UserController::class,'updateProfile'])->name('update-profile');
Route::post('change-password', [UserController::class,'changePassword'])->name('change-password');
Route::get('user/getPermission', [UserController::class,'getPermission'])->name('user-get-permission');

//Write Route for Create Role in Admin
Route::get('role', [RoleController::class,'index'])->name('role')->middleware('permission:role_list');
Route::get('role/create', [RoleController::class,'create'])->name('role-create')->middleware('permission:role_create');
Route::post('role/store', [RoleController::class,'store'])->name('role-store')->middleware('permission:role_create');
Route::get('role/edit/{id}', [RoleController::class,'edit'])->name('role-edit')->middleware('permission:role_view');
Route::get('role/view/{id}', [RoleController::class,'view'])->name('role-view')->middleware('permission:role_view');
Route::put('role/mode', [RoleController::class,'changeStatus'])->name('role-mode')->middleware('permission:role_edit');
Route::post('role/update', [RoleController::class,'update'])->name('role-update')->middleware('permission:role_edit');
Route::delete('role/destroy', [RoleController::class,'delete'])->name('role-destroy')->middleware('permission:role_delete');

//Write Route for Create Permission in Admin
Route::get('permission', [PermissionController::class,'index'])->name('permission')->middleware('permission:permission_list');
Route::get('permission/create', [PermissionController::class,'create'])->name('permission-create')->middleware('permission:permission_create');
Route::post('permission/store', [PermissionController::class,'store'])->name('permission-store')->middleware('permission:permission_create');
Route::get('permission/edit/{id}', [PermissionController::class,'edit'])->name('permission-edit')->middleware('permission:permission_view');
Route::get('permission/view/{id}', [PermissionController::class,'view'])->name('permission-view')->middleware('permission:permission_list')->middleware('permission:permission_list');
Route::put('permission/mode', [PermissionController::class,'changeStatus'])->name('permission-mode')->middleware('permission:permission_edit');
Route::post('permission/update', [PermissionController::class,'update'])->name('permission-update')->middleware('permission:permission_edit');
Route::delete('permission/destroy', [PermissionController::class,'delete'])->name('permission-destroy')->middleware('permission:permission_delete');