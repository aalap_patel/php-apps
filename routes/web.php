<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

///////////ROUTE FOR TENANT////////////
Route::domain('{subdomain_name}.localhost.com')->name('tenant.')->middleware('tenant')->namespace('App\Http\Controllers\Tenant')->group(function ($router) {
	require __DIR__.'/tenant.php';
});

Route::domain(env('SITE_URL'))->group(function ($router) {
	///////////////////ROUTE FOR FROUNTSIDE/////////////////////
	Route::get('/', function () {
		return view('frontend/welcome');
	});
	require __DIR__.'/front.php';

	////////////////////ROUTE FOR SUPER ADMIN/////////////////////
	Route::get('/admin', function () {
		if (Auth::check() && Auth::User()->is_admin ==1) {
	    	//return Redirect('admin/dashboard');
		} else {   
			return view('auth.login');	
		}
	});

	//Master Admin Auth Route
	Route::group(array('namespace'=>'Admin', 'prefix' => 'admin'), function(){
		require __DIR__.'/auth.php'; //This route are used in without login in master admin panel.
	});

	Route::group(array('middleware'=>['admin.auth','verified'],'namespace'=>'Admin', 'prefix' => 'admin'), function(){
		require __DIR__.'/admin.php'; 
	});
});



