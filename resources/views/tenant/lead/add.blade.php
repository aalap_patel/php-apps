@extends('layouts-tenant.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Lead</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Add Lead</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
          
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Add Lead</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="addData" name="addData" enctype="multipart/form-data" method="post" action="{{route('tenant.lead-store',['subdomain_name'=>Request::route('subdomain_name')])}}">
                @csrf
                <div class="card-body">
                   <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Salutation<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2" required="" name="salutation" id="salutation" style="width: 100%;">
                        <option value="">Select Salutation</option>
                        @foreach(\Config::get('constants.salutationArray') as $key=>$value)
                          <option value="{{$key}}" @if($key==old('salutation')) selected="" @endif>{{$value}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">First Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="first_name" name="first_name" maxlength="150" minlength="2" placeholder="First name" value="{{old('first_name')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Last Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="last_name" name="last_name" maxlength="150" minlength="2" placeholder="Last Name" value="{{old('last_name')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Title<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="title" name="title" maxlength="255" minlength="2" placeholder="Enter Title" value="{{old('title')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Profile Image</label>
                    <div class="col-sm-8">
                      <input type="file" class="form-control" id="profile_pic" name="profile_pic" placeholder="Profile image">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Email<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" required="" maxlength="100" minlength="5" id="primary_email" name="primary_email" placeholder="Email" value="{{old('primary_email')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Secondry Email</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" maxlength="100" minlength="5" id="secondry_email" name="secondry_email" placeholder="Secondry Email" value="{{old('secondry_email')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Department<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="department" name="department" maxlength="255" minlength="2" placeholder="Enter Department" value="{{old('department')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Address 1<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" required="" id="address1" name="address1">{{old('address1')}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Address 2</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" id="address2" name="address2">{{old('address2')}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">City<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="100" minlength="2" id="city" name="city" required="" placeholder="Enter City" value="{{old('city')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">State<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="100" minlength="2" id="state" name="state" required="" placeholder="Enter State" value="{{old('state')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Country<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2" required="" name="country_id" id="country_id" style="width: 100%;">
                        <option value="">Select Country</option>
                        @if($countries->count()>0)
                          @foreach($countries as $country)
                            <option value="{{$country->id}}" @if($country->id==old('country_id')) selected="" @endif>{{$country->country_name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Zipcode<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="10" minlength="2" id="zipcode" name="zipcode" required="" placeholder="Enter zipcode" value="{{old('zipcode')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Phone1<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control number" maxlength="15" minlength="6" id="phone1" name="phone1" required="" placeholder="Enter Phone Number" value="{{old('phone1')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Phone2</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="15" minlength="2" id="phone2" name="phone2" placeholder="Enter Phone2" value="{{old('phone2')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Organization Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" required="" class="form-control" maxlength="200" minlength="2" id="organization_name" name="organization_name" placeholder="Enter Organization Name" value="{{old('organization_name')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Organization Email<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" required="" class="form-control email" maxlength="150" minlength="2" id="organization_email" name="organization_email" placeholder="Enter Organization Email" value="{{old('organization_email')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Organization Phone1<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" required="" class="form-control number" maxlength="15" minlength="2" id="organization_phone1" name="organization_phone1" placeholder="Enter Organization Phone1" value="{{old('organization_phone1')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Organization Phone2</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control number" maxlength="15" minlength="2" id="organization_phone2" name="organization_phone2" placeholder="Enter Organization Phone2" value="{{old('organization_phone2')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Organization Website</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control url" maxlength="150" minlength="2" id="organization_website" name="organization_website" placeholder="Enter Organization Website" value="{{old('organization_website')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Facebook Url</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control url" maxlength="200" minlength="2" id="facebook_url" name="facebook_url" placeholder="Enter organization facebook url" value="{{old('facebook_url')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Twitter Url</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control url" maxlength="200" minlength="2" id="twitter_url" name="twitter_url" placeholder="Enter organization twitter url" value="{{old('twitter_url')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Linkedin Url</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control url" maxlength="200" minlength="2" id="linkedin_url" name="linkedin_url" placeholder="Enter organization linkedin url" value="{{old('linkedin_url')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Instagram Url</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control url" maxlength="200" minlength="2" id="instagram_url" name="instagram_url" placeholder="Enter organization instagram url" value="{{old('instagram_url')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Is Contact </label>
                    <div class="custom-control">
                      <input type="checkbox" name="is_contact" id="is_contact" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                      <small>If you don't need to used this lead as contact than make it off.</small>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('tenant.lead',['subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-default">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
  <!-- Bootstrap Switch -->
<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<script language="javascript">
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })
 });

	$('#addData').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      name: {
        required: true
      }
    },
    messages: {
      email: {
        email: "Please enter a vaild email address"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop