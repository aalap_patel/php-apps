@extends('layouts-tenant.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Event</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Add Event</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
          
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Add Event</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="addData" name="addData" enctype="multipart/form-data" method="post" action="{{route('tenant.lead-event-store',['subdomain_name'=>Request::route('subdomain_name')])}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="name" name="name" maxlength="255" minlength="2" placeholder="Enter event name" value="{{old('name')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Description</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" id="description" placeholder="Enter event description" name="description">{{old('description')}}</textarea>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Start Date<span class="red">*</span></label>
                      <div class="input-group date col-sm-4" id="reservationdate" data-target-input="nearest">
                          <input type="text" id="start_date" name="start_date" required="" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                          <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">End Date<span class="red">*</span></label>
                      <div class="input-group date col-sm-4" id="reservationdate_end" data-target-input="nearest">
                          <input type="text" id="end_date" name="end_date" required="" class="form-control datetimepicker-input" data-target="#reservationdate_end"/>
                          <div class="input-group-append" data-target="#reservationdate_end" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <input type="hidden" id="leadId" name="leadId" value="{{$leadId}}">
                      <a href="{{route('tenant.lead-event',['leadId'=>$leadId,'subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-default">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
  <!-- Bootstrap Switch -->
<script language="javascript">
  $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      //Date picker
      $('#reservationdate').datetimepicker({
          format: 'YYYY-MM-DD'
      });

      $('#reservationdate_end').datetimepicker({
          format: 'YYYY-MM-DD'
      });
      
  });

	$('#addData').validate({
    rules: {
      name: {
        required: true
      },
      product_code: {
        required: true
      },
      price: {
        required: true,
        number: true
      }
    },
    messages: {},
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop