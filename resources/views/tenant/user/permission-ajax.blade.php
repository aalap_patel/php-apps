<div class="form-group">
  <label for="permission" class="col-sm-4 col-form-label">Select Permissions</label>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <div class="row">
      @if(count($permissionArray))
      <div id="per">
      <input type="checkbox" id="select_all_permissions" />
      Select All</div>
        @foreach($permissionArray as $permissionName=>$permissonValue)                                    
         <div class="col-sm-12">
           <label class="col-sm-12"> {{ucfirst($permissionName)}} </label>
           @foreach($permissonValue as $permission)
           <div class="checkbox form-check-inline">
             <label>
               <input type="checkbox" class="flat" name="permissions[]" value="{{$permission['id']}}" {{in_array($permission['id'],$recordPermissions) ? 'checked' : '' }}>
               {{$permission['name']}}</label>
           </div>
           @endforeach
         </div>
          @endforeach
        @endif  
      </div>                                  
  </div>                         
</div>
<script type="text/javascript">
$( document ).ready(function() {
    $("#select_all_permissions").on('click',function() { 
      if($(this).prop("checked")){
        $("input[type=checkbox].flat").prop("checked",true);
      }else{
        $("input[type=checkbox].flat").prop("checked",false);
      }
    });
});
</script>