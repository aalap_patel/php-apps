@extends('layouts-tenant.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile Update</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Profile Update</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    @include('backend.errors')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Profile</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Change password</a></li>
                  @if(Auth::user()->is_admin)
                  <li class="nav-item"><a class="nav-link" href="#site-setting" data-toggle="tab">Website Setting</a></li>
                  @endif
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <form class="form-horizontal" id="updateProfile" name="updateProfile" method="post" enctype="multipart/form-data" action="{{route('tenant.update-profile',['subdomain_name'=>Request::route('subdomain_name')])}}">
                       @csrf
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Name<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" required id="name" name="name" value="{{$record->name}}" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="email"  class="form-control" id="email" disabled="" readonly="" value="{{$record->email}}" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="secondry_email" class="col-sm-2 col-form-label">Secondry Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control email" maxlength="100" minlength="5" id="secondry_email" name="secondry_email" placeholder="Secondry Email" value="{{$record->secondry_email}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="profile_image" class="col-sm-2 col-form-label">Profile Image</label>
                        <div class="col-sm-10">
                          <input type="file" class="form-control" id="profile_image" name="profile_image" placeholder="Profile image">
                        </div>
                      </div>
                      @php
                        $image =  asset('storage/no-image-available.png');
                    
                        if(!empty($record->profile_image)) {
                          $image = asset('storage/'.$record->profile_image);
                        }
                      @endphp
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Uploaded Profile Image</label>
                        <div class="col-sm-10">
                          <div class="text-left">
                            <img class="profile-user-img img-fluid img-circle" src="{{$image}}" alt="{{$record->name}}" title="{{$record->name}}">
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="address1" class="col-sm-2 col-form-label">Address 1<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <textarea class="form-control" required id="address1" name="address1">{{$record->address1}}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="address2" class="col-sm-2 col-form-label">Address 2</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="address2" name="address2">{{$record->address2}}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="city" class="col-sm-2 col-form-label">City<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" maxlength="100" minlength="2" id="city" name="city" required placeholder="Enter City" value="{{$record->city}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="state" class="col-sm-2 col-form-label">State<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" maxlength="100" minlength="2" id="state" name="state" required placeholder="Enter State" value="{{$record->state}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="country_id" class="col-sm-2 col-form-label">Country<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <select class="form-control select2" required name="country_id" id="country_id" style="width: 100%;">
                            <option value="">Select Country</option>
                            @if($countries->count()>0)
                              @foreach($countries as $country)
                                <option value="{{$country->id}}" @if($record->country_id==$country->id) selected="" @endif>{{$country->country_name}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="zipcode" class="col-sm-2 col-form-label">Zipcode<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" maxlength="10" minlength="2" id="zipcode" name="zipcode" required placeholder="Enter zipcode" value="{{$record->zipcode}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="phone1" class="col-sm-2 col-form-label">Phone1<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control number" maxlength="15" minlength="6" id="phone1" name="phone1" required placeholder="Enter Phone Number" value="{{$record->phone1}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="phone2" class="col-sm-2 col-form-label">Phone2</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" maxlength="15" minlength="2" id="phone2" name="phone2" placeholder="Enter Phone2" value="{{$record->phone2}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="preferred_timezone" class="col-sm-2 col-form-label">Preferred Timezone</label>
                        <div class="col-sm-10">
                          <select class="form-control" name="preferred_timezone" id="preferred_timezone">
                            <option value="">Select Timezone</option>
                            @if(\Config::get('constants.timezoneArray'))
                              @foreach(\Config::get('constants.timezoneArray') as $key => $value)
                                <option value="{{$key}}" @if($key==$record->preferred_timezone) selected="" @endif>{{$value}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="preferred_currency" class="col-sm-2 col-form-label">Preferred Currency</label>
                        <div class="col-sm-10">
                          <select class="form-control select2" name="preferred_currency" id="preferred_currency" style="width: 100%;">
                            <option value="">Select Currency</option>
                            @if($currency->count()>0)
                              @foreach($currency as $currencyName)
                                <option value="{{$currencyName->id}}" @if($currencyName->id==$record->preferred_currency) selected="" @endif>{{$currencyName->code.' ('.$currencyName->name.')'}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="preferred_date_format" class="col-sm-2 col-form-label">Preferred Date Format</label>
                        <div class="col-sm-10">
                          <select class="form-control select2" name="preferred_date_format" id="preferred_date_format" style="width: 100%;">
                            <option value="">Select Date Format</option>
                            @if(\Config::get('constants.dateFormat'))
                              @foreach(\Config::get('constants.dateFormat') as $key => $value)
                                <option value="{{$key}}" @if($key==$record->preferred_date_format) selected="" @endif>{{$value}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <input type="submit" class="btn btn-info" value="Submit" name="jaypal">
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                  @if(Auth::user()->is_admin)
                  <div class="tab-pane" id="site-setting">
                    <form class="form-horizontal" id="updateSetting" name="updateSetting" method="post" enctype="multipart/form-data" action="{{route('tenant.change-site-data',['subdomain_name'=>Request::route('subdomain_name')])}}">
                       @csrf
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Site Name<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" required id="name" name="name" value="{{isset($siteData->name) ? $siteData->name:''}}" placeholder="Enter your site name. For example Sonic Jellybean Applications">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="domain_url" class="col-sm-2 col-form-label">Domain Url</label>
                        <div class="col-sm-10">
                          <input type="text"  class="form-control" id="domain_url" disabled="" readonly="" value="{{$baseUrl}}" placeholder="Domain url">
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <label for="logo" class="col-sm-2 col-form-label">Logo<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="file" class="form-control" id="logo" name="logo" placeholder="Logo">
                        </div>
                      </div>
                      @php
                        $image =  asset('storage/no-image-available.png');
                    
                        if(!empty($siteData->logo)) {
                          $image = asset('storage/'.$siteData->logo);
                        }
                      @endphp
                      <div class="form-group row">
                        <label for="profileImage" class="col-sm-2 col-form-label">Uploaded Logo</label>
                        <div class="col-sm-10">
                          <div class="text-left">
                            <img class="profile-user-img img-fluid img-circle" src="{{$image}}" alt="{{isset($siteData->name) ? $siteData->name:''}}" title="{{isset($siteData->name) ? $siteData->name:''}}">
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <label for="facebook_url" class="col-sm-2 col-form-label">Facebook Url</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control url" maxlength="150" minlength="2" id="facebook_url" name="facebook_url" placeholder="Enter your facebook url" value="{{isset($siteData->facebook_url) ? $siteData->facebook_url:''}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="linkedin_url" class="col-sm-2 col-form-label">Linkedin Url</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control url" maxlength="150" minlength="2" id="linkedin_url" name="linkedin_url" placeholder="Enter linkedin url" value="{{isset($siteData->linkedin_url) ? $siteData->linkedin_url:''}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="twitter_url" class="col-sm-2 col-form-label">Twitter Url</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" maxlength="150" minlength="2" id="twitter_url" name="twitter_url" placeholder="Enter twitter url" value="{{isset($siteData->twitter_url) ? $siteData->twitter_url:''}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <input type="submit" class="btn btn-info" value="Submit" name="submit">
                          <input type="hidden" name="baseurl" value="{{$baseUrl}}">
                        </div>
                      </div>
                    </form>
                  </div>
                  @endif
                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" id="updatePassword" name="updatePassword" method="post" enctype="multipart/form-data" action="{{route('tenant.change-password',['subdomain_name'=>Request::route('subdomain_name')])}}">
                       @csrf
                      <div class="form-group row">
                        <label for="inputOldPassword" class="col-sm-2 col-form-label">Old Password<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" required="required" id="old_password" name="old_password" placeholder="Enter your old password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputNewPassword" class="col-sm-2 col-form-label">New Password<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" required="required" id="password" name="password" placeholder="Enter yout new password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputConPassword" class="col-sm-2 col-form-label">Confirm Password<span class="red">*</span></label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" required="required" id="password_confirm" name="password_confirm" placeholder="Confirm password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div> 
<script language="javascript">
  
  //Initialize Select2 Elements
    $('.select2').select2()
    $('#preferred_timezone').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })


  //$(document).ready(function() {
      $('#updateProfile').validate({
        rules: {
          name: {
            required: true
          },
          role: {
            required: true
          }
        },
        messages: {
          email: {
            email: "Please enter a vaild email address"
          },
          password: {
            minlength: "Your password must be at least 5 characters long"
          }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('div').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });
 // });

	$('#updatePassword').validate({
    rules: {
      old_password: {
        required: true
      },
      password: {
        required: true,
        minlength: 5,
        maxlength: 20
      },
      password_confirm : {
          required: true,
          minlength: 5,
          maxlength: 20,
          equalTo : "#password"
      }
    },
    messages: {},
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('div').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });

  $('#updateSetting').validate({
    rules: {
      name: {
        required: true
      }
    },
    messages: {},
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('div').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });

  

  
</script> 
@stop