@extends('layouts-tenant.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Add User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
          
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Add User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="addData" name="addData" enctype="multipart/form-data" method="post" action="{{route('tenant.user-store',['subdomain_name'=>Request::route('subdomain_name')])}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="name" name="name" maxlength="150" minlength="2" placeholder="Name" value="{{old('name')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Email<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" required="" maxlength="100" minlength="5" id="email" name="email" placeholder="Email" value="{{old('email')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Secondry Email</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control" maxlength="100" minlength="5" id="secondry_email" name="secondry_email" placeholder="Secondry Email" value="{{old('secondry_email')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Password<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" required="" id="password" name="password" placeholder="Password" minlength="5" maxlength="20">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Confirm Password<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" required="" id="password_confirmation" name="password_confirmation" placeholder="Password" minlength="5" maxlength="20">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Profile Image<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="file" class="form-control" id="profile_image" name="profile_image" placeholder="Profile image">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Address 1<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" required="" id="address1" name="address1">{{old('address1')}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Address 2</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" id="address2" name="address2">{{old('address2')}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">City<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="100" minlength="2" id="city" name="city" required="" placeholder="Enter City" value="{{old('city')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">State<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="100" minlength="2" id="state" name="state" required="" placeholder="Enter State" value="{{old('state')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Country<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2" required="" name="country_id" id="country_id" style="width: 100%;">
                        <option value="">Select Country</option>
                        @if($countries->count()>0)
                          @foreach($countries as $country)
                            <option value="{{$country->id}}" @if($country->id == old('country_id')) selected="" @endif>{{$country->country_name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Zipcode<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="10" minlength="2" id="zipcode" name="zipcode" required="" placeholder="Enter zipcode" value="{{old('zipcode')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Phone1<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control number" maxlength="15" minlength="6" id="phone1" name="phone1" required="" placeholder="Enter Phone Number" value="{{old('phone1')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Phone2</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control number" maxlength="15" minlength="2" id="phone2" name="phone2" placeholder="Enter Phone2" value="{{old('phone2')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Preferred Timezone</label>
                    <div class="col-sm-8">
                      <select class="form-control select2" name="preferred_timezone" id="preferred_timezone" style="width: 100%;">
                        <option value="">Select Timezone</option>
                        @if(\Config::get('constants.timezoneArray'))
                          @foreach(\Config::get('constants.timezoneArray') as $key => $value)
                            <option value="{{$key}}" @if($key == old('preferred_timezone')) selected="" @endif>{{$value}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Preferred Currency</label>
                    <div class="col-sm-8">
                      <select class="form-control select2" name="preferred_currency" id="preferred_currency" style="width: 100%;">
                        <option value="">Select Currency</option>
                        @if($currency->count()>0)
                          @foreach($currency as $currencyName)
                            <option value="{{$currencyName->id}}" @if($currencyName->id == old('preferred_currency')) selected="" @endif>{{$currencyName->code.' ('.$currencyName->name.')'}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Preferred Date Format</label>
                    <div class="col-sm-8">
                      <select class="form-control select2" name="preferred_date_format" id="preferred_date_format" style="width: 100%;">
                        <option value="">Select Date Format</option>
                        @if(\Config::get('constants.dateFormat'))
                          @foreach(\Config::get('constants.dateFormat') as $key => $value)
                            <option value="{{$key}}" @if($key == old('preferred_date_format')) selected="" @endif>{{$value}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>


                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Is Verified </label>
                    <div class="custom-control">
                      <input type="checkbox" name="is_verified" id="is_verified" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                      <small>If you don't need to email verification than make it off.</small>
                    </div>
                    
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Role<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2 getPermission" required="" name="role" id="name" style="width: 100%;">
                        <option value="">Select Role</option>
                        @if($roles->count()>0)
                          @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="permissionListing">
                    @include('tenant/user/permission-ajax') 
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('tenant.user',['subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-default">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
  <!-- Bootstrap Switch -->
<script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<script language="javascript">
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

    $('.getPermission').on('change',function(){
      var id = $(this).val();
       $.ajax({
         method: 'get',
         url: "{{route('tenant.user-get-permission',['subdomain_name'=>Request::route('subdomain_name')])}}",
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: {
             id: id,
             _token: $('#_token').val()
         }
      }).then(function (data) {
        $(".permissionListing").html(data);
      }).fail(function (data) {
          alertGritter('Permission issue', 'Get permission failed', 'growl-error');
      });  
    });
 });

	$('#addData').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      name: {
        required: true
      },
      password: {
        required: true,
        minlength: 5,
        maxlength: 20
      },
      password_confirmation: {
            required: true,
            minlength: 5,
            maxlength: 20,
            equalTo: "#password"
        }
    },
    messages: {
      email: {
        email: "Please enter a vaild email address"
      },
      password: {
        minlength: "Your password must be at least 5 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop