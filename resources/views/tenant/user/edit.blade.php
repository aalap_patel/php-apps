@extends('layouts-tenant.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Edit User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
         
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="editData" enctype="multipart/form-data" method="post" action="{{route('tenant.user-update',['subdomain_name'=>Request::route('subdomain_name')])}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" required="required" class="form-control" id="name" name="name" maxlength="150" minlength="2" placeholder="Name" value="{{$record->name}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Email<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" maxlength="100" minlength="5" id="email" placeholder="Email" value="{{$record->email}}" readonly="" disabled="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Secondry Email</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" maxlength="100" minlength="5" id="secondry_email" name="secondry_email" placeholder="Secondry Email" value="{{$record->secondry_email}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Profile Image</label>
                    <div class="col-sm-8">
                      <input type="file" class="form-control" id="profile_image" name="profile_image" placeholder="Profile image">
                    </div>
                  </div>
                  @php
                    $image =  asset('storage/no-image-available.png');
                
                    if(!empty($record->profile_image)) {
                      $image = asset('storage/'.$record->profile_image);
                    }
                  @endphp
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Uploaded Profile Image</label>
                    <div class="col-sm-8">
                      <div class="text-left">
                  <img class="profile-user-img img-fluid img-circle" src="{{$image}}" alt="{{$record->name}}" title="{{$record->name}}">
                </div>
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Address 1<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <textarea class="form-control" required="" id="address1" name="address1">{{$record->address1}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Address 2</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" id="address2" name="address2">{{$record->address2}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">City<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="100" minlength="2" id="city" name="city" required="" placeholder="Enter City" value="{{$record->city}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">State<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="100" minlength="2" id="state" name="state" required="" placeholder="Enter State" value="{{$record->state}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Country<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2" required="" name="country_id" id="country_id" style="width: 100%;">
                        <option value="">Select Country</option>
                        @if($countries->count()>0)
                          @foreach($countries as $country)
                            <option value="{{$country->id}}" @if($record->country_id==$country->id) selected="" @endif>{{$country->country_name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Zipcode<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="10" minlength="2" id="zipcode" name="zipcode" required="" placeholder="Enter zipcode" value="{{$record->zipcode}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Phone1<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control number" maxlength="15" minlength="6" id="phone1" name="phone1" required="" placeholder="Enter Phone Number" value="{{$record->phone1}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Phone2</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="15" minlength="2" id="phone2" name="phone2" placeholder="Enter Phone2" value="{{$record->phone2}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Preferred Timezone</label>
                    <div class="col-sm-8">
                      <select class="form-control select2" name="preferred_timezone" id="preferred_timezone" style="width: 100%;">
                        <option value="">Select Timezone</option>
                        @if(\Config::get('constants.timezoneArray'))
                          @foreach(\Config::get('constants.timezoneArray') as $key => $value)
                            <option value="{{$key}}" @if($key==$record->preferred_timezone) selected="" @endif>{{$value}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Preferred Currency</label>
                    <div class="col-sm-8">
                      <select class="form-control select2" name="preferred_currency" id="preferred_currency" style="width: 100%;">
                        <option value="">Select Currency</option>
                        @if($currency->count()>0)
                          @foreach($currency as $currencyName)
                            <option value="{{$currencyName->id}}" @if($currencyName->id==$record->preferred_currency) selected="" @endif>{{$currencyName->code.' ('.$currencyName->name.')'}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Preferred Date Format</label>
                    <div class="col-sm-8">
                      <select class="form-control select2" name="preferred_date_format" id="preferred_date_format" style="width: 100%;">
                        <option value="">Select Date Format</option>
                        @if(\Config::get('constants.dateFormat'))
                          @foreach(\Config::get('constants.dateFormat') as $key => $value)
                            <option value="{{$key}}" @if($key==$record->preferred_date_format) selected="" @endif>{{$value}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Is Verified </label>
                    <div class="custom-control">
                      <input type="checkbox" name="is_verified" id="is_verified" @if($record->email_verified_at) checked @endif data-bootstrap-switch data-off-color="danger" data-on-color="success">
                      <small>If you don't need to email verification than make it off.</small>
                    </div>
                    
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Role<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2" required="" name="role" id="role" style="width: 100%;">
                        <option value="">Select Role</option>
                        @if($roles->count()>0)
                          @foreach($roles as $role)
                            <option value="{{$role->id}}" {{in_array($role['id'],$recordRole) ? 'selected' : '' }}>{{$role->name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Select Permissions</label>                           
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="row">
                        @if(count($permissionArray))
                        <div>
                        <input type="checkbox" id="select_all_permissions" />
                        Select All</div>
                          @foreach($permissionArray as $permissionName=>$permissonValue)                                    
                           <div class="col-sm-12">
                             <label class="col-sm-12"> {{ucfirst($permissionName)}} </label>
                             @foreach($permissonValue as $permission)
                             <div class="checkbox form-check-inline">
                               <label>
                                 <input type="checkbox" class="flat" name="permissions[]" value="{{$permission['id']}}" {{in_array($permission['id'],$recordPermissions) ? 'checked' : '' }}>
                                 {{$permission['name']}}</label>
                             </div>
                             @endforeach
                           </div>
                            @endforeach
                          @endif  
                        </div>                                  
                    </div>                         
                </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('tenant.user',['subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-default">Cancel</a>
                      <input type="hidden" name="id" value="{{$record->id}}">
                      <input type="hidden" name="queryString" value="{{Request::getQueryString()}}">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
  <script src="{{asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<script language="javascript">
  
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $("#select_all_permissions").on('click',function() {   
      if($(this).prop("checked")){
        $("input[type=checkbox].flat").prop("checked",true);
      }else{
        $("input[type=checkbox].flat").prop("checked",false);
      }
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

  $('#editData').validate({
    rules: {
      name: {
        required: true
      },
      role: {
        required: true
      }
    },
    messages: {
      email: {
        email: "Please enter a vaild email address"
      },
      password: {
        minlength: "Your password must be at least 5 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop