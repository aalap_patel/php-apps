@extends('layouts-tenant.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Add Role</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
          
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Add Role</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="addData" name="addData" enctype="multipart/form-data" method="post" action="{{route('tenant.role-store',['subdomain_name'=>Request::route('subdomain_name')])}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="name" name="name" maxlength="150" minlength="2" placeholder="Name" value="{{old('name')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Select Permissions</label>                         
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="row">
                        @if(count($permissionArray))
                        <div>
                        <input type="checkbox" id="select_all_permissions" />
                        Select All</div>
                          @foreach($permissionArray as $permissionName=>$permissonValue)                                    
                           <div class="col-sm-12">
                             <label class="col-sm-12"> {{ucwords($permissionName)}} </label>
                             @foreach($permissonValue as $permission)
                              <div class="checkbox form-check-inline">
                               <label style="font-weight: normal;">
                    <input type="checkbox" class="flat" name="permissions[]" value="{{$permission['id']}}" @if(old('permissions') && is_array(old('permissions')) && in_array($permission['id'],old('permissions'))) checked @endif >
                                 {{$permission['name']}}</label>
                             </div>
                             @endforeach
                           </div>
                            @endforeach
                          @endif  
                        </div>                                  
                    </div>                         
                </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('tenant.role',['subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-default">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<script language="javascript">
  $("#select_all_permissions").on('click',function() {   
    if($(this).prop("checked")){
      $("input[type=checkbox].flat").prop("checked",true);
    }else{
      $("input[type=checkbox].flat").prop("checked",false);
    }
  });
  
  jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z\s]+$/i.test(value);
  }, "Only alphabetical characters");
  
	$('#addData').validate({
    rules: {
      name: {
        required: true,
        lettersonly: true
      }
    },
    messages: {
      name: {
        required: "Please enter a name",
        lettersonly: "Letters only please",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop