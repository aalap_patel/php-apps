@extends('layouts-tenant.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Note</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Edit Note</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
         
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit Note</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="editData" enctype="multipart/form-data" method="post" action="{{route('tenant.lead-note-update',['subdomain_name'=>Request::route('subdomain_name')])}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Note<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" required="" class="form-control" id="notes" name="notes" maxlength="255" minlength="2" placeholder="Enter note" value="{{$record->notes}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Description</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" required="" id="description" placeholder="Enter event description" name="description">{{$record->description}}</textarea>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Note File</label>
                    <div class="col-sm-8">
                      <input type="file" class="form-control" id="blob_azure_url" name="blob_azure_url" placeholder="file">
                    </div>
                  </div>
                  @php
                    $image =  asset('storage/no-image-available.png');
                    $type = '';
                    if(!empty($record->blob_azure_url)) {
                      $exp=explode('.',$record->blob_azure_url);
                      if (in_array(end($exp), ['jpeg','png','jpg','gif','svg'])){
                        $type = 'image';
                      }
                      $image = asset('storage/'.$record->blob_azure_url);
                    }
                  @endphp
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Uploaded File</label>
                    <div class="col-sm-8">
                      <div class="text-left">

                        @if($type=='image')
                          <img class="profile-user-img img-fluid img-circle" src="{{$image}}" alt="{{$record->notes}}" title="{{$record->notes}}">
                        @else
                          <a href="{{$image}}" target="_blank">click here to show uploaded file</a>
                        @endif
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('tenant.lead-note',['leadId'=>$leadId,'subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-default">Cancel</a>
                      <input type="hidden" name="id" value="{{$record->id}}">
                      <input type="hidden" id="leadId" name="leadId" value="{{$leadId}}">
                      <input type="hidden" name="queryString" value="{{Request::getQueryString()}}">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<script language="javascript">
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  });

  $('#editData').validate({
    rules: {
      notes: {
        required: true
      }
    },
    messages: {},
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop