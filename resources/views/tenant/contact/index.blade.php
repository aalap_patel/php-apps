@extends('layouts-tenant.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Contacts</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Contacts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
     @include('backend.errors')
    <!-- Main content -->
    <section class="content">
      <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">Filter Options</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Click to Collapse filter option">
                    <i class="fas fa-plus"></i>
                  </button>
                  <a class="btn btn-primary btn-xs" title="Click to add new contact" href="{{route('tenant.contact',['subdomain_name'=>Request::route('subdomain_name')])}}">
                      <i class="fas fa-redo-alt">
                      </i>
                      Reset
                  </a>
                  <a class="btn btn-primary btn-xs" title="Click to add new contact" href="{{route('tenant.contact-create',['subdomain_name'=>Request::route('subdomain_name')])}}">
                      <i class="fas fa-folder">
                      </i>
                      Add New
                  </a>
                  
                </div>
              </div>
              <div class="card-body">
                <div class="col-12">
                    <form name="form_filter" id="form_filter" action="{{route('tenant.contact',['subdomain_name'=>Request::route('subdomain_name')])}}" class="form-horizontal form-label-left input-mask" novalidate="novalidate" method="get">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">                                        
                                        <label class="control-label" for="customer-id">Status</label>
                                        <select class="form-control custom-select" name="status" id="status_filter" data-placeholder="">
                                            <option value="">All</option>
                                            <option value="1" {{request()->status=="1" ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{request()->status=="0" ? 'selected' : '' }}>Inactive</option>
                                            <option value="2" {{request()->status=="2" ? 'selected' : '' }}>Deleted</option>                         
                                        </select>
                                    </div>
                                    @if(Auth::user()->is_admin)
                                    <div class="form-group">
                                      <label class="control-label" for="added_by">AddedBy</label>
                                        <select class="form-control select2" name="user_id" id="user_id" style="width: 100%;">
                                          <option value="">Select AddedBy</option>
                                          @if($users->count()>0)
                                            @foreach($users as $user)
                                              <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                <div class="control col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="signup-date">From Date </label>
                                        <div class="input-group date" id="from_date" data-target-input="nearest">
                                        <input type="text" name="from_date" class="form-control datetimepicker-input" data-target="#from_date"/ placeholder="To:YYYY-MM-DD" value="{{request()->from_date}}">
                                        <div class="input-group-append" data-target="#from_date" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>

                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label" for="signup-date">To Date </label>                            
                                        <div class="input-group date" id="to_date" data-target-input="nearest">
                                        <input type="text" name="to_date" class="form-control datetimepicker-input" data-target="#to_date"/ placeholder="To:YYYY-MM-DD" value="{{request()->to_date}}">
                                        <div class="input-group-append" data-target="#to_date" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">                                        
                                        <label class="control-label" for="fisrt-name">Search By Term </label>
                                        <select class="form-control custom-select" name="search_by" data-placeholder="Search by">
                                            <option value="name" {{request()->search_by=='name' ? 'selected' : '' }} >Name</option>
                                            <option value="primary_email" {{request()->search_by=='email' ? 'selected' : '' }} >Email</option>
                                            <option value="title" {{request()->search_by=='title' ? 'selected' : '' }} >Title</option>
                                            <option value="phone1" {{request()->search_by=='phone' ? 'selected' : '' }} >Phone</option>
                                        </select>
                                    </div>  
                                    <div class="form-group">
                                        <label class="control-label" for="last-name">Search String</label>
                                        <input type="text" name="search_str" id="search_str" class="form-control col-md-7 col-xs-12" placeholder="Enter Search String" autocomplete="off" value="{{request()->search_str}}">
                                    </div>                                        
                                </div>            
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="form-group float-right">  
                                        <button type="submit" class="btn btn-info">Search</button> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                                                         
                </div>
              </div>
              <!-- /.card-body -->
             </div>

      <!-- Default box -->
      <div class="card">
        
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 15%">
                          Name
                      </th>
                      <th style="width: 10%">
                          Image
                      </th>
                       <th style="width: 10%">
                          Title
                      </th>
                      <th style="width: 10%">
                          Email
                      </th>
                      <th style="width: 10%">
                          Phone
                      </th>
                      @if(Auth::user()->is_admin)
                      <th style="width: 10%">
                          Added by
                      </th>
                      @endif
                      <th style="width: 09%" class="text-center">
                          Status
                      </th>
                      <th style="width: 25%">
                        Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                 @forelse($records as $record)
                 @php
                    $image =  asset('storage/no-image-available.png');
                
                    if(!empty($record->profile_pic)) {
                      $image = asset('storage/'.$record->profile_pic);
                    }
                 @endphp
                  <tr id="record_{{$record->id}}">
                      <td>
                         {{$record->id}} 
                      </td>
                      <td>
                          {{\Config::get('constants.salutationArray.'.$record->salutation)}} {{$record->first_name.' '.$record->last_name}}
                          <p><small>
                              Created : {{$record->created_at}}
                          </small></p>
                      </td>
                      <td>
                        <img class="timeline-badge-userpic" src="{{$image}}" width="70" >  
                      </td>
                      <td>
                        {{$record->title}}  
                      </td>
                      <td>
                        {{$record->primary_email}}  
                      </td>
                      <td>
                        {{$record->phone1}}  
                      </td>
                      @if(Auth::user()->is_admin)
                      <td>
                        {{$record->addedBy->name}}  
                      </td>
                      @endif
                      <td class="project-state">
                        @if(!$record->trashed())
                          <a href="javascript:void();" title="Click to change status."><span id="status{{ $record->id }}" onclick="change_status('{{ $record->id }}','{{route('tenant.contact-mode',['subdomain_name'=>Request::route('subdomain_name')])}}');" class="badge badge-{{ $record->status == 1 ? 'success':'warning' }}">{{ $record->status == 1 ? 'Active':'Inactive'}}</span></a>
                        @else
                          <span class="badge badge-danger">Deleted</span>
                        @endif  
                      </td>
                      <td>
                          <a class="btn btn-primary btn-xs" title="Show Lead detail." href="{{route('tenant.contact-view',['id'=>$record->id,'subdomain_name'=>Request::route('subdomain_name')]).'?'.Request::getQueryString()}}">
                              <i class="fas fa-folder"></i>
                              View
                          </a>
                          <a class="btn btn-info btn-xs" href="{{route('tenant.contact-edit',['id'=>$record->id,'subdomain_name'=>Request::route('subdomain_name')]).'?'.Request::getQueryString()}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a data-id="{{$record->id}}" data-url="{{route('tenant.contact-destroy',['subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-danger btn-xs DataDelete">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                          
                      </td>
                  </tr>
                 @empty
                    <tr><td colspan="100">No Record</td></tr>
                @endforelse 
              </tbody>
          </table>

        </div>
        <!-- /.card-body -->
          <div class="card-footer clearfix">
           {{$records->links()}}
          </div>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  $(document).ready(function(){        
        $('#from_date,#to_date').datetimepicker ({
          format: 'YYYY-MM-DD',
          showClear:true,
        });

        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
          theme: 'bootstrap4'
        })
    });
  </script>
  <script type="text/javascript" src="{{asset('js/tenant-js/contact.js')}}"></script>
@stop