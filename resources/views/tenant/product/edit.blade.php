@extends('layouts-tenant.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}">Home</a></li>
              <li class="breadcrumb-item active">Edit Product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
         
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit Product</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="editData" enctype="multipart/form-data" method="post" action="{{route('tenant.product-update',['subdomain_name'=>Request::route('subdomain_name')])}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" required="" class="form-control" id="name" name="name" maxlength="150" minlength="2" placeholder="Name" value="{{$record->name}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Product Code<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="50" minlength="1" id="product_code" name="product_code" placeholder="Product Code" value="{{$record->product_code}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Description</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" required="" id="description" name="description">{{$record->description}}</textarea>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Price<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" maxlength="9" minlength="1" id="price" name="price" required="" placeholder="Enter Price" value="{{$record->price}}">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('tenant.product',['subdomain_name'=>Request::route('subdomain_name')])}}" class="btn btn-default">Cancel</a>
                      <input type="hidden" name="id" value="{{$record->id}}">
                      <input type="hidden" name="queryString" value="{{Request::getQueryString()}}">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<script language="javascript">
  
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })


  $('#editData').validate({
    rules: {
      name: {
        required: true
      },
      product_code: {
        required: true
      },
      price: {
        required: true,
        number: true
      }
    },
    messages: {},
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop