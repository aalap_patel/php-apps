<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}" />
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
      <!-- Daterange picker -->
      <link rel="stylesheet" href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}">
      <!-- Select2 -->
      <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
      <!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="{{asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap/-4.min.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
      <!-- overlayScrollbars -->
      <link rel="stylesheet" href="{{asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
      <!-- jQuery -->
        <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button)
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>
        <!-- InputMask -->
        <script src="{{asset('assets/plugins/moment/moment.min.js')}}"></script>
        <script src="{{asset('assets/plugins/inputmask/jquery.inputmask.min.js')}}"></script>
        <!-- Select2 -->
        <script src="{{asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>
        <!-- daterangepicker -->
        <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
        
        <!-- overlayScrollbars -->
        <script src="{{asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('assets/dist/js/adminlte.js')}}"></script>
        <!-- jquery-validation -->
      <script src="{{asset('assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
      <script src="{{asset('assets/plugins/jquery-validation/additional-methods.min.js')}}"></script>
      <!-- Tempusdominus Bootstrap 4 -->
      <script src="{{asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
      <!-- gritter notification css -->
      <link href="{{asset('assets/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">
      <link href="{{asset('assets/plugins/gritter/jquery.growl.css')}}" rel="stylesheet">
      
      <!-- gritter notification Scripts -->
      <script src="{{asset('assets/plugins/gritter/gritter.min.js')}}"></script>
  	  <!-- smart-wizard for property page -->
  <script type="text/javascript" >
	    /**
     * Gritter notifications when user does something
     *
     * E.g. alertGritter('Restaurants', 'Request for listing restaurants failed', 'growl-error');
     *
     * @param title = title of the popup
     * @param content = content inside popup
     * @param status = 'growl-error', 'growl-success', 'growl-warning' etc.
     */
    function alertGritter(title, content, status) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: status,
            sticky: false,
            time: ''
        });
    }

	</script>
    @if(Session::has('message'))
    <script type="text/javascript" >
    	alertGritter( <?php echo Session::get('module');  ?>, <?php echo Session::get('message');  ?>, <?php echo Session::get('alert-class'); ?> );
	</script>	
    @endif
  </head>
  
  
