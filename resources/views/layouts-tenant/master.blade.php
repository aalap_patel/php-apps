@include('layouts-tenant.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
	<!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>
  	
    @include('layouts-tenant.header-bar')
 	  @include('layouts-tenant.left-panel')   
    @include('layouts-tenant.content')
    @include('layouts-tenant.footer') 
</div>
</body>
</html>