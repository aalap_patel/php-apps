<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}" class="brand-link">
      @php
        $image =  asset('storage/no-image-available.png');
        if(!empty(domainDetail()->logo)) {
          $image = asset('storage/'.domainDetail()->logo);
        }
      @endphp
      <img src="{{$image}}" alt="{{isset(domainDetail()->name) ? domainDetail()->name : config('app.name')}}" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{isset(domainDetail()->name) ? domainDetail()->name : config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        @php
          $image =  asset('storage/no-image-available.png');
          if(!empty(Auth::user()->profile_image)) {
            $image = asset('storage/'.Auth::user()->profile_image);
          }
        @endphp
        <div class="image">
          <img src="{{$image}}" class="img-circle elevation-2" alt="{{Auth::user()->name}}">
        </div>
        <div class="info">
          <a href="{{route('tenant.profile',['subdomain_name'=>Request::route('subdomain_name')])}}" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{route('tenant.dashboard',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('dashboard') ? 'active' : '' }} {{ Request::is('dashboard/profile') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @can('user_list')
          <li class="nav-item">
            <a href="{{route('tenant.user',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('user') ? 'active' : '' }} {{ Request::is('user/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Manage Users
              </p>
            </a>
          </li>
          @endcan
          @can('role_list')
          <li class="nav-item">
            <a href="{{route('tenant.role',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('role') ? 'active' : '' }} {{ Request::is('role/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-lock"></i>
              <p>
                Manage Roles
              </p>
            </a>
          </li>
          @endcan
          @can('stage_list')
          <li class="nav-item">
            <a href="{{route('tenant.stage',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('stage') ? 'active' : '' }} {{ Request::is('stage/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-file-export"></i>
              <p>
                Manage Stages
              </p>
            </a>
          </li>
          @endcan
          @can('contact_list')
          <li class="nav-item">
            <a href="{{route('tenant.contact',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('contact') ? 'active' : '' }} {{ Request::is('contact/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                Manage Contacts
              </p>
            </a>
          </li>
          @endcan
          @can('product_list')
          <li class="nav-item">
            <a href="{{route('tenant.product',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('product') ? 'active' : '' }} {{ Request::is('product/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Manage Products
              </p>
            </a>
          </li>
          @endcan
          @can('lead_list')
          <li class="nav-item">
            <a href="{{route('tenant.lead',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('lead') ? 'active' : '' }} {{ Request::is('lead/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Manage Leads
              </p>
            </a>
          </li>
          @endcan
          @can('deal_list')
          <li class="nav-item">
            <a href="{{route('tenant.role',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('deal') ? 'active' : '' }} {{ Request::is('deal/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-calendar-week"></i>
              <p>
                Manage Deals
              </p>
            </a>
          </li>
          @endcan
          @can('report_list')
          <li class="nav-item">
            <a href="{{route('tenant.role',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('report') ? 'active' : '' }} {{ Request::is('report/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-download"></i>
              <p>
                Reports Generation
              </p>
            </a>
          </li>
          @endcan
          @can('notification_list')
          <li class="nav-item">
            <a href="{{route('tenant.role',['subdomain_name'=>Request::route('subdomain_name')])}}" class="nav-link {{ Request::is('notification') ? 'active' : '' }} {{ Request::is('notification/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-comment-dots"></i>
              <p>
                Notifications & Alert Management
              </p>
            </a>
          </li>
          @endcan
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
