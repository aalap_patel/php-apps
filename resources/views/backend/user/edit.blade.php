@extends('layouts.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Edit User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
         
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="editData" enctype="multipart/form-data" method="post" action="{{route('user-update')}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" required="required" class="form-control" id="name" name="name" maxlength="150" minlength="2" placeholder="Name" value="{{$record->name}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Email<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" maxlength="100" minlength="5" id="email" placeholder="Email" value="{{$record->email}}" readonly="" disabled="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Role<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2" required="" name="role" id="role" style="width: 100%;">
                        <option value="">Select Role</option>
                        @if($roles->count()>0)
                          @foreach($roles as $role)
                            <option value="{{$role->id}}" {{in_array($role['id'],$recordRole) ? 'selected' : '' }}>{{$role->name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 col-form-label">Select Permissions</label>                           
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="row">
                                @if(count($permissionArray))
                                <div>
                                <input type="checkbox" required="" id="select_all_permissions" />
                                Select All</div>
                                  @foreach($permissionArray as $permissionName=>$permissonValue)                                    
                                   <div class="col-sm-12">
                                     <label class="col-sm-12"> {{ucfirst($permissionName)}} </label>
                                     @foreach($permissonValue as $permission)
                                     <div class="checkbox form-check-inline">
                                       <label>
                                         <input type="checkbox" class="flat" name="permissions[]" value="{{$permission['id']}}" {{in_array($permission['id'],$recordPermissions) ? 'checked' : '' }}>
                                         {{$permission['name']}}</label>
                                     </div>
                                     @endforeach
                                   </div>
                                    @endforeach
                                  @endif  
                                </div>                                  
                            </div>                         
                        </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('user')}}" class="btn btn-default">Cancel</a>
                      <input type="hidden" name="id" value="{{$record->id}}">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<script language="javascript">
    $('#editData').validate();
    
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $("#select_all_permissions").on('click',function() {   
      if($(this).prop("checked")){
        $("input[type=checkbox].flat").prop("checked",true);
      }else{
        $("input[type=checkbox].flat").prop("checked",false);
      }
    });
    

</script> 
@stop