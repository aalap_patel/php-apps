@extends('layouts.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Add User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
          
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Add User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="addData" name="addData" enctype="multipart/form-data" method="post" action="{{route('user-store')}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required="" id="name" name="name" maxlength="150" minlength="2" placeholder="Name" value="{{old('name')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Email<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" required="" maxlength="100" minlength="5" id="email" name="email" placeholder="Email" value="{{old('email')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Password<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" required="" id="password" name="password" placeholder="Password" minlength="5" maxlength="20" value="{{old('password')}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Is Verified</label>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="is_verified" class="custom-control-input" id="is_verified">
                      <label class="custom-control-label" for="is_verified"><small>If you don't need to email verification than make it check.</small></label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Role<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control select2 getPermission" required="" name="role" id="name" style="width: 100%;">
                        <option value="">Select Role</option>
                        @if($roles->count()>0)
                          @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="permissionListing">
                    @include('backend/user/permission-ajax') 
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('user')}}" class="btn btn-default">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<script language="javascript">
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $('.getPermission').on('change',function(){
      var id = $(this).val();
       $.ajax({
         method: 'get',
         url: "{{route('user-get-permission')}}",
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: {
             id: id,
             _token: $('#_token').val()
         }
      }).then(function (data) {
        $(".permissionListing").html(data);
      }).fail(function (data) {
          alertGritter('Permission issue', 'Get permission failed', 'growl-error');
      });  
    });

   $('#addData').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      name: {
        required: true
      },
      password: {
        required: true,
        minlength: 5,
        maxlength: 20
      }
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  }); 

  });

	
</script> 
@stop