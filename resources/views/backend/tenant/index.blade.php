@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tenants</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
     @include('backend.errors')
    <!-- Main content -->
    <section class="content">
      <div class="card collapsed-card">
              <div class="card-header">
                <h3 class="card-title">Filter Options</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Click to Collapse filter option">
                    <i class="fas fa-plus"></i>
                  </button>
                  <a class="btn btn-primary btn-xs" title="Click to add new Tenant" href="{{route('tenant')}}">
                      <i class="fas fa-redo-alt">
                      </i>
                      Reset
                  </a>
                  <a class="btn btn-primary btn-xs" title="Click to add new Tenant" href="{{route('tenant-create')}}">
                      <i class="fas fa-folder">
                      </i>
                      Add New
                  </a>
                  
                </div>
              </div>
              <div class="card-body">
                <div class="col-12">
                    <form name="form_filter" id="form_filter" action="{{route('tenant')}}" class="form-horizontal form-label-left input-mask" novalidate="novalidate" method="get">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">                                        
                                        <label class="control-label" for="customer-id">Status</label>
                                        <select class="form-control custom-select" name="status" id="status_filter" data-placeholder="">
                                            <option value="">All</option>
                                            <option value="1" {{request()->status=="1" ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{request()->status=="0" ? 'selected' : '' }}>Inactive</option>
                                            <option value="2" {{request()->status=="2" ? 'selected' : '' }}>Deleted</option>                         
                                        </select>
                                    </div>
                                </div>
                                <div class="control col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="signup-date">From Creation Date </label>
                                        <div class="input-group date" id="from_date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#from_date"/ placeholder="To:YYYY-MM-DD" value="{{request()->from_date}}">
                                        <div class="input-group-append" data-target="#from_date" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>

                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label" for="signup-date">To Creation Date </label>                            
                                        <div class="input-group date" id="to_date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#to_date"/ placeholder="To:YYYY-MM-DD" value="{{request()->to_date}}">
                                        <div class="input-group-append" data-target="#to_date" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">                                        
                                        <label class="control-label" for="fisrt-name">Search By Term </label>
                                        <select class="form-control custom-select" name="search_by" data-placeholder="Search by">
                                            <option value="name" {{request()->search_by=='name' ? 'selected' : '' }} >Name</option>
                                            <option value="domain" {{request()->search_by=='domain' ? 'selected' : '' }} >Domain</option>
                                            <option value="database" {{request()->search_by=='database' ? 'selected' : '' }} >DataBase</option> 
                                        </select>
                                    </div>  
                                    <div class="form-group">
                                        <label class="control-label" for="last-name">Search String</label>
                                        <input type="text" name="search_str" id="search_str" class="form-control col-md-7 col-xs-12" placeholder="Enter Search String" autocomplete="off" value="{{request()->search_str}}">
                                    </div>                                        
                                </div>            
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="form-group float-right">  
                                        <button type="submit" class="btn btn-info">Search</button> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                                                         
                </div>
              </div>
              <!-- /.card-body -->
             </div>

      <!-- Default box -->
      <div class="card">
        
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Name
                      </th>
                      <th style="width: 20%">
                          Domain
                      </th>
                      <th style="width: 20%">
                          Database
                      </th>
                      <th style="width: 8%" class="text-center">
                          Status
                      </th>
                      <th style="width: 8%" class="text-center">
                          Database
                      </th>
                      <th style="width: 20%">
                        Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                 @forelse($records as $record)

                  <tr id="record_{{$record->id}}">
                      <td>
                         {{$record->id}} 
                      </td>
                      <td>
                          {{$record->name}}
                          <p><small>
                              Created : {{$record->created_at}}
                          </small></p>
                      </td>
                      <td>
                        {{$record->domain}}  
                      </td>
                      <td>
                         {{$record->database}} 
                      </td>
                      <td class="project-state">
                        @if(!$record->trashed())
                          <a href="javascript:void();" title="Click to change status."><span id="status{{ $record->id }}" onclick="change_status('{{ $record->id }}');" class="badge badge-{{ $record->status == 1 ? 'success':'warning' }}">{{ $record->status == 1 ? 'Active':'Inactive'}}</span></a>
                        @else
                          <span class="badge badge-danger">Deleted</span>
                        @endif  
                      </td>
                      <td class="database-create">
                        @if($record->database_status==0)
                          <a href="javascript:void();" title="Click to Create Database and Subdomain."><span id="create_db{{ $record->id }}" onclick="create_database('{{ $record->id }}');" class="badge badge-danger }}">Create Databse</span></a>
                        @else
                          <span class="badge badge-success" title="Database and subdomain are created successfully">Database Created</span>
                        @endif  
                      </td>
                      <td>
                          <a class="btn btn-primary btn-xs" title="Show Tenant Payment detail." href="{{route('tenant-show',$record->id).'?'.Request::getQueryString()}}">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          <a class="btn btn-info btn-xs" href="{{route('tenant-edit',$record->id).'?'.Request::getQueryString()}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a data-id="{{$record->id}}" class="btn btn-danger btn-xs DataDelete">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                  <div id="overlay" style="display:none;">
                      <div class="spinner"></div>
                      <br/>
                      Loading...
                  </div>
                 @empty
                    <tr><td colspan="100">No Record</td></tr>
                @endforelse 
              </tbody>
          </table>

        </div>
        <!-- /.card-body -->
          <div class="card-footer clearfix">
           {{$records->links()}}
          </div>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  $(document).ready(function(){ 

        $('#from_date,#to_date').datetimepicker ({
          format: 'YYYY-MM-DD',
          showClear:true,
        });

    });
  </script>
  <script type="text/javascript" src="{{asset('js/admin-js/tenant.js')}}"></script>
@stop