@extends('layouts.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Tenant</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Edit Tenant</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       @include('backend.errors')
      <div class="container-fluid">
        <div class="row">
         
          <!-- left column -->
          <div class="col-md-12">
           <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit Tenant</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" id="ediData" name="addData" enctype="multipart/form-data" method="post" action="{{route('tenant-update')}}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="name" name="name" maxlength="150" minlength="2" placeholder="Name" value="{{$record->name}}">
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Email<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control email" maxlength="100" minlength="5" id="email" name="email" placeholder="Email" value="{{$record->email}}" readonly="" disabled="">
                    </div>
                  </div> -->
                  <!-- <div class="form-group">
                    <label for="inputPassword3" class="col-sm-4 col-form-label">Password<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" required="" id="password" name="password" placeholder="Password" minlength="5" maxlength="20" value="{{old('password')}}">
                    </div>
                  </div> -->
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Domain<span class="red">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control"  id="domain" name="domain" placeholder="Domain" maxlength="100" minlength="2" value="{{$record->domain}}" readonly="" disabled="">
                      <small>Enter your domain name only. <b>ex. (abc).maildomain.com</b></small>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Database</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="database" name="database" placeholder="Database" maxlength="100" minlength="2" value="{{$record->database}}" readonly="" disabled="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <input type="submit" class="btn btn-info" value="Submit" name="submit">
                      <a href="{{route('tenant')}}" class="btn btn-default">Cancel</a>
                      <input type="hidden" name="id" value="{{$record->id}}">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </form>
              
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> 
<script language="javascript">
  $('#ediData').validate({
    rules: {
      name: {
        required: true
      },
    },
    messages: {
      name: {
        required: "Please enter a name"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
</script> 
@stop