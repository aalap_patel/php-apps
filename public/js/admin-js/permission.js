/**
 * Tenant Module
 */

'use strict';

	function change_status(id) {
		if($(".project-state #status"+id).hasClass('badge-success')){
			var new_status = 0;	
		}else{
			var new_status = 1;
		}
		
		if (confirm('Are you sure you want to change Status?')) {
		   $.ajax({
			   method: 'PUT',
			   url: 'permission/mode',
			   headers: {
				   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			   },
			   data: {
				   id: id,
				   status: new_status,
				   _token: $('#_token').val()
			   }
		   }).then(function (data) {
				if(new_status==0){
					$(".project-state #status"+id).text('Inactive');
					$(".project-state #status"+id).removeClass("badge-success");
					$(".project-state #status"+id).addClass("badge-warning");
				}else{
					$(".project-state #status"+id).text('Active');
					$(".project-state #status"+id).removeClass("badge-warning");
					$(".project-state #status"+id).addClass("badge-success");
				}
				alertGritter('Update Status', 'Status Update success', 'growl-success');
		   }).fail(function (data) {
		   		alertGritter('Update Status', 'Status Update failed', 'growl-error');
		   });
	   }
   	}

    $(document).ready(function () {
    $('.DataDelete').on('click', function () { 
           var id = $(this).data('id');
           if (confirm('Are you sure you want to delete this?')) {
               $.ajax({
                   method: 'DELETE',
                   url: 'permission/destroy',
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
                   data: {
                       id: id,
                       _token: $('#_token').val()
                   }
               }).then(function (data) {
                   $('#record_' + id).fadeOut();
                    alertGritter('Delete Permission', 'Permission Deleted successfully', 'growl-success');
               }).fail(function (data) {
                    alertGritter('Delete Permission', 'Permission Deleted failed', 'growl-error');
               });
           }
    });
});
    





