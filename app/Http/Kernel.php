<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

           // \Spatie\Multitenancy\Http\Middleware\NeedsTenant::class,
           // \Spatie\Multitenancy\Http\Middleware\EnsureValidTenantSession::class,
        ],

        'api' => [
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Illuminate\Session\Middleware\StartSession::class,
            //\Illuminate\View\Middleware\ShareErrorsFromSession::class,
        ],

        'tenant' => [
           \Spatie\Multitenancy\Http\Middleware\NeedsTenant::class,
           \Spatie\Multitenancy\Http\Middleware\EnsureValidTenantSession::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'tenantverified' => \App\Http\Middleware\TenantEnsureEmailIsVerified::class,
        'subdomain.tenantverified' => \App\Http\Middleware\Tenant\TenantEnsureEmailIsVerified::class,
        'subdomain.api.tenantverified' => \App\Http\Middleware\API\TenantEnsureEmailIsVerified::class,
        'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
        'auth.tenant' => \App\Http\Middleware\Tenant\Authenticate::class,
        'guest.tenant' => \App\Http\Middleware\Tenant\RedirectIfAuthenticated::class,
        'checkLeadId' => \App\Http\Middleware\Tenant\GetLeadId::class,
        'admin.auth' => \App\Http\Middleware\Admin\Authenticate::class,
        'admin.guest' => \App\Http\Middleware\Admin\RedirectIfAuthenticated::class,
        'auth-api' => \App\Http\Middleware\API\Authenticate::class,
    ];
}
