<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use DB;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {   
        if ($request->is('admin/*')) {
            return view('auth.login');
        }
        return view('frontend.auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        if ($request->is('admin/*')) {
            $request->authenticate();
        }
        
        //change some modification for login
        $request->frontAuthenticate();

        $request->session()->regenerate();

        if ($request->is('admin/*')) {
            return redirect()->intended(RouteServiceProvider::HOME);
        }
        return redirect()->intended(RouteServiceProvider::FRONTLOGINHOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($request->is('admin/*')) {
            return redirect('/admin');
        }
        return redirect('/');
    }
}
