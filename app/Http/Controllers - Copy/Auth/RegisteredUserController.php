<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Tenant;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Str;
use Carbon\Carbon;




class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {   
        if ($request->is('admin/*')) {
            return view('auth.register');
        }
        return view('frontend.auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|confirmed|min:5|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$@&#%]).*$/|',
            'domain' => 'required|string|max:25|regex:/^[\w-]*$/|unique:tenants',
        ]);
        
        $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'is_admin' => 1,
                ]);

        //create entry on Tenant Table 
        $tenant = Tenant::create([
                    'user_id' => $user->id,
                    'name' => $request->name,
                    'domain' => $request->domain.'.'.env('SITE_URL'),
                    'database' => config('app.tenantbdprifix').'_'.$user->id.'_'.Str::random(5),
                    'status' =>1,
                    'created_at' =>Carbon::now()
                ]);

        //event(new Registered($user));
        //Auth::login($user);
        
        $user->sendTenantEmailVerificationNotification();

        if ($request->is('admin/*')) {
            return redirect(RouteServiceProvider::HOME);
        }
        
        //return redirect(RouteServiceProvider::FRONTHOME);
        return redirect()->route('front-login')->with('status','You have successfully registrer, please verified your email to login');
    }

    
}
