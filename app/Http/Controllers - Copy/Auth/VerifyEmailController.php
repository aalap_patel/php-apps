<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class VerifyEmailController extends Controller
{
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Foundation\Auth\EmailVerificationRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke($id,$hash,Request $request)
    {   
        if(!Auth::check()){
            $user = User::where('id',$id)->whereNull('email_verified_at')->first();
            if(!$user){
                return redirect()->route('front-login')->withErrors(['Link expired or invalid']);
            }
            
            if (! hash_equals((string) $hash,
                          sha1($user->email))) {
                return redirect()->route('front-login')->withErrors(['Link expired or invalid']);
            }

            if ($user->markEmailAsVerified()) {
                event(new Verified($user));
                return redirect()->route('front-login')->withStatus('Successfully verified. Please login continue');
            }
            return redirect()->route('front-login')->withErrors(['Link expired or invalid']);
        }
        
       
        if (! hash_equals((string) $id,
                          (string) $request->user()->getKey())) {
            return redirect()->route(RouteServiceProvider::FRONTLOGINHOME)->withErrors(['Link expired or invalid']);
        }

        if (! hash_equals((string) $hash,
                          sha1($request->user()->email))) {
            return redirect()->route(RouteServiceProvider::FRONTLOGINHOME)->withErrors(['Link expired or invalid']);
        }

        if ($request->user()->hasVerifiedEmail()) {
            return redirect()->route(RouteServiceProvider::FRONTLOGINHOME)->withErrors(['Link expired or invalid']);
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
            return redirect()->route(RouteServiceProvider::FRONTLOGINHOME)->withStatus('Successfully verified');
        }

        return redirect()->route(RouteServiceProvider::FRONTLOGINHOME)->withErrors(['Link expired or invalid']);



        // if ($request->user()->hasVerifiedEmail()) {
        //     return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
        // }

        // if ($request->user()->markEmailAsVerified()) {
        //     event(new Verified($request->user()));
        // }

        // return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
    }
}
