<?php

namespace App\Http\Controllers\Tenant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Stage;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;

class StageController extends Controller
{
    protected $model;

    public function __construct(){
        $this->model = new Stage;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        
        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();  
        return view('tenant/stage/index', compact('records'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        return view('tenant/stage/add');
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $request->only('name', 'hierarchy');
        $this->validate($request, 
            [
                'name'=> ['required','not_regex:/^(.)\1{2,}$/i','unique:stages,name,null,id,deleted_at,NULL'],
                'hierarchy' => 'required',
                
            ]);
        
        $this->model::create([
            'name' => $request->name,
            'hierarchy' => $request->hierarchy,
            'created_at' =>Carbon::now(),
            'status'=>1,
        ]);

        return redirect()->route('tenant.stage',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Stage Successfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id)
    {   
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:stages',
        ]);

        $record = $this->model::where('id',$request->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        return view('tenant/stage/edit',compact('record')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
         $request->validate([
            'id'=>'required|numeric|exists:stages',
            'name'=> ['required','not_regex:/^(.)\1{2,}$/i','unique:product,name,'.$request->id.',id,deleted_at,NULL'],
            'hierarchy' => 'required',
        ]);
        
        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $request->only('name', 'hierarchy','id');

        $requestData = $request->all();
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        return redirect()->route('tenant.stage',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:stages',
        ]);

        $record = $this->model::where('id',$request->id)->first();

        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:stages',
        ]);

        $data = $this->model::whereId($request->id)->first();
       
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        $data->delete();
        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }
}