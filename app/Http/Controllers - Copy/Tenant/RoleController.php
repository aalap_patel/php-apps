<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

class RoleController extends Controller
{
    protected $model;
    public function __construct(){

        $this->model = new Role;
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request){
        $records = $this->model;
        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && $request->search_by=='name'){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');
        }
        $records = $records->latest()->paginate(config('app.paginate'))->withQueryString();     
        return view('tenant/role/index',compact('records'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        return view('tenant.role.add',compact('permissionArray'));
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
    	$request->validate([
	        'name' => 'required|string|max:255|min:2|unique:roles',
	    ]);
    	$inputs = $request->only('name');
    	$record = $this->model::create($inputs);
        $record->givePermissionTo([$request->permissions]);
        return redirect()->route('tenant.role',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Successfully created...');
    }

    /**
     * Display edit view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id){
        $record = $this->model::where('id',$id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        //dd(Config('constants.exceptPermissionArry'));
        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $recordPermissions = $record->permissions->pluck('id')->toArray();

    	return view('tenant.role.edit',compact('record','permissionArray','recordPermissions'));
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {   
    	$request->validate([
            'id'=>'bail|required',
    		'name' => 'required|string|max:255|min:2|unique:roles,name,'.$request->id,	       
    	]);
        
    	$record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
    	$record->name = $request->name;
        $record->updated_at = Carbon::now();
    	$record->update();
        
        $rolePermission = $record->getAllPermissions()->pluck('name')->toArray();
        if( is_array($request->permissions) && count($request->permissions) ){
            $record->revokePermissionTo($rolePermission);
            $record->givePermissionTo([$request->permissions]);          
        }else{
            $record->revokePermissionTo($rolePermission);
        }

        return redirect()->route('tenant.role',['subdomain_name'=>$request->route('subdomain_name'),$request->queryString])->withSuccess('Successfully updated...');
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:roles',
        ]);

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:roles',
        ]);

        $data = $this->model::whereId($request->id)->first();
        if(empty($data)){
            return response()->json(['error' => true,'message'=>'record not found'], 300);
        }
        $data->delete();
        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }
}
