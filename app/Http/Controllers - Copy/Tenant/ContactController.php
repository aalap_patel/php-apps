<?php

namespace App\Http\Controllers\Tenant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Contact;
use App\Models\Country;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;

class ContactController extends Controller
{
    protected $model;

    public function __construct(){
        $this->model = new Contact;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

         if($request->user_id){
            $records = $records->where('user_id',$request->user_id);
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name' || $request->search_by=='primary_email' || $request->search_by=='title' || $request->search_by=='phone1')){
            if($request->search_by=='name'){
                $records = $records->where(function($q) use($request){
                    $q->whereRaw("CONCAT(`first_name`, ' ', `last_name`) LIKE ?", ["%".$request->search_str."%"])
                    ->orWhereRaw("CONCAT(`last_name`, ' ', `first_name`) LIKE ?", ["%".$request->search_str."%"]);
                });
            }elseif($request->search_by=='phone1'){
                $records = $records->where($request->search_by,'LIKE',$request->search_str);
            }else{
                $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');
            } 
        }
        $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();
        $users = User::Select('id','name')->get();     
        return view('tenant/contact/index', compact('records','users'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        return view('tenant/contact/add',compact('countries'));
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $request->only('salutation', 'first_name', 'last_name','profile_pic','title','department','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','primary_email','secondry_email','facebook_url','twitter_url','linkedin_url','instagram_url');

        $this->validate($request, 
            [
                'salutation' => 'required|string|max:150|min:2',
                'first_name' => 'required|string|max:150|min:2',
                'last_name' => 'required|string|max:150|min:2',
                'profile_pic' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'title' => 'required|string|max:255|min:2',
                'department' => 'required|string|max:255|min:2',
                'address1'=> 'required',
                'city'=> 'required',
                'state'=> 'required',
                'country_id'=> 'required|numeric',
                'zipcode'=> 'required',
                'phone1'=> 'required|numeric',
                'primary_email' => 'required|email|unique:contact,primary_email,null,id,user_id,'.Auth::user()->id.',deleted_at,NULL',
            ]);
        
        $this->model::create([
            'user_id' => Auth::user()->id,
            'salutation' => $request->salutation,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'profile_pic' => isset($request->profile_pic) ? Storage::disk('public')->put($request->route('subdomain_name').'/contact', $request->profile_pic) : '',
            'title' => $request->title,
            'department' => $request->department,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state,
            'country_id' => $request->country_id,
            'zipcode' => $request->zipcode,
            'phone1' => $request->phone1,
            'phone2' => $request->phone2,
            'primary_email' => $request->primary_email,
            'secondry_email' => $request->secondry_email,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'linkedin_url' => $request->linkedin_url,
            'instagram_url' => $request->instagram_url,
        ]);

        return redirect()->route('tenant.contact',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Contact susscessfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id)
    {   
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:contact',
        ]);
        $record = $this->model::where('user_id',Auth::user()->id)->where('id',$request->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        return view('tenant/contact/edit', compact('record','countries')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
         
        $request->only('salutation', 'first_name', 'last_name','profile_pic','title','department','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','primary_email','secondry_email','facebook_url','twitter_url','linkedin_url','instagram_url');

        $this->validate($request, [   
            'id'=>'required|numeric|exists:contact',
            'salutation' => 'required|string|max:150|min:2',
            'first_name' => 'required|string|max:150|min:2',
            'last_name' => 'required|string|max:150|min:2',
            'profile_pic' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required|string|max:255|min:2',
            'department' => 'required|string|max:255|min:2',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|numeric',
            'primary_email' => 'required|email|unique:contact,primary_email,'.$request->id.',id,user_id,'.Auth::user()->id.',deleted_at,NULL',
        ]);

        $record = $this->model::where('id',$request->id)->where('user_id',Auth::user()->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $requestData = $request->all();
        
        $image = $request->file('profile_pic');
        if(isset($image) && !empty($image)) {
            $requestData['profile_pic'] = Storage::disk('public')->put($request->route('subdomain_name').'/contact', $image);
            //Delete old image
            Storage::disk('public')->delete($request->route('subdomain_name').'/contact',$record->profile_pic);
        }else{
            $requestData['profile_pic'] = $record->profile_pic;
        }

        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        return redirect()->route('tenant.contact',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:contact',
        ]);

        $record = $this->model::where('id',$request->id)->where('user_id',Auth::user()->id)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:contact',
        ]);

        $data = $this->model::where('id',$request->id)->where('user_id',Auth::user()->id)->first();
       
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        $data->delete();
        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
    }

}