<?php

namespace App\Http\Controllers\Tenant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Product;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;

class ProductController extends Controller
{
    protected $model;

    public function __construct(){
        $this->model = new Product;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        if($request->user_id){
            $records = $records->where('user_id',$request->user_id);
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name' || $request->search_by=='product_code' || $request->search_by=='price')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->orderBy('id','DESC')->with('addedBy')->paginate(config('app.paginate'))->withQueryString();  
        $users = User::Select('id','name')->get();
        return view('tenant/product/index', compact('records','users'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        return view('tenant/product/add');
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $request->only('name', 'description', 'product_code','price');
        $this->validate($request, 
            [
                'name'=> ['required','not_regex:/^(.)\1{2,}$/i','unique:product,name,null,id,user_id,'.Auth::user()->id.',deleted_at,NULL'],
                'product_code' => 'required',
                'price'=> 'required|numeric',
                
            ]);
        
        $this->model::create([
            'name' => $request->name,
            'description' => $request->description,
            'product_code' => $request->product_code,
            'price' => $request->price,
            'created_at' =>Carbon::now(),
            'status'=>1,
            'user_id'=>Auth::user()->id,
        ]);

        return redirect()->route('tenant.product',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Product Successfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id)
    {   
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:product',
        ]);

        $record = $this->model::with('addedBy')->where('id',$request->id)->where('user_id',Auth::user()->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        return view('tenant/product/edit',compact('record')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
         $request->validate([
            'id'=>'required|numeric|exists:product',
            'name'=> ['required','not_regex:/^(.)\1{2,}$/i','unique:product,name,'.$request->id.',id,user_id,'.Auth::user()->id.',deleted_at,NULL'],
            'product_code' => 'required',
            'price'=> 'required|numeric',
        ]);
        
        $record = $this->model::with('addedBy')->where('id',$request->id)->where('user_id',Auth::user()->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $request->only('name', 'description', 'product_code','price','id');

        $requestData = $request->all();
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        return redirect()->route('tenant.product',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:product',
        ]);

        $record = $this->model::where('id',$request->id)->where('user_id',Auth::user()->id)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:product',
        ]);

        $data = $this->model::whereId($request->id)->where('user_id',Auth::user()->id)->first();
       
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        $data->delete();
        //write a code for when tenant delete at time user also delete and delete their database

        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }
}