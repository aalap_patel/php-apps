<?php

namespace App\Http\Controllers\Tenant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Lead;
use App\Models\Task;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;

class LeadTaskController extends Controller
{
    protected $model;

    public function __construct(){
        $this->model = new Task;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   
        $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('due_date','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('due_date','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='title')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->where('lead_id',$request->leadId)->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();
        $leadId = $request->leadId;  
        return view('tenant/task/index', compact('records','leadId'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {   $leadId = $request->leadId;
        return view('tenant/task/add',compact('leadId'));
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $request->only('title', 'description', 'due_date');
        $this->validate($request, 
            [
                'title'=> ['required','unique:task,title,null,id,lead_id,'.$request->leadId.',deleted_at,NULL'],
                'description' => 'required',
                'due_date'=> 'required',
                
            ]);

        $this->model::create([
            'title' => $request->title,
            'description' => $request->description,
            'due_date' => $request->due_date,
            'lead_id' => $request->leadId,
            'created_at' =>Carbon::now(),
            'status'=>1,
        ]);

        return redirect()->route('tenant.lead-task',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Task Successfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id)
    {  
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:task',
        ]);

        $record = $this->model::where('lead_id',$request->leadId)->where('id',$request->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        $leadId = $request->leadId;

        return view('tenant/task/edit',compact('record','leadId')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
         $request->validate([
            'id'=>'required|numeric|exists:task',
            'title'=> ['required','unique:task,title,'.$request->id.',id,lead_id,'.$request->leadId.',deleted_at,NULL'],
            'description' => 'required',
            'due_date'=> 'required',
        ]);
        
        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $request->only('title', 'description', 'due_date','leadId','id');

        $requestData = $request->all();
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        return redirect()->route('tenant.lead-task',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:task',
        ]);

        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:task',
        ]);

        $data = $this->model::whereId($request->id)->where('lead_id',$request->leadId)->first();
       
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        $data->delete();
        //write a code for when tenant delete at time user also delete and delete their database

        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }
}