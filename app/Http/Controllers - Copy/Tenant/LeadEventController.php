<?php

namespace App\Http\Controllers\Tenant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Lead;
use App\Models\Event;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;

class LeadEventController extends Controller
{
    protected $model;

    public function __construct(){
        $this->model = new Event;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   
        $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        $fromStartDate = $request->from_start_date;
        $toStartDate = $request->to_start_date;

        if(!empty($fromStartDate)){
            $fromStartDate .= ' 00:00:00';
            $records = $records->whereDate('start_date','>=',$fromStartDate);
        }
        if(!empty($toStartDate)){
            $toStartDate .= ' 23:59:59';
            $records = $records->where('start_date','<=',$toStartDate);
        }

        $fromEndDate = $request->from_end_date;
        $toEndDate = $request->to_end_date;

        if(!empty($fromEndDate)){
            $fromEndDate .= ' 00:00:00';
            $records = $records->whereDate('end_date','>=',$fromEndDate);
        }
        if(!empty($toEndDate)){
            $toEndDate .= ' 23:59:59';
            $records = $records->where('end_date','<=',$toEndDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->where('lead_id',$request->leadId)->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();
        $leadId = $request->leadId;  
        return view('tenant/event/index', compact('records','leadId'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {   $leadId = $request->leadId;
        return view('tenant/event/add',compact('leadId'));
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $request->only('name', 'description', 'start_date','end_date');
        $this->validate($request, 
            [
                'name'=> ['required','unique:event,name,null,id,lead_id,'.$request->leadId.',deleted_at,NULL'],
                'description' => 'required',
                'start_date'=> 'required',
                'end_date'=> 'required',
                
            ]);

        $this->model::create([
            'name' => $request->name,
            'description' => $request->description,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'lead_id' => $request->leadId,
            'created_at' =>Carbon::now(),
            'status'=>1,
        ]);

        return redirect()->route('tenant.lead-event',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Event Successfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id)
    {  
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:event',
        ]);

        $record = $this->model::where('lead_id',$request->leadId)->where('id',$request->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        $leadId = $request->leadId;

        return view('tenant/event/edit',compact('record','leadId')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {   
         $request->validate([
            'id'=>'required|numeric|exists:event',
            'name'=> ['required','unique:event,name,'.$request->id.',id,lead_id,'.$request->leadId.',deleted_at,NULL'],
            'description' => 'required',
            'start_date'=> 'required|date_format:Y-m-d|after_or_equal:today',
            'end_date'=> 'required|date_format:Y-m-d|after_or_equal:start_date',
            'leadId'=> 'required|numeric',
        ]);
        
        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $request->only('name', 'description', 'start_date','end_date','leadId','id');

        $requestData = $request->all();
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        return redirect()->route('tenant.lead-event',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:event',
        ]);

        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:event',
        ]);

        $data = $this->model::whereId($request->id)->where('lead_id',$request->leadId)->first();
       
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        $data->delete();
        //write a code for when tenant delete at time user also delete and delete their database

        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }
}