<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\Rules;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Tenant;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;

class UserController extends Controller
{
    protected $model;

    public function __construct(){
        $this->model = new User;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name' || $request->search_by=='email')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();     
        return view('backend/user/index', compact('records'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $permissions = Permission::orderBy('name','DESC')->get();
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $roles = Role::where('status',1)->orderBy('name','DESC')->get();
        $recordPermissions = [];
        return view('backend/user/add',compact('permissionArray','roles','recordPermissions'));
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {  
        $this->validate($request, 
            [
                'name' => 'required|string|max:100',
                'email' => 'required|unique:users',
                'password' => 'required|min:5|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$@&#%]).*$/|',
                'permissions'=>'nullable|array',
                'role'=>'required',
            ]);
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'email_verified_at' => isset($request->is_verified) ? Carbon::now() : NULL,
            'created_at' =>Carbon::now(),
            'is_admin' =>1,
            'status'=>1
        ]);

        if(is_array($request->permissions) && count($request->permissions)){
            $permissions = Permission::whereIn('id',$request->permissions)->get()->pluck('name')->toArray();
            if(count($permissions)){
                $user->syncPermissions($permissions);
            } 
        }

        $user->assignRole($request->role);
        //event(new Registered($user));

        return redirect()->route('user')->withSuccess('User susscessfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$id)
    {   
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:users',
        ]);
        $record = $this->model::with('permissions')->with('roles')->where('id',$request->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $permissions = Permission::orderBy('id','ASC')->get(); 
        $roles = Role::where('status',1)->orderBy('id','ASC')->get();       
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $recordPermissions = $record->permissions->pluck('id')->toArray();
        $recordRole = $record->roles->pluck('id')->toArray();
        
        return view('backend/user/edit', compact('record','permissionArray','recordPermissions','roles','recordRole')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
        $request->validate([
            'id'=>'required|numeric|exists:users',
            'name' => 'required|string|max:150|min:2',
            'permissions'=>'nullable|array',
            ]);
       
        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        $record->name = $request->name;
        $record->updated_at = Carbon::now();
        $record->update();

        if( is_array($request->permissions) && count($request->permissions) ){
            $permissions = Permission::whereIn('id',$request->permissions ? $request->permissions : [])->get()->pluck('name')->toArray();
            if(count($permissions))
                $record->syncPermissions($permissions);
            else{
                $record->syncPermissions();
                //$record->revokePermissionTo($permissions);
            }            
        }else{
             $record->syncPermissions();
        }
        $record->syncRoles($request->role);
        
        return redirect()->route('user')->withSuccess('Record successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:users',
        ]);

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:users',
        ]);

        $data = $this->model::whereId($request->id)->first();
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        $data->delete();
        //write a code for when tenant delete at time user also delete and delete their database

        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function profile(Request $request)
    {
        $record = Auth::user();
        return view('backend/user/profile',compact('record'));
    }

    /**
     * Request for update data.
     *
     * @return \Illuminate\View\View
     */
    public function updateProfile(Request $request)
    {  
        $request->validate([
            'name' => 'required|string|max:150|min:2'
            ]);
       
        $record = $this->model::where('id',Auth::user()->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        $record->name = $request->name;
        $record->updated_at = Carbon::now();
        $record->update();

        return redirect()->route('profile')->withSuccess('Record successfully updated');
    }


    /**
     * Request for update password.
     *
     * @return \Illuminate\View\View
     */
    public function changePassword(Request $request)
    {   
        
        $request->validate([
            'old_password' => 'required|max:20|min:8',
            'password' => ['required', Rules\Password::defaults()],
            'password_confirm' => 'required|same:password',        
        ]);

        $user = Auth::user();
        if(!Hash::check($request->old_password, $user->password)){
            return redirect()->route('profile')->withErrors('Your old password are not match with record.');   
        }

        $user->forceFill([
            'password' => Hash::make($request->password),
            'remember_token' => Str::random(60),
        ])->save();
        return redirect()->route('profile')->withSuccess('Your password updated successfully');        
    }


    /**
     * Handle permission Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for get permission for role
     *
     * @return \Ajax response
     */
    public function getPermission(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:roles',
        ]);

        $record = Role::where('id',$request->id)->where('status',1)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }
        $recordPermissions = $record->getAllPermissions()->pluck('id')->toArray();  
        
        $permissions = Permission::orderBy('id','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        return Response::json(View('backend/user/permission-ajax', compact('permissionArray','recordPermissions'))->render());
    }
            

}