<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Illuminate\Auth\Events\Registered;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Spatie\Multitenancy\Commands\Concerns\TenantAware;
use Spatie\Multitenancy\Commands\Concerns\TenantsArtisanCommand;
use App\Models\User;
use App\Models\Tenant;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Config;
use Auth;
use File;
use General;
use Mail;
use Response;
use DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TenantController extends Controller
{
    protected $model;

    public function __construct(){
        $this->model = new Tenant;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the tenant view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='domain' || $request->search_by=='name' || $request->search_by=='database')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();

        $masterPermission = Permission::orderBy('name','DESC')->get();

        return view('backend/tenant/index', compact('records'));
    }

    /**
     * Display create tenant view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/tenant/add');
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $this->validate($request, 
            [
                'name' => 'required|string|max:100',
                'email' => 'required|unique:users',
                'password' => 'required|min:5|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$@&#%]).*$/|',
                'domain' =>'required|unique:tenants'
            ]);
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'email_verified_at' => isset($request->is_verified) ? Carbon::now() : NULL
        ]);

        //add tenant
        $tenant = Tenant::create([
            'name' => $request->name,
            'domain' => $request->domain.'.'.env('SITE_URL'),
            'database' => config('app.tenantbdprifix').'_'.$user->id.'_'.Str::random(5),
            'user_id'=> $user->id,
            'status' =>1,
            'created_at' =>Carbon::now()
        ]);

        //event(new Registered($user));

        return redirect()->route('tenant')->withSuccess('Tenant susscessfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$id)
    {   
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:tenants',
        ]);

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        return view('backend/tenant/edit', compact('record')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
        $request->validate([
            'id'=>'bail|required|numeric|exists:tenants',
            'name' => 'required|string|max:150|min:2',
            ]);
        
        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        $record->name = $request->name;
        $record->updated_at = Carbon::now();
        $record->update();

        return redirect()->route('tenant')->withSuccess('Successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:tenants',
        ]);
        
        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:tenants',
        ]);
        $data = $this->model::whereId($request->id)->first();
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        
        $user = Users::where('id',$data->user_id)->first();
        $user->delete();

        //write a code for when tenant delete at time user also delete and delete their database

        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }

    /**
     * Handle an incoming ajax request for create database.
     * Creates a new database schema.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createDatabase(Request $request)
    {  
        $request->validate([
            'id'=>'required|numeric|exists:tenants',
        ]);
        $data = $this->model::whereId($request->id)->first();
        
        if(empty($data)){
            return Response::json(['error' => true,'message'=>'record not found'], 300);
        }

        if($data->database_status==1){
            return Response::json(['error' => true,'message'=>'Database allready created'], 300);
        }

        //get user detail from master and add to tenant user
        if($data->user_id == NULL){
            return Response::json(['error' => true,'message'=>'User not assign in tanant table'], 300);
        }

        $masterUser = User::where('id',$data->user_id)->first();
        $masterPermission = Permission::orderBy('name','DESC')->get();
        

        // We will use the `statement` method from the connection class so that
        // we have access to parameter binding.
        $databaseCreated = DB::statement('CREATE DATABASE '.strtolower($data->database));
        if(!$databaseCreated){
            return Response::json(['error' => true,'message'=>'There was some issue on create database / Database allready exists'], 300);  
        }
        
        //Artisan::call('php artisan tenants:artisan "migrate --database=tenant"');
        //\Artisan::call('tenants:artisan "migrate --database=tenant"');;

        $connectionNameOld = config('database.default');
        $databaseNameOld = config("database.connections.{$connectionNameOld}.database");
        DB::purge();

        config([
            "database.connections.tenant.database" => strtolower($data->database),
        ]);
        config([
             "database.default" => 'tenant',
        ]);
        DB::purge();
        
        try{
            Schema::create('migrations', function (Blueprint $table) {
                $table->id();
                $table->string('migration');
                $table->integer('batch');
            });
            \Artisan::call('migrate --database=tenant');

            //Create a tenant admin entry in tenant database (get from master user table)
            $TenantUser = User::create(['name'=>$masterUser->name,'email'=>$masterUser->email,'email_verified_at'=>Carbon::now(),'password'=>$masterUser->password,'created_at'=>Carbon::now(),'status'=>1,'is_admin'=>1]);

            //add default permission for tenant table
            foreach($masterPermission as $mpermission){
                Permission::create(['name'=>$mpermission->name]);
            } 
            
            
         }catch(\Exception $e){
            config([
                "database.default" => $connectionNameOld,
             ]);
            DB::purge();
            return Response::json(['error' => true,'message'=>'There was some issue on create database'], 300);
        }

        config([
             "database.default" => $connectionNameOld,
         ]);
        DB::purge();
        
        $data->updated_at = Carbon::now();
        $data->database_status = 1;
        $data->update();

        return Response::json(['success' => true,'message'=>'successfully deleted'], 200);  
    }

}