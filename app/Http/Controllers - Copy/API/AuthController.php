<?php 
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\Traits\ApiResponser;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use App\Models\User;
use Carbon\Carbon;
use Response;
use General;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller{

   use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function register(Request $request)
    { 
      $rules = [
         'email'=>'required|unique:users,email,NULL,id,deleted_at,NULL|max:150',
         'password'=>'required|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$@&#%]).*$/|confirmed',
         'name'=>'required|max:100',
      ];

      $messages = [
         'not_regex' => 'The :attribute field must not be in a repetitive patttern.',
      ];

      $validate = Validator::make($request->all(), $rules, $messages);

      if ($validate->fails()) {
         $errormessage = implode("\r\n", $validate->errors()->all());
         return $this->errorResponse($errormessage, 400);
      }
      
      $input = $request->only(['email','name','password']);
      $input['password'] = Hash::make($request->password);
      $input['name'] =$request->name;
      $input['email'] =$request->email;

      $user = User::create($input);
      $user->sendApiTenantEmailVerificationNotification($request->route('subdomain_name'));
      
      $accessToken = $user->createToken('authToken')->accessToken;
      
      return $this->successResponse([ 'user' => $user, 'access_token' => $accessToken],'User Registered successfully');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Json
     */
    public function login(LoginRequest $request)
    { 
      $rules = [
         'email'=>'email|required|exists:users',
         'password'=>'required',
      ];

      $messages = [
         'not_regex' => 'The :attribute field must not be in a repetitive patttern.',
      ];

      $validate = Validator::make($request->all(), $rules, $messages);

      if ($validate->fails()) {
         $errormessage = implode("\r\n", $validate->errors()->all());
         return $this->errorResponse($errormessage, 400);
      }

      try{
         $request->tenantAuthenticate();

         $accessToken = auth()->user()->createToken('authToken')->accessToken;
         return $this->successResponse(['user' => auth()->user(), 'access_token' => $accessToken],'Login successfully');
      }catch(\Exception $e){
         return $this->errorResponse('These credentials do not match with our record OR user status is inactive', 400);
      }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Json
     */
    public function logout(Request $request)
    {
      $request->user()->tokens()->delete();
      return $this->successResponse([],'You have successfully logout');
   }
}