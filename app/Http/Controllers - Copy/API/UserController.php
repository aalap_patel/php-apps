<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\Rules;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Country;
use App\Models\Currency;
use App\Models\DomainDetail;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;
use App\Services\UserService;



class UserController extends Controller
{
    protected $model;
    public $service;
    public function __construct(Request $request, UserService $service){
        $this->service = $service;
        $this->model = new User;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   
        return $this->service->listService($request);
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {   
        return $this->service->createService($request);
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        return $this->service->storeService($request);
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id)
    {   
        return $this->service->editService($request,$subdomain,$id);  
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
         return $this->service->updateService($request); 
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        return $this->service->changeStatusService($request);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        return $this->service->deleteService($request);
        
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function profile(Request $request)
    {
        return $this->service->profileService($request);
    }

    /**
     * Request for update data.
     *
     * @return \Illuminate\View\View
     */
    public function updateProfile(Request $request)
    {  
        return $this->service->updateProfileService($request);
    }


    /**
     * Request for update password.
     *
     * @return \Illuminate\View\View
     */
    public function changePassword(Request $request)
    {   
        return $this->service->changeProfilePasswordService($request);         
    }  


    /**
     * Request for update site data.
     *
     * @return \Illuminate\View\View
     */
    public function changeSiteData(Request $request)
    {   
        return $this->service->changeSiteDataService($request);       
    }  

    /**
     * Handle permission Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for get permission for role
     *
     * @return \Ajax response
     */
    public function getPermission(Request $request)
    {   
        return $this->service->getPermissionService($request);
    }
            

}