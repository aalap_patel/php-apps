<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class EmailVerificationPromptController extends Controller
{
    use ApiResponser;
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {   if($request->user()==NULL){
            return $this->errorResponse('Opps something went wrong, please try again', 400);
        }
        
        return $request->user()->hasVerifiedEmail()
                    ? redirect()->intended(RouteServiceProvider::TENANTHOME)
                    : view('tenant.auth.verify-email');
    
    }
}
