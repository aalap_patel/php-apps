<?php 

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\User;
use Auth;

/**
 * Class EmailVerificationController
 * @package App\Http\Controllers
 */
class EmailVerificationController extends Controller
{

   use ApiResponser;

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Foundation\Auth\EmailVerificationRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendVerificationEmail(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return $this->errorResponse('Already Verified', 400);
        }

        //$request->user()->sendEmailVerificationNotification();
        $request->user()->sendApiTenantEmailVerificationNotification($request->route('subdomain_name'));

        return $this->successResponse([],'verification-link-sent');
    }

    public function verify($sub_domain,$id,$hash,Request $request)
    {    
        // if ($request->user()->hasVerifiedEmail()) {
        //     return $this->errorResponse('Email already verified', 400);
        // }

        // if ($request->user()->markEmailAsVerified()) {
        //     event(new Verified($request->user()));
        // }

        // return $this->successResponse([],'Email has been verified');

        if(!Auth::check()){
            $user = User::where('id',$id)->whereNull('email_verified_at')->first();
            
            if(!$user){
                return $this->errorResponse('Link expired or invalid', 400);
            }
            
            if (! hash_equals((string) $hash,sha1($user->email))) {
                return $this->errorResponse('Link expired or invalid', 400);
            }

            if ($user->markEmailAsVerified()) {
                event(new Verified($user));
                return $this->successResponse([],'Email has been verified. Please login continue');
            }

            return $this->errorResponse('Link expired or invalid', 400);
        }
        
        
        if (! hash_equals((string) $id,(string) $request->user()->getKey())) {
            return $this->errorResponse('Link expired or invalid', 400);
        }

        if (! hash_equals((string) $hash,sha1($request->user()->email))) {
            return $this->errorResponse('Link expired or invalid', 400);
        }

        if ($request->user()->hasVerifiedEmail()) {
            return $this->errorResponse('Link expired or invalid', 400);
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
            return $this->successResponse([],'Successfully verified');
        }
        
        return $this->errorResponse('Link expired or invalid', 400);
    }
}