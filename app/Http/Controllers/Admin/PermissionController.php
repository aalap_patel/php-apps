<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Guard;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    protected $model;
    public function __construct(){

        $this->model = new Permission;
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request){
        $records = $this->model;
        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && $request->search_by=='name'){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');
        }
        $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();     
        return view('backend.permission.index',compact('records'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        return view('backend.permission.add');
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        if(!isset($request->bulk)){
            $request->validate([
                'name' => 'required|alpha_dash|max:255|min:2|unique:permissions',
            ]);
        }
        $count=0;
        $inputs = str_replace(' ', '', $request->only('name'));
        if(isset($request->bulk)){
            $exp = explode(',',str_replace(' ', '', $request->name));
            if(count($exp)>1){
                foreach($exp as $exppermission){
                    //check id exists data
                    if(preg_match('/^[a-z]+_[a-z]+$/i', $exppermission)){
                        $data=$this->model::where('name',$exppermission)->first(); 
                        if(empty($data)){
                            $attributes['name'] = $exppermission;
                            $record = $this->model::create($attributes);
                        }
                    }else{
                        $count++;
                    }
                }
            }
        }else{
            if(preg_match('/^[a-z]+_[a-z]+$/i', $inputs['name'])){
                $record = $this->model::create($inputs); 
            }else{
              $count++;  
            }  
        }
        $message = $count ? $count.' Formate are invalid and other ':'';
        return redirect()->route('permission')->withSuccess($message .'Permission successfully created...');
    }

    /**
     * Display edit view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$id){
       
        $record = $this->model::where('id',$id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        return view('backend.permission.edit',compact('record'));
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {   
        $request->validate([
            'id'=>'bail|required',
            'name' => 'required|string|max:255|min:2|unique:permissions,name,'.$request->id,         
        ]);
        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        $record->name = $request->name;
        $record->updated_at = Carbon::now();
        $record->update();

        return redirect()->route('permission',$request->queryString)->withSuccess('Successfully updated...');
    }

     /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:permissions',
        ]);

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:permissions',
        ]);

        $data = $this->model::whereId($request->id)->first();
        if(empty($data)){
            return response()->json(['error' => true,'message'=>'record not found'], 300);
        }
        $data->delete();
        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }
}
