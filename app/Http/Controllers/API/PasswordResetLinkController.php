<?php 

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Auth;

/**
 * Class PasswordResetLinkController
 * @package App\Http\Controllers
 */
class PasswordResetLinkController extends Controller
{

   use ApiResponser;

    /**
     * Handle an incoming password reset link request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
             'email'=>'required|exists:users|email|max:150',
            ];

        $messages = [];

        $validate = Validator::make($request->all(), $rules, $messages);

        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }


        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.

        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status == Password::RESET_LINK_SENT) {
            return $this->successResponse([],$status);
        }

                    
    }
}