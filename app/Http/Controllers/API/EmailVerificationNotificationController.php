<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class EmailVerificationNotificationController extends Controller
{
    use ApiResponser;

    /**
     * Send a new email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {   
        if ($request->user()->hasVerifiedEmail()) {
            return $this->errorResponse('you email has already verified', 400);
        }

        $request->user()->sendApiTenantEmailVerificationNotification($request->route('subdomain_name'));

        return $this->successResponse([],'verification link send successfully.');
    }
}
