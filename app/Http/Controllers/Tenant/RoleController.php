<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $model;
    public $service;
    public function __construct(Request $request, RoleService $service){
        $this->service = $service;
        $this->model = new Role;
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request){
        return $this->service->listService($request);
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {   
        return $this->service->createService($request);
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
    	return $this->service->storeService($request);
    }

    /**
     * Display edit view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id){
        return $this->service->editService($request,$subdomain,$id); 
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {   
    	return $this->service->updateService($request);
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        return $this->service->changeStatusService($request);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        return $this->service->deleteService($request);
    }
}
