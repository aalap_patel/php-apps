<?php 
namespace App\Http\Controllers\Tenant;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Carbon\Carbon;
use Response;
use General;
class SubdomainDashboardController extends Controller{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index(Request $request){
        return View('tenant/dashboard/index');
    }
}