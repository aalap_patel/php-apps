<?php

namespace App\Http\Controllers\Tenant\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {   if($request->user()==NULL){
            return redirect('/');
        }
        
        return $request->user()->hasVerifiedEmail()
                    ? redirect()->intended(RouteServiceProvider::TENANTHOME)
                    : view('tenant.auth.verify-email');
    
    }
}
