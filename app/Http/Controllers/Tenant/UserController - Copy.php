<?php

namespace App\Http\Controllers\Tenant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\Rules;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Country;
use App\Models\Currency;
use App\Models\DomainDetail;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;
use App\Services\UserService;



class UserController extends Controller
{
    protected $model;
    public $service;
    public function __construct(Request $request, UserService $service){
        $this->service = $service;
        $this->model = new User;
        $this->tableName = $this->model->getTable();
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   
        return $this->service->listService($request);

        // $records = $this->model;

        // if($request->status=="0" || $request->status==1){
        //     $records = $records->where('status',$request->status);
        // }elseif($request->status==2){
        //     $records = $records->onlyTrashed();
        // }

        // if($request->email_verified_at=="0"){
        //     $records = $records->whereNull('email_verified_at');
        // }elseif($request->email_verified_at=="1"){
        //     $records = $records->whereNotNull('email_verified_at');
        // }

        // $fromDate = $request->from_date;
        // $toDate = $request->to_date;

        // if(!empty($fromDate)){
        //     $fromDate .= ' 00:00:00';
        //     $records = $records->whereDate('created_at','>=',$fromDate);
        // }
        // if(!empty($toDate)){
        //     $toDate .= ' 23:59:59';
        //     $records = $records->where('created_at','<=',$toDate);
        // }

        // if(!empty($request->search_str) && ($request->search_by=='name' || $request->search_by=='email' || $request->search_by=='phone1')){
        //     $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        // }
        // $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();     
        // return view('tenant/user/index', compact('records'));
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $roles = Role::where('status',1)->orderBy('name','ASC')->get();
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        $currency = Currency::where('status',1)->orderBy('code','ASC')->get();
        $recordPermissions = [];
        return view('tenant/user/add',compact('permissionArray','roles','recordPermissions','countries','currency'));
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {   
        $request->only('name', 'email', 'secondry_email','password','password_confirmation','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','preferred_timezone','preferred_currency','preferred_date_format','is_verified','role','profile_image');
        $this->validate($request, 
            [
                'name' => 'required|string|max:100',
                'email' => 'required|email|unique:users',
                'secondry_email' => 'email',
                'password' => 'required|confirmed|min:5|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$@&#%]).*$/|',
                'permissions'=>'nullable|array',
                'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'role'=> 'required',
                'address1'=> 'required',
                'city'=> 'required',
                'state'=> 'required',
                'country_id'=> 'required|numeric',
                'zipcode'=> 'required',
                'phone1'=> 'required|min:6|max:15',
            ]);
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'secondry_email' => $request->secondry_email,
            'password' => Hash::make($request->password),
            'email_verified_at' => isset($request->is_verified) ? Carbon::now() : NULL,
            'created_at' =>Carbon::now(),
            'status'=>1,
            'profile_image' => isset($request->profile_image) ? Storage::disk('public')->put($request->route('subdomain_name').'/user', $request->profile_image) : '',
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state,
            'country_id' => $request->country,
            'zipcode' => $request->zipcode,
            'phone1' => $request->phone1,
            'phone2' => $request->phone2,
            'preferred_timezone' => $request->preferred_timezone,
            'preferred_currency' => $request->preferred_currency,
            'preferred_date_format' => $request->preferred_date_format,
        ]);

        if(is_array($request->permissions) && count($request->permissions)){
            $permissions = Permission::whereIn('id',$request->permissions)->get()->pluck('name')->toArray();
            if(count($permissions)){
                $user->syncPermissions($permissions);
            } 
        }

        $user->assignRole($request->role);
        //event(new Registered($user));

        return redirect()->route('tenant.user',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('User susscessfully created.');
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$subdomain,$id)
    {   
        $request->merge(['id'=>$id]);
        $request->validate([
            'id'=>'required|numeric|exists:users',
        ]);
        $record = $this->model::with('permissions')->with('roles')->where('id',$request->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $roles = Role::where('status',1)->orderBy('id','ASC')->get();       
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $recordPermissions = $record->permissions->pluck('id')->toArray();
        $recordRole = $record->roles->pluck('id')->toArray();
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        $currency = Currency::where('status',1)->orderBy('code','ASC')->get();
        return view('tenant/user/edit', compact('record','permissionArray','recordPermissions','roles','recordRole','countries','currency')); 
    }

    
    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {  
         $request->validate([
            'id'=>'required|numeric|exists:users',
            'name' => 'required|string|max:150|min:2',
            'permissions'=>'nullable|array',
            'secondry_email' => 'nullable|email',
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|min:6|max:15',
            'role'  => 'required',
            ]);
        
        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $request->only('name', 'email', 'secondry_email','password','password_confirmation','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','preferred_timezone','preferred_currency','preferred_date_format','is_verified','role','profile_image','id');

        $requestData = $request->all();
        $requestData['email_verified_at'] = isset($request->is_verified) ? Carbon::now() : NULL;
        $image = $request->file('profile_image');

        if(isset($image) && !empty($image)) {
            $requestData['profile_image'] = Storage::disk('public')->put($request->route('subdomain_name').'/user', $image);
            //Delete old image
            Storage::disk('public')->delete($request->route('subdomain_name').'/user',$record->profile_image);
        }else{
            $requestData['profile_image'] = $record->profile_image;
        }

        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        if( is_array($request->permissions) && count($request->permissions) ){
            $permissions = Permission::whereIn('id',$request->permissions ? $request->permissions : [])->get()->pluck('name')->toArray();
            if(count($permissions))
                $record->syncPermissions($permissions);
            else{
                $record->syncPermissions();
                //$record->revokePermissionTo($permissions);
            }            
        }else{
             $record->syncPermissions();
        }
        $record->syncRoles($request->role);
        
        return redirect()->route('tenant.user',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');   
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatus(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:users',
        ]);

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        
        return response()->json(['message'=>'Status Update Successfully']);
    }

    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id'=>'required|numeric|exists:users',
        ]);

        $data = $this->model::whereId($request->id)->first();
       
		if(empty($data)){
			return response()->json(['error' => true,'message'=>'record not found'], 300);
		}
        $data->delete();
        //write a code for when tenant delete at time user also delete and delete their database

        return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function profile(Request $request)
    {
        $record = Auth::user();
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        $currency = Currency::where('status',1)->orderBy('code','ASC')->get();
        $siteData = DomainDetail::first();

        $baseUrl = explode('profile',URL::current());
        if($baseUrl && count($baseUrl)>=2){
          $baseUrl = $baseUrl[0];  
        }else{
            $baseUrl = '';
        }
        return view('tenant/user/profile',compact('record','countries','currency','siteData','baseUrl'));
    }

    /**
     * Request for update data.
     *
     * @return \Illuminate\View\View
     */
    public function updateProfile(Request $request)
    {  
        $request->validate([
            'name' => 'required|string|max:150|min:2',
            'secondry_email' => 'nullable|email',
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|min:6|max:15',
        ]);
       
        $record = $this->model::where('id',Auth::user()->id)->first();
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }

        $request->only('name', 'secondry_email','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','preferred_timezone','preferred_currency','preferred_date_format','profile_image');

        $requestData = $request->all();
        $image = $request->file('profile_image');
        if(isset($image) && !empty($image)) {
            $requestData['profile_image'] = Storage::disk('public')->put($request->route('subdomain_name').'/user', $image);
            //Delete old image
            Storage::disk('public')->delete($request->route('subdomain_name').'/user',$record->profile_image);
        }else{
            $requestData['profile_image'] = $record->profile_image;
        }
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);

        return redirect()->route('tenant.profile',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');
    }


    /**
     * Request for update password.
     *
     * @return \Illuminate\View\View
     */
    public function changePassword(Request $request)
    {   
        
        $request->validate([
            'old_password' => 'required|max:20|min:8',
            'password' => ['required', Rules\Password::defaults()],
            'password_confirm' => 'required|same:password',        
        ]);

        $user = Auth::user();
        if(!Hash::check($request->old_password, $user->password)){
            return redirect()->route('profile')->withErrors('Your old password are not match with record.');   
        }

        $user->forceFill([
            'password' => Hash::make($request->password),
            'remember_token' => Str::random(60),
        ])->save();
        return redirect()->route('tenant.profile',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Your password updated successfully');        
    }  


    /**
     * Request for update site data.
     *
     * @return \Illuminate\View\View
     */
    public function changeSiteData(Request $request)
    {   
        $request->validate([
            'name' => 'required|string|max:150|min:2',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'baseurl'=> 'url',
            'facebook_url'=> 'url',
            'linkedin_url'=> 'url',
            'twitter_url'=> 'url',
        ]);
        
        $request->only('name', 'logo','facebook_url','linkedin_url','twitter_url');

        $record = DomainDetail::first();

        if(empty($record)){
             $domain = DomainDetail::create([
                'name' => $request->name,
                'logo' => isset($request->logo) ? Storage::disk('public')->put($request->route('subdomain_name').'/user', $request->logo) : '',
                'domain_url' => $request->baseurl,
                'facebook_url' => $request->facebook_url,
                'linkedin_url' => $request->linkedin_url,
                'twitter_url' => $request->twitter_url,
                'created_at' =>Carbon::now(),
            ]);  
        }else{
            $requestData = $request->all();
            $image = $request->file('logo');
            if(isset($image) && !empty($image)) {
                $requestData['logo'] = Storage::disk('public')->put($request->route('subdomain_name').'/user', $image);
                //Delete old image
                Storage::disk('public')->delete($request->route('subdomain_name').'/user',$record->logo);
            }else{
                $requestData['logo'] = $record->logo;
            }
            $requestData['updated_at'] = Carbon::now();
            $record->update($requestData);
        }
        return redirect()->route('tenant.profile',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');       
    }  

    /**
     * Handle permission Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for get permission for role
     *
     * @return \Ajax response
     */
    public function getPermission(Request $request)
    {   
        $request->validate([
            'id'=>'required|numeric|exists:roles',
        ]);

        $record = Role::where('id',$request->id)->where('status',1)->first();
        if(empty($record)){
            return response()->json(['message'=>'No record found'],404);
        }
        $recordPermissions = $record->getAllPermissions()->pluck('id')->toArray();  
        
        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        return Response::json(View('tenant/user/permission-ajax', compact('permissionArray','recordPermissions'))->render());
    }
            

}