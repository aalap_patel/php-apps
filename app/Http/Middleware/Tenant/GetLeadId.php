<?php
namespace App\Http\Middleware\Tenant;

use Closure;
use App\Traits\ApiResponser;
use App\Models\Lead;
use Auth;
use Validator;

class GetLeadId 
{
    use ApiResponser;

    public function handle($request, Closure $next)
    {   
        //$request->merge(['id'=>$request->leadId]);
        $rules = [
            'leadId'=>['required','numeric','exists:lead,id'],
        ];

        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return redirect()->back()->withErrors($errormessage);
        }
       
        $record = Lead::where('id',$request->leadId)->where('user_id',Auth::user()->id)->first();
        
        if(empty($record)){
            return redirect()->back()->withErrors('No record found');
        }
        return $next($request);
    }
}
