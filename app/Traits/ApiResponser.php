<?php
namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser
{
    /**
     * Success Response
     * @param string $data
     * @param int $code
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */

    protected function successResponse($data, $message = null, $code = 200)
    {   
        return response()->json([
            'status'=> 'success', 
            'message' => $message, 
            'data' => $data
        ], $code);
    }

    protected function errorResponse($message = null, $code)
    {   
        return response()->json([
            'status'=>'error',
            'message' => $message,
            'data' => null
        ], $code);
        
    }

    public function errorMessage($message, $code)
     {   
         return response($message, $code)->header('Content-Type', 'application/json');
    }
    

    /**
    * success response method.
    *
    * @return \Illuminate\Http\Response
    */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message,
        ];
        
        return response()->json($response, 200);
    }


    /**
    * return error response.
    *
    * @return \Illuminate\Http\Response
    */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    
}