<?php

namespace App\Traits;

use Illuminate\Http\Response;
use App\Models\LeadTimeline;
use Auth;

trait LeadActivity
{
    /**
     * Store File
     * @param $file object from Illuminate\Http\Request
     * @param $destinationPath
     * @return string | file path
     */
    public function storeActivity($contentId, $contentType, $action, $description)
    {   
        LeadTimeline::create([
            'user_id'     => Auth::id(),
            'content_id'   => $contentId,
            'content_type' => $contentType,
            'action'      => $action,
            'description' => $description,
        ]);

        
    }
}
