<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Redirect;
use Throwable;
use Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Session\TokenMismatchException;
use Spatie\Multitenancy\Exceptions\NoCurrentTenant as NoCurrentTenantExceptionHandler;
use Spatie\Multitenancy\Exceptions\InvalidConfiguration as InvalidConfigurationExceptionHandler;
use Spatie\Permission\Exceptions\UnauthorizedException as SpatieExceptionHandler;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {   
       /* $this->renderable(function(Exception $e, $request) {
            return $this->handleException($request, $e);
        });*/
         $this->reportable(function (Throwable $e) {
           
        });
        
    }


    public function render($request, Throwable $exception){
        $type = $request->is('api/*') ? 'api':'web';
        //dd($exception);
        if (env('APP_DEBUG', true)) {
            return parent::render($request, $exception);
        }
        
        if ($exception instanceof SpatieExceptionHandler) {
            if($type=='api'){
                return $this->errorResponse($exception->getMessage(), Response::HTTP_FORBIDDEN);
            }else if($type=='web'){
                return response()->view('errors.404',['message'=>$exception->getMessage()]);
            }
        }

        if ($exception instanceof NoCurrentTenantExceptionHandler) {
            if($type=='api'){
                return $this->errorResponse($exception->getMessage(), Response::HTTP_FORBIDDEN);
            }else if($type=='web'){
                return response()->view('errors.404',['message'=>$exception->getMessage()]);
            }
        }

        if ($exception instanceof InvalidConfigurationExceptionHandler) {
            if($type=='api'){
                return $this->errorResponse($exception->getMessage(), Response::HTTP_FORBIDDEN);
            }else if($type=='web'){
                return response()->view('errors.404',['message'=>$exception->getMessage()]);
            }
        }


        if ($exception instanceof HttpException) {
            $code = $exception->getStatusCode();
            $message = Response::$statusTexts[$code];
            if($type=='api'){
                return $this->errorResponse($message, $code);
            }else if($type=='web'){
                return response()->view('errors.404',['message'=>$message]);
            }
        }


        if ($exception instanceof ModelNotFoundException) {
            $model = strtolower(class_basename($exception->getModel()));
            return $this->errorResponse("Does not exist any instance of {$model} with the given id", Response::HTTP_NOT_FOUND);
            if($type=='api'){
                return $this->errorResponse("Does not exist any instance of {$model} with the given id", Response::HTTP_NOT_FOUND);
            }else if($type=='web'){
                return response()->view('errors.404',['message'=>'Does not exist any instance of {$model} with the given id']);
            }
        }

        if($exception instanceof MethodNotAllowedHttpException){
            if($type=='web'){
                return response()->view('errors.404',['message'=>'Opps..! The Method you are using is not allowed for this url.']);
            }else if($type='api'){
                return $this->errorResponse($exception->getMessage(), Response::HTTP_FORBIDDEN);
            }
        }

        if ($exception instanceof AuthorizationException) {
            $message = 'Whooops! There was some issue with authorization';//$exception->getMessage();
            if($type=='web'){
                return response()->view('errors.404',['message'=>$message]);
            }else if($type='api'){
                return $this->errorResponse($message, Response::HTTP_FORBIDDEN);
            }
        }

        if ($exception instanceof AuthenticationException) {
            $message = 'Your login session has been expired, please try again to login.';//$exception->getMessage();
            if($type=='web'){
                return redirect()->route('tenant.login',['subdomain_name'=>$request->route('subdomain_name')])->withErrors($message);
            }else if($type='api'){
                return $this->errorResponse($message, Response::HTTP_FORBIDDEN);
            }
        }


        if ($exception instanceof ValidationException) {
            //$errors = $exception->validator->errors()->getMessages();
            $errormessage = implode("\r\n", $exception->validator->errors()->all());
            if($type=='web'){
                //return response()->view('errors.404',['message'=>$errormessage]);
            }else if($type='api'){
                return $this->errorResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        if ($exception instanceof QueryException) {
            $message = 'Whooops! something went wrong.';//$exception->getMessage();
            $code = $exception->getCode();
            if($type=='web'){
                return response()->view('errors.404',['message'=>$message]);
            }else if($type='api'){
                return $this->errorResponse($message, 300);
            }
        }


        if ($exception instanceof TokenMismatchException) {
            if($type=='web'){
                return redirect()->back()->withInput()->withErrors("Oops! Your Session has Expired, Seems you couldn't submit form for a long time. Please try again.");
            }else if($type='api'){
                return $this->errorResponse("Oops! Your Session has Expired, Seems you couldn't submit form for a long time. Please try again.", Response::HTTP_FORBIDDEN);
            }
        }


        if($type=='web'){
            return response()->view('errors.404',['message'=>"Oops! Your Session has Expired, Seems you couldn't submit form for a long time. Please try again."]);
        }else if($type='api'){
            return $this->errorResponse('Unexpected error. Try later', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        
    }
}
