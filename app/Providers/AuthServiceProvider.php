<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        ResetPassword::createUrlUsing(function ($user, string $token){
            $request = app(\Illuminate\Http\Request::class);
            if($request->route('subdomain_name')){
                if(str_contains($request->url(), 'api')){
                    return route('api.password.reset',['subdomain_name'=>$request->route('subdomain_name'),'token'=>$token,'email'=>$user->email]);
                }
                return route('tenant.password.reset',['subdomain_name'=>$request->route('subdomain_name'),'token'=>$token,'email'=>$user->email]);
            }else{
                if ($request->is('admin/*')) {
                    return route('password.reset',['token'=>$token,'email'=>$user->email]);
                }else{
                    return route('front.password.reset',['token'=>$token,'email'=>$user->email]);
                }
            }
            
        });

        Passport::routes();
        //Passport::loadKeysFrom(__DIR__.'/../secrets/oauth');
        //Passport::hashClientSecrets();
        //Token Life
        Passport::tokensExpireIn(now()->addDays(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));
        Passport::personalAccessTokensExpireIn(now()->addMonths(6));
    }
}
