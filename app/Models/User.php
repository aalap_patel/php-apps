<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Notifications\TenantEmailVerify;
use App\Notifications\TenantSubdomainEmailVerify;
use App\Notifications\TenantApiSubdomainEmailVerify;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable,SoftDeletes,HasRoles,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'email_verified_at',
        'is_admin',
        'secondry_email',
        'profile_image',
        'address1',
        'address2',
        'city',
        'state',
        'country_id',
        'zipcode',
        'phone1',
        'phone2',
        'preferred_timezone',
        'preferred_currency',
        'preferred_date_format',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendTenantEmailVerificationNotification()
    {
        $this->notify(new TenantEmailVerify);
    }

    public function sendApiTenantEmailVerificationNotification($subdomain)
    {
        $this->notify(new TenantApiSubdomainEmailVerify($subdomain));
    }

    public function subdomainTenantEmailVerificationNotification($subdomain)
    {
        $this->notify(new TenantSubdomainEmailVerify($subdomain));
    }
}
