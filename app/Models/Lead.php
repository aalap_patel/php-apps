<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Lead extends Model{

    use Notifiable, SoftDeletes;
	/**
     * @var string
     */
    protected $table = 'lead';
	
	/**
     * $timestamps false to avoid auto insert of default date
     *
     * @var boolean
     */
    public $timestamps = true;

    protected $guarded = ['id'];
	
	public function addedBy(){
        return $this->belongsTo('App\Models\User','user_id');
    }
    
}
