<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class DomainDetail extends Model{

    use Notifiable;
	/**
     * @var string
     */
    protected $table = 'domain_detail';
	
	/**
     * $timestamps false to avoid auto insert of default date
     *
     * @var boolean
     */
    public $timestamps = true;

    protected $guarded = ['id'];
	
	
    
}
