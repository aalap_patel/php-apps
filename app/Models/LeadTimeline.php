<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class LeadTimeline extends Model{

    use Notifiable, SoftDeletes;
	/**
     * @var string
     */
    protected $table = 'lead_timeline';
	
	/**
     * $timestamps false to avoid auto insert of default date
     *
     * @var boolean
     */
    public $timestamps = true;

    protected $guarded = ['id'];
	
}
