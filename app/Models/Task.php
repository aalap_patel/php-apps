<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Task extends Model{

    use Notifiable, SoftDeletes;
	/**
     * @var string
     */
    protected $table = 'task';
	
	/**
     * $timestamps false to avoid auto insert of default date
     *
     * @var boolean
     */
    public $timestamps = true;

    protected $guarded = ['id'];
	
	
    
}
