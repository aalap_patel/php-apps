<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Tenant extends Model{

    use Notifiable, SoftDeletes;
	/**
     * @var string
     */
    protected $table = 'tenants';
	
	/**
     * $timestamps false to avoid auto insert of default date
     *
     * @var boolean
     */
    public $timestamps = false;

    protected $guarded = ['id'];
	
	
    
}
