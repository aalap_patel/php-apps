<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Traits\ApiResponser;
use App\Traits\LeadActivity;
use App\Models\Notes;
use Carbon\Carbon;
use Response;
use Config;


class LeadNoteService
{
    use ApiResponser,LeadActivity;
    protected $model;

    public function __construct(){
        $this->model = new Notes;
        $this->tableName = $this->model->getTable();
    }

   /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function listService($request)
    {
        $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='notes')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->where('lead_id',$request->leadId)->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();
        $leadId = $request->leadId; 
             
        if($request->is('api/*')){
            return $this->successResponse(['records'=>$records,'leadId'=>$leadId]);
        }else{
            return view('tenant/note/index', compact('records','leadId'));
        }
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function createService($request)
    {
        $leadId = $request->leadId;
        if($request->is('api/*')){
            return $this->successResponse(['leadId'=>$leadId]);
        }else{
            return view('tenant/note/add',compact('leadId'));
        }
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
     public function storeService($request){
        $rules = [
            'notes'=> ['required','unique:notes,notes,null,id,lead_id,'.$request->leadId.',deleted_at,NULL'],
            'description' => 'required',
            'blob_azure_url' => 'required|file|mimes:jpeg,png,jpg,gif,svg,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-ms-wmv,video/x-msvideo,audio/mp3,audio/mp4,mp3,mp4|max:2048',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $request->only('notes', 'description', 'blob_azure_url');

        $this->model::create([
           'notes' => $request->notes,
            'description' => $request->description,
            'blob_azure_url' => isset($request->blob_azure_url) ? Storage::disk('public')->put($request->route('subdomain_name').'/lead/notes', $request->blob_azure_url) : '',
            'lead_id' => $request->leadId,
            'created_at' =>Carbon::now(),
            'status'=>1,
        ]);

        if($request->is('api/*')){
            return $this->successResponse([],'Note susscessfully created');
        }else{
            return redirect()->route('tenant.lead-note',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Note Successfully created.');
        }
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function editService($request,$subdomain,$id)
    {
        $request->merge(['id'=>$id]);
        $rules = [
            'id'=>'required|numeric|exists:notes',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('lead_id',$request->leadId)->where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }
        $leadId = $request->leadId;

        if($request->is('api/*')){
            return $this->successResponse(['record'=>$record,'leadId'=>$leadId]);
        }else{
            return view('tenant/note/edit',compact('record','leadId')); 
        }

         
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateService($request)
    {
        $rules=[
            'id'=>'required|numeric|exists:notes',
            'notes'=> ['required','unique:notes,notes,'.$request->id.',id,lead_id,'.$request->leadId.',deleted_at,NULL'],
            'description' => 'required',
            'blob_azure_url' => 'file|mimes:jpeg,png,jpg,gif,svg,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-ms-wmv,video/x-msvideo,audio/mp3,audio/mp4,|max:2048',
            'leadId'=> 'required|numeric',      
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $request->only('notes', 'description', 'blob_azure_url','id');

        $requestData = $request->all();

        $file = $request->file('blob_azure_url');
        if(isset($file) && !empty($file)) {
            $requestData['blob_azure_url'] = Storage::disk('public')->put($request->route('subdomain_name').'/lead/notes', $file);
            //Delete old image
            Storage::disk('public')->delete($request->route('subdomain_name').'/note',$record->blob_azure_url);
        }else{
            $requestData['blob_azure_url'] = $record->blob_azure_url;
        }
        
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        if($request->is('api/*')){
            return $this->successResponse([],'Record successfully updated');
        }else{
            return redirect()->route('tenant.lead-note',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated'); 
        }  
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatusService($request){
        $rules = [
            'id'=>'required|numeric|exists:notes',
            'status'=>['required','numeric','gte:0','lte:1'],
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['message'=>'No record found'],404);
            } 
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->successResponse([],'Status Update Successfully');
            }else{
                return response()->json(['message'=>'Status Update Successfully']);
            } 
        }
        
    }


    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function deleteService($request)
    {
        $rules = [
            'id'=>'required|numeric|exists:notes',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::whereId($request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['error' => true,'message'=>'record not found'], 300);
            } 
        }

        $record->delete();
        if($request->is('api/*')){
            return $this->successResponse([],'Successfully deleted');
        }else{
            return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        } 
    }

}
