<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\Rules;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Country;
use App\Models\Currency;
use App\Models\DomainDetail;
use Illuminate\Support\Facades\URL;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Auth;
use File;
use General;
use Mail;
use Response;
use Config;

class UserService
{
    use ApiResponser;
    protected $model;

    public function __construct(){
        $this->model = new User;
        $this->tableName = $this->model->getTable();
    }

   /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function listService($request)
    {
        $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        if($request->email_verified_at=="0"){
            $records = $records->whereNull('email_verified_at');
        }elseif($request->email_verified_at=="1"){
            $records = $records->whereNotNull('email_verified_at');
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name' || $request->search_by=='email' || $request->search_by=='phone1')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%'); 
            //$records = $records->WhereRaw("MATCH(name,email) AGAINST(? IN BOOLEAN MODE)", [$request->search_str]);  
        }
        $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();

        if($request->is('api/*')){
            return $this->successResponse([$records]);
        }else{
            return view('tenant/user/index', compact('records'));
        }
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function createService($request)
    {
        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $roles = Role::where('status',1)->orderBy('name','ASC')->get();
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        $currency = Currency::where('status',1)->orderBy('code','ASC')->get();
        $recordPermissions = [];
        
        if($request->is('api/*')){
            return $this->successResponse(['permissionArray'=>$permissionArray,'roles'=>$roles,'recordPermissions'=>$recordPermissions,'countries'=>$countries,'currency'=>$currency]);
        }else{
            return view('tenant/user/add',compact('permissionArray','roles','recordPermissions','countries','currency'));
        }
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
     public function storeService($request){

        $request->only('name', 'email', 'secondry_email','password','password_confirmation','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','preferred_timezone','preferred_currency','preferred_date_format','is_verified','role','profile_image');

        $rules = [
            'name' => 'required|string|max:100',
            'email' => 'required|email|unique:users',
            'secondry_email' => 'nullable|email',
            'password' => 'required|confirmed|min:5|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$@&#%]).*$/|',
            'permissions'=>'nullable|array',
            'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'role'=> 'required',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|numeric|digits:10',
            'phone2'=> 'nullable|numeric|digits:10',
        ];

        $messages = [
            'not_regex' => 'The :attribute field must not be in a repetitive patttern.',
        ];

        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'secondry_email' => $request->secondry_email,
            'password' => Hash::make($request->password),
            'email_verified_at' => isset($request->is_verified) ? Carbon::now() : NULL,
            'created_at' =>Carbon::now(),
            'status'=>1,
            'profile_image' => isset($request->profile_image) ? Storage::disk('public')->put($request->route('subdomain_name').'/user', $request->profile_image) : '',
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state,
            'country_id' => $request->country,
            'zipcode' => $request->zipcode,
            'phone1' => $request->phone1,
            'phone2' => $request->phone2,
            'preferred_timezone' => $request->preferred_timezone,
            'preferred_currency' => $request->preferred_currency,
            'preferred_date_format' => $request->preferred_date_format,
        ]);

        if(is_array($request->permissions) && count($request->permissions)){
            $permissions = Permission::whereIn('id',$request->permissions)->get()->pluck('name')->toArray();
            if(count($permissions)){
                $user->syncPermissions($permissions);
            } 
        }

        $user->assignRole($request->role);

        if($request->is('api/*')){
            return $this->successResponse([],'User susscessfully created');
        }else{
            return redirect()->route('tenant.user',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('User susscessfully created.');
        }
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function editService($request,$subdomain,$id)
    {
        $request->merge(['id'=>$id]);
        $rules = [
            'id'=>'required|numeric|exists:users',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::with('permissions')->with('roles')->where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $roles = Role::where('status',1)->orderBy('id','ASC')->get();       
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $recordPermissions = $record->permissions->pluck('id')->toArray();
        $recordRole = $record->roles->pluck('id')->toArray();
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        $currency = Currency::where('status',1)->orderBy('code','ASC')->get();
        
        if($request->is('api/*')){
            return $this->successResponse(['record'=>$record,'permissionArray'=>$permissionArray,'recordPermissions'=>$recordPermissions,'roles'=>$roles,'recordRole'=>$recordRole,'countries'=>$countries,'countries'=>$countries]);
        }else{
            return view('tenant/user/edit', compact('record','permissionArray','recordPermissions','roles','recordRole','countries','currency'));
        }

         
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateService($request)
    {
        $rules = [
            'id'=>'required|numeric|exists:users',
            'name' => 'required|string|max:150|min:2',
            'permissions'=>'nullable|array',
            'secondry_email' => 'nullable|email',
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|numeric|digits:10',
            'role'  => 'required',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $request->only('name', 'email', 'secondry_email','password','password_confirmation','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','preferred_timezone','preferred_currency','preferred_date_format','is_verified','role','profile_image','id');

        $requestData = $request->all();
        $requestData['email_verified_at'] = isset($request->is_verified) ? Carbon::now() : NULL;
        $image = $request->file('profile_image');

        if(isset($image) && !empty($image)) {
            $requestData['profile_image'] = Storage::disk('public')->put($request->route('subdomain_name').'/user', $image);
            //Delete old image
            Storage::disk('public')->delete($request->route('subdomain_name').'/user',$record->profile_image);
        }else{
            $requestData['profile_image'] = $record->profile_image;
        }

        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        if( is_array($request->permissions) && count($request->permissions) ){
            $permissions = Permission::whereIn('id',$request->permissions ? $request->permissions : [])->get()->pluck('name')->toArray();
            if(count($permissions))
                $record->syncPermissions($permissions);
            else{
                $record->syncPermissions();
                //$record->revokePermissionTo($permissions);
            }            
        }else{
             $record->syncPermissions();
        }
        $record->syncRoles($request->role);
        
        if($request->is('api/*')){
            return $this->successResponse([],'Record successfully updated');
        }else{
            return redirect()->route('tenant.user',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');
        }  
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatusService($request){
        $rules = [
            'id'=>'required|numeric|exists:users',
            'status'=>['required','numeric','gte:0','lte:1'],
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['message'=>'No record found'],404);
            } 
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->successResponse([],'Status Update Successfully');
            }else{
                return response()->json(['message'=>'Status Update Successfully']);
            } 
        }
        
    }


    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function deleteService($request)
    {
        $rules = [
            'id'=>'required|numeric|exists:users',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::whereId($request->id)->first();
       
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['error' => true,'message'=>'record not found'], 300);
            } 
        }

        $record->delete();
        if($request->is('api/*')){
            return $this->successResponse([],'Successfully deleted');
        }else{
            return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        } 

        
    }

    /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function profileService($request)
    {
        $record = Auth::user();
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        $currency = Currency::where('status',1)->orderBy('code','ASC')->get();
        $siteData = DomainDetail::first();

        $baseUrl = explode('profile',URL::current());
        if($baseUrl && count($baseUrl)>=2){
          $baseUrl = $baseUrl[0];  
        }else{
            $baseUrl = '';
        }

        if($request->is('api/*')){
            return $this->successResponse(['record'=>$record,'countries'=>$countries,'currency'=>$currency,'siteData'=>$siteData,'baseUrl'=>$baseUrl]);
        }else{
            return view('tenant/user/profile',compact('record','countries','currency','siteData','baseUrl'));
        } 
    }

    /**
     * Request for update data.
     *
     * @return \Illuminate\View\View
     */
    public function updateProfileService($request)
    {
        $rules = [
            'name' => 'required|string|max:150|min:2',
            'secondry_email' => 'nullable|email',
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|numeric|digits:10',
            'phone2'=> 'nullable|numeric|digits:10',
        ];

        $messages = [];

        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('id',Auth::user()->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $request->only('name', 'secondry_email','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','preferred_timezone','preferred_currency','preferred_date_format','profile_image');

        $requestData = $request->all();
        $image = $request->file('profile_image');
        if(isset($image) && !empty($image)) {
            $requestData['profile_image'] = Storage::disk('public')->put($request->route('subdomain_name').'/user', $image);
            //Delete old image
            Storage::disk('public')->delete($request->route('subdomain_name').'/user',$record->profile_image);
        }else{
            $requestData['profile_image'] = $record->profile_image;
        }
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);

        if($request->is('api/*')){
            return $this->successResponse([],'Record successfully updated');
        }else{
            return redirect()->route('tenant.profile',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');
        }

        
    }


    /**
     * Request for update password.
     *
     * @return \Illuminate\View\View
     */
    public function changeProfilePasswordService($request)
    {
       $rules = [
            'old_password' => 'required|max:20|min:8',
            'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-zA-Z and])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$@&#%]).*$/',
            'password_confirm' => 'required|same:password', 
        ];

        $messages = [
            'password.regex' => 'The :attribute field must not be in a repetitive patttern. you should enter with [a-zA-Z, 0-9 and allow special characters are !, $, @, &, #, %]',
        ];

        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $user = Auth::user();
        if(!Hash::check($request->old_password, $user->password)){
            if($request->is('api/*')){
                return $this->errorResponse('Your old password are not match with record.', 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('Your old password are not match with record.');
            }
        }

        $user->forceFill([
            'password' => Hash::make($request->password),
            'remember_token' => Str::random(60),
        ])->save();

        if($request->is('api/*')){
            return $this->successResponse([],'Your password updated successfully');
        }else{
            return redirect()->route('tenant.profile',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Your password updated successfully'); 
        }
    }


    /**
     * Request for update site data.
     *
     * @return \Illuminate\View\View
     */
    public function changeSiteDataService($request)
    {
        $rules = [
            'name' => 'required|string|max:150|min:2',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'baseurl'=> 'url',
            'facebook_url'=> 'url',
            'linkedin_url'=> 'url',
            'twitter_url'=> 'url',
        ];

        $messages = [];

        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $request->only('name', 'logo','facebook_url','linkedin_url','twitter_url');

        $record = DomainDetail::first();

        $baseUrl = explode('profile',URL::current());
        if($baseUrl && count($baseUrl)>=2){
          $baseUrl = $baseUrl[0];  
        }else{
            $baseUrl = $request->baseurl;
        }

        if(empty($record)){
             $domain = DomainDetail::create([
                'name' => $request->name,
                'logo' => isset($request->logo) ? Storage::disk('public')->put($request->route('subdomain_name').'/user', $request->logo) : '',
                'domain_url' => $baseUrl,
                'facebook_url' => $request->facebook_url,
                'linkedin_url' => $request->linkedin_url,
                'twitter_url' => $request->twitter_url,
                'created_at' =>Carbon::now(),
            ]);  
        }else{
            $requestData = $request->all();
            $image = $request->file('logo');
            if(isset($image) && !empty($image)) {
                $requestData['logo'] = Storage::disk('public')->put($request->route('subdomain_name').'/user', $image);
                //Delete old image
                Storage::disk('public')->delete($request->route('subdomain_name').'/user',$record->logo);
            }else{
                $requestData['logo'] = $record->logo;
            }
            $requestData['updated_at'] = Carbon::now();
            $record->update($requestData);
        }

        if($request->is('api/*')){
            return $this->successResponse([],'Record successfully updated');
        }else{
            return redirect()->route('tenant.profile',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated'); 
        }

         
    }

    /**
     * Handle permission Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for get permission for role
     *
     * @return \Ajax response
     */
    public function getPermissionService($request)
    {
        
        $rules = [
            'id'=>'required|numeric|exists:roles',
        ];

        $messages = [];

        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }


        $record = Role::where('id',$request->id)->where('status',1)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }
        $recordPermissions = $record->getAllPermissions()->pluck('id')->toArray();  
        
        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }

        if($request->is('api/*')){
            return $this->successResponse(['permissionArray'=>$permissionArray,'recordPermissions'=>$recordPermissions]);
        }else{
            return Response::json(View('tenant/user/permission-ajax', compact('permissionArray','recordPermissions'))->render());
        }
    }





}
