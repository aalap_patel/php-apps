<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Traits\ApiResponser;
use App\Traits\LeadActivity;
use App\Models\User;
use App\Models\Lead;
use Carbon\Carbon;
use Response;
use Auth;
use File;
use Config;


class LeadService
{
    use ApiResponser,LeadActivity;
    protected $model;

    public function __construct(){
        $this->model = new Lead;
        $this->tableName = $this->model->getTable();
    }

   /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function listService($request)
    {
        $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

         if($request->user_id){
            $records = $records->where('user_id',$request->user_id);
        }

        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name' || $request->search_by=='primary_email' || $request->search_by=='title' || $request->search_by=='phone1')){
            if($request->search_by=='name'){
                $records = $records->where(function($q) use($request){
                    $q->whereRaw("CONCAT(`first_name`, ' ', `last_name`) LIKE ?", ["%".$request->search_str."%"])
                    ->orWhereRaw("CONCAT(`last_name`, ' ', `first_name`) LIKE ?", ["%".$request->search_str."%"]);
                });
            }elseif($request->search_by=='phone1'){
                $records = $records->where($request->search_by,'LIKE',$request->search_str);
            }else{
                $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');
            } 
        }
        $records = $records->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();
        $users = User::Select('id','name')->get();     
        
        if($request->is('api/*')){
            return $this->successResponse(['records'=>$records,'users'=>$users]);
        }else{
            return view('tenant/lead/index', compact('records','users'));   
        }
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function createService($request)
    {
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        if($request->is('api/*')){
            return $this->successResponse(['countries'=>$countries]);
        }else{
            return view('tenant/lead/add',compact('countries'));
        }
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
     public function storeService($request){
        $rules = [
            'salutation' => 'required|string|max:150|min:2',
            'first_name' => 'required|string|max:150|min:2',
            'last_name' => 'required|string|max:150|min:2',
            'profile_pic' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required|string|max:255|min:2',
            'department' => 'required|string|max:255|min:2',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|numeric',
            'primary_email' => 'required|email|unique:lead,primary_email,null,id,user_id,'.Auth::user()->id.',deleted_at,NULL',
            'organization_name'=> 'required|string|max:255|min:2',
            'organization_email'=> 'required|email|max:150|min:2',
            'organization_phone1'=> 'required|numeric',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $request->only('salutation', 'first_name', 'last_name','profile_pic','title','department','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','primary_email','secondry_email','facebook_url','twitter_url','linkedin_url','instagram_url','organization_name','organization_email','organization_phone1','organization_phone2','organization_website','is_contact');

        $this->model::create([
            'user_id' => Auth::user()->id,
            'salutation' => $request->salutation,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'profile_pic' => isset($request->profile_pic) ? Storage::disk('public')->put($request->route('subdomain_name').'/lead', $request->profile_pic) : '',
            'title' => $request->title,
            'department' => $request->department,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'state' => $request->state,
            'country_id' => $request->country_id,
            'zipcode' => $request->zipcode,
            'phone1' => $request->phone1,
            'phone2' => $request->phone2,
            'primary_email' => $request->primary_email,
            'secondry_email' => $request->secondry_email,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'linkedin_url' => $request->linkedin_url,
            'instagram_url' => $request->instagram_url,
            'organization_name' => $request->organization_name,
            'organization_email' => $request->organization_email,
            'organization_phone1' => $request->organization_phone1,
            'organization_phone2' => $request->organization_phone2,
            'organization_website' => $request->organization_website,
            'is_contact' => isset($request->is_contact) ? 1 : NULL,
            'created_at' => Carbon::now(),
        ]);

        if($request->is('api/*')){
            return $this->successResponse([],'Contact susscessfully created');
        }else{
            return redirect()->route('tenant.lead',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Lead susscessfully created.');
        }
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function editService($request,$subdomain,$id)
    {
        $request->merge(['id'=>$id]);
        $rules = [
            'id'=>'required|numeric|exists:lead',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('user_id',Auth::user()->id)->where('id',$request->id)->first();
        $countries = Country::where('status',1)->orderBy('country_name','ASC')->get();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        if($request->is('api/*')){
            return $this->successResponse(['record'=>$record,'countries'=>$countries]);
        }else{
            return view('tenant/lead/edit', compact('record','countries'));  
        }

         
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateService($request)
    {
        $rules=[
            'id'=>'required|numeric|exists:lead',
            'salutation' => 'required|string|max:150|min:2',
            'first_name' => 'required|string|max:150|min:2',
            'last_name' => 'required|string|max:150|min:2',
            'profile_pic' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required|string|max:255|min:2',
            'department' => 'required|string|max:255|min:2',
            'address1'=> 'required',
            'city'=> 'required',
            'state'=> 'required',
            'country_id'=> 'required|numeric',
            'zipcode'=> 'required',
            'phone1'=> 'required|numeric',
            'primary_email' => 'required|email|unique:lead,primary_email,'.$request->id.',id,user_id,'.Auth::user()->id.',deleted_at,NULL',
            'organization_name'=> 'required|string|max:255|min:2',
            'organization_email'=> 'required|email|max:150|min:2',
            'organization_phone1'=> 'required|numeric',       
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('id',$request->id)->where('user_id',Auth::user()->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $request->only('salutation', 'first_name', 'last_name','profile_pic','title','department','address1','address2','city','state','country_id','zipcode', 'phone1','phone2','primary_email','secondry_email','facebook_url','twitter_url','linkedin_url','instagram_url','organization_name','organization_email','organization_phone1','organization_phone2','organization_website','is_contact');

        $requestData = $request->all();
        $requestData['is_contact'] = isset($request->is_contact) ? 1 : NULL;

        $image = $request->file('profile_pic');
        if(isset($image) && !empty($image)) {
            $requestData['profile_pic'] = Storage::disk('public')->put($request->route('subdomain_name').'/lead', $image);
            //Delete old image
            Storage::disk('public')->delete($request->route('subdomain_name').'/lead',$record->profile_pic);
        }else{
            $requestData['profile_pic'] = $record->profile_pic;
        }

        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);

        //add lead timeline
        $this->storeActivity($request->id, 'Lead', 'update', 'User has been update lead detail');
        
        if($request->is('api/*')){
            return $this->successResponse([],'Record successfully updated');
        }else{
            return redirect()->route('tenant.lead',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated');  
        }  
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatusService($request){
        $rules = [
            'id'=>'required|numeric|exists:lead',
            'status'=>['required','numeric','gte:0','lte:1'],
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['message'=>'No record found'],404);
            } 
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->successResponse([],'Status Update Successfully');
            }else{
                return response()->json(['message'=>'Status Update Successfully']);
            } 
        }
        
    }


    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function deleteService($request)
    {
        $rules = [
            'id'=>'required|numeric|exists:lead',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::whereId($request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['error' => true,'message'=>'record not found'], 300);
            } 
        }

        $record->delete();
        if($request->is('api/*')){
            return $this->successResponse([],'Successfully deleted');
        }else{
            return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        } 
    }

}
