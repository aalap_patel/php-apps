<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Auth;
use Response;
use Config;

class RoleService
{
    use ApiResponser;
    protected $model;

    public function __construct(){
        $this->model = new Role;
        $this->tableName = $this->model->getTable();
    }

   /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function listService($request)
    {
        $records = $this->model;
        $fromDate = $request->from_date;
        $toDate = $request->to_date;

        if(!empty($fromDate)){
            $fromDate .= ' 00:00:00';
            $records = $records->whereDate('created_at','>=',$fromDate);
        }
        if(!empty($toDate)){
            $toDate .= ' 23:59:59';
            $records = $records->where('created_at','<=',$toDate);
        }

        if(!empty($request->search_str) && $request->search_by=='name'){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');
        }
        $records = $records->latest()->paginate(config('app.paginate'))->withQueryString();     
        
        if($request->is('api/*')){
            return $this->successResponse([$records]);
        }else{
            return view('tenant/role/index', compact('records'));
        }
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function createService($request)
    {
        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        
        if($request->is('api/*')){
            return $this->successResponse(['permissionArray'=>$permissionArray]);
        }else{
            return view('tenant/role/add',compact('permissionArray'));
        }
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
     public function storeService($request){

        $rules = [
            'name' => 'required|string|max:255|min:2|unique:roles',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $inputs = $request->only('name');
        $record = $this->model::create($inputs);
        $record->givePermissionTo([$request->permissions]);
        
        if($request->is('api/*')){
            return $this->successResponse([],'Role susscessfully created');
        }else{
            return redirect()->route('tenant.role',['subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Successfully created...');
        }
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function editService($request,$subdomain,$id)
    {
        $request->merge(['id'=>$id]);
        $rules = [
            'id'=>'required|numeric|exists:roles',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $permissions = Permission::whereNotIn('name',Config('constants.exceptTenantPermissionArray'))->orderBy('name','ASC')->get(); 
        $permissionArray = [];
        foreach($permissions as $permission){
            $ex = explode('_',$permission->name);
            if(count($ex) && count($ex)>1){
                if(!isset($permissionArray[$ex[0]])){
                    $permissionArray[$ex[0]] = [];
                }
                $permissionArray[$ex[0]][] =  ['name'=>implode('_', array_slice($ex, 1)),'id'=>$permission->id];                
            }
        }
        $recordPermissions = $record->permissions->pluck('id')->toArray();

        if($request->is('api/*')){
            return $this->successResponse(['record'=>$record,'permissionArray'=>$permissionArray,'recordPermissions'=>$recordPermissions]);
        }else{
            return view('tenant.role.edit',compact('record','permissionArray','recordPermissions'));
        }

         
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateService($request)
    {
        $rules=[
            'id'=>'bail|required',
            'name' => 'required|string|max:255|min:2|unique:roles,name,'.$request->id,         
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $request->only('name','id','permissions');

        $record->name = $request->name;
        $record->updated_at = Carbon::now();
        $record->update();
        
        $rolePermission = $record->getAllPermissions()->pluck('name')->toArray();
        if( is_array($request->permissions) && count($request->permissions) ){
            $record->revokePermissionTo($rolePermission);
            $record->givePermissionTo([$request->permissions]);          
        }else{
            $record->revokePermissionTo($rolePermission);
        }

        if($request->is('api/*')){
            return $this->successResponse([],'Record successfully updated');
        }else{
            return redirect()->route('tenant.role',['subdomain_name'=>$request->route('subdomain_name'),$request->queryString])->withSuccess('Successfully updated...');
        }  
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatusService($request){
        $rules = [
            'id'=>'required|numeric|exists:roles',
            'status'=>['required','numeric','gte:0','lte:1'],
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['message'=>'No record found'],404);
            } 
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->successResponse([],'Status Update Successfully');
            }else{
                return response()->json(['message'=>'Status Update Successfully']);
            } 
        }
        
    }


    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function deleteService($request)
    {
        $rules = [
            'id'=>'required|numeric|exists:roles',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::whereId($request->id)->first();
       
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['error' => true,'message'=>'record not found'], 300);
            } 
        }

        $record->delete();
        if($request->is('api/*')){
            return $this->successResponse([],'Successfully deleted');
        }else{
            return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        } 
    }

}
