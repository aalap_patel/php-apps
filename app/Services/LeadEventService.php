<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Traits\ApiResponser;
use App\Traits\LeadActivity;
use App\Models\User;
use App\Models\Event;
use Carbon\Carbon;
use Response;
use Config;


class LeadEventService
{
    use ApiResponser,LeadActivity;
    protected $model;

    public function __construct(){
        $this->model = new Event;
        $this->tableName = $this->model->getTable();
    }

   /**
     * Display the listing view.
     *
     * @return \Illuminate\View\View
     */
    public function listService($request)
    {
        $records = $this->model;

        if($request->status=="0" || $request->status==1){
            $records = $records->where('status',$request->status);
        }elseif($request->status==2){
            $records = $records->onlyTrashed();
        }

        $fromStartDate = $request->from_start_date;
        $toStartDate = $request->to_start_date;

        if(!empty($fromStartDate)){
            $fromStartDate .= ' 00:00:00';
            $records = $records->whereDate('start_date','>=',$fromStartDate);
        }
        if(!empty($toStartDate)){
            $toStartDate .= ' 23:59:59';
            $records = $records->where('start_date','<=',$toStartDate);
        }

        $fromEndDate = $request->from_end_date;
        $toEndDate = $request->to_end_date;

        if(!empty($fromEndDate)){
            $fromEndDate .= ' 00:00:00';
            $records = $records->whereDate('end_date','>=',$fromEndDate);
        }
        if(!empty($toEndDate)){
            $toEndDate .= ' 23:59:59';
            $records = $records->where('end_date','<=',$toEndDate);
        }

        if(!empty($request->search_str) && ($request->search_by=='name')){
            $records = $records->where($request->search_by,'LIKE','%'.$request->search_str.'%');   
        }
        $records = $records->where('lead_id',$request->leadId)->orderBy('id','DESC')->paginate(config('app.paginate'))->withQueryString();
        $leadId = $request->leadId;  
          
             
        if($request->is('api/*')){
            return $this->successResponse(['records'=>$records,'leadId'=>$leadId]);
        }else{
            return view('tenant/event/index', compact('records','leadId'));
        }
    }

    /**
     * Display create view.
     *
     * @return \Illuminate\View\View
     */
    public function createService($request)
    {
        $leadId = $request->leadId;
        if($request->is('api/*')){
            return $this->successResponse(['leadId'=>$leadId]);
        }else{
            return view('tenant/event/add',compact('leadId'));
        }
    }

    /**
     * Handle an incoming store request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
     public function storeService($request){
        $rules = [
            'name'=> ['required','unique:event,name,null,id,lead_id,'.$request->leadId.',deleted_at,NULL'],
            'description' => 'required',
            'start_date'=> 'required',
            'end_date'=> 'required',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $request->only('name', 'description', 'start_date','end_date');

        $this->model::create([
           'name' => $request->name,
            'description' => $request->description,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'lead_id' => $request->leadId,
            'created_at' =>Carbon::now(),
            'status'=>1,
        ]);

        if($request->is('api/*')){
            return $this->successResponse([],'Event susscessfully created');
        }else{
            return redirect()->route('tenant.lead-event',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Event Successfully created.');
        }
    }

    /**
     * Display edit view for tenant.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var  \Get id for edit request
     *
     * @return \Illuminate\View\View
     */
    public function editService($request,$subdomain,$id)
    {
        $request->merge(['id'=>$id]);
        $rules = [
            'id'=>'required|numeric|exists:event',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('lead_id',$request->leadId)->where('id',$request->id)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }
        $leadId = $request->leadId;

        if($request->is('api/*')){
            return $this->successResponse(['record'=>$record,'leadId'=>$leadId]);
        }else{
            return view('tenant/event/edit',compact('record','leadId')); 
        }

         
    }

    /**
     * Handle an incoming update request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateService($request)
    {
        $rules=[
            'id'=>'required|numeric|exists:event',
            'name'=> ['required','unique:event,name,'.$request->id.',id,lead_id,'.$request->leadId.',deleted_at,NULL'],
            'description' => 'required',
            'start_date'=> 'required|date_format:Y-m-d|after_or_equal:today',
            'end_date'=> 'required|date_format:Y-m-d|after_or_equal:start_date',
            'leadId'=> 'required|numeric',      
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            if($request->is('api/*')){
                return $this->errorResponse($errormessage, 400);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors($errormessage);
            }
        }

        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 300);
            }else{
                return redirect()->back()
                        ->withInput()
                        ->withErrors('No record found');
            }
        }

        $request->only('name', 'description', 'start_date','end_date','leadId','id');

        $requestData = $request->all();
        $requestData['updated_at'] = Carbon::now();
        $record->update($requestData);
        
        if($request->is('api/*')){
            return $this->successResponse([],'Record successfully updated');
        }else{
            return redirect()->route('tenant.lead-event',['leadId'=>$request->leadId,'subdomain_name'=>$request->route('subdomain_name')])->withSuccess('Record successfully updated'); 
        }  
    }

    /**
     * Handle Change Status Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for change status request
     *
     * @return \Ajax response
     */
    public function changeStatusService($request){
        $rules = [
            'id'=>'required|numeric|exists:event',
            'status'=>['required','numeric','gte:0','lte:1'],
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::where('id',$request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['message'=>'No record found'],404);
            } 
        }  
        if($record->status == 1){
            $record->status = 0;
        }else{
           $record->status = 1;
        }   
        $record->updated_at = Carbon::now();
        $record->update();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->successResponse([],'Status Update Successfully');
            }else{
                return response()->json(['message'=>'Status Update Successfully']);
            } 
        }
        
    }


    /**
     * Handle Delete Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @var   \Get id in request for delete request
     *
     * @return \Ajax response
     */
    public function deleteService($request)
    {
        $rules = [
            'id'=>'required|numeric|exists:event',
        ];
        $messages = [];
        $validate = Validator::make($request->all(), $rules, $messages);
        if ($validate->fails()) {
            $errormessage = implode("\r\n", $validate->errors()->all());
            return $this->errorResponse($errormessage, 400);
        }

        $record = $this->model::whereId($request->id)->where('lead_id',$request->leadId)->first();
        if(empty($record)){
            if($request->is('api/*')){
                return $this->errorResponse('No record found', 400);
            }else{
                return response()->json(['error' => true,'message'=>'record not found'], 300);
            } 
        }

        $record->delete();
        if($request->is('api/*')){
            return $this->successResponse([],'Successfully deleted');
        }else{
            return response()->json(['success' => true,'message'=>'successfully deleted'], 200);
        } 
    }

}
