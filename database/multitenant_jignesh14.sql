-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2021 at 09:02 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multitenant_jignesh14`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `phone_code` int(5) NOT NULL,
  `country_code` char(2) NOT NULL,
  `country_name` varchar(80) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `phone_code`, `country_code`, `country_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 93, 'AF', 'Afghanistan', 1, NULL, NULL, NULL),
(2, 358, 'AX', 'Aland Islands', 1, NULL, NULL, NULL),
(3, 355, 'AL', 'Albania', 1, NULL, NULL, NULL),
(4, 213, 'DZ', 'Algeria', 1, NULL, NULL, NULL),
(5, 1684, 'AS', 'American Samoa', 1, NULL, NULL, NULL),
(6, 376, 'AD', 'Andorra', 1, NULL, NULL, NULL),
(7, 244, 'AO', 'Angola', 1, NULL, NULL, NULL),
(8, 1264, 'AI', 'Anguilla', 1, NULL, NULL, NULL),
(9, 672, 'AQ', 'Antarctica', 1, NULL, NULL, NULL),
(10, 1268, 'AG', 'Antigua and Barbuda', 1, NULL, NULL, NULL),
(11, 54, 'AR', 'Argentina', 1, NULL, NULL, NULL),
(12, 374, 'AM', 'Armenia', 1, NULL, NULL, NULL),
(13, 297, 'AW', 'Aruba', 1, NULL, NULL, NULL),
(14, 61, 'AU', 'Australia', 1, NULL, NULL, NULL),
(15, 43, 'AT', 'Austria', 1, NULL, NULL, NULL),
(16, 994, 'AZ', 'Azerbaijan', 1, NULL, NULL, NULL),
(17, 1242, 'BS', 'Bahamas', 1, NULL, NULL, NULL),
(18, 973, 'BH', 'Bahrain', 1, NULL, NULL, NULL),
(19, 880, 'BD', 'Bangladesh', 1, NULL, NULL, NULL),
(20, 1246, 'BB', 'Barbados', 1, NULL, NULL, NULL),
(21, 375, 'BY', 'Belarus', 1, NULL, NULL, NULL),
(22, 32, 'BE', 'Belgium', 1, NULL, NULL, NULL),
(23, 501, 'BZ', 'Belize', 1, NULL, NULL, NULL),
(24, 229, 'BJ', 'Benin', 1, NULL, NULL, NULL),
(25, 1441, 'BM', 'Bermuda', 1, NULL, NULL, NULL),
(26, 975, 'BT', 'Bhutan', 1, NULL, NULL, NULL),
(27, 591, 'BO', 'Bolivia', 1, NULL, NULL, NULL),
(28, 599, 'BQ', 'Bonaire, Sint Eustatius and Saba', 1, NULL, NULL, NULL),
(29, 387, 'BA', 'Bosnia and Herzegovina', 1, NULL, NULL, NULL),
(30, 267, 'BW', 'Botswana', 1, NULL, NULL, NULL),
(31, 55, 'BV', 'Bouvet Island', 1, NULL, NULL, NULL),
(32, 55, 'BR', 'Brazil', 1, NULL, NULL, NULL),
(33, 246, 'IO', 'British Indian Ocean Territory', 1, NULL, NULL, NULL),
(34, 673, 'BN', 'Brunei Darussalam', 1, NULL, NULL, NULL),
(35, 359, 'BG', 'Bulgaria', 1, NULL, NULL, NULL),
(36, 226, 'BF', 'Burkina Faso', 1, NULL, NULL, NULL),
(37, 257, 'BI', 'Burundi', 1, NULL, NULL, NULL),
(38, 855, 'KH', 'Cambodia', 1, NULL, NULL, NULL),
(39, 237, 'CM', 'Cameroon', 1, NULL, NULL, NULL),
(40, 1, 'CA', 'Canada', 1, NULL, NULL, NULL),
(41, 238, 'CV', 'Cape Verde', 1, NULL, NULL, NULL),
(42, 1345, 'KY', 'Cayman Islands', 1, NULL, NULL, NULL),
(43, 236, 'CF', 'Central African Republic', 1, NULL, NULL, NULL),
(44, 235, 'TD', 'Chad', 1, NULL, NULL, NULL),
(45, 56, 'CL', 'Chile', 1, NULL, NULL, NULL),
(46, 86, 'CN', 'China', 1, NULL, NULL, NULL),
(47, 61, 'CX', 'Christmas Island', 1, NULL, NULL, NULL),
(48, 672, 'CC', 'Cocos (Keeling) Islands', 1, NULL, NULL, NULL),
(49, 57, 'CO', 'Colombia', 1, NULL, NULL, NULL),
(50, 269, 'KM', 'Comoros', 1, NULL, NULL, NULL),
(51, 242, 'CG', 'Congo', 1, NULL, NULL, NULL),
(52, 242, 'CD', 'Congo, Democratic Republic of the Congo', 1, NULL, NULL, NULL),
(53, 682, 'CK', 'Cook Islands', 1, NULL, NULL, NULL),
(54, 506, 'CR', 'Costa Rica', 1, NULL, NULL, NULL),
(55, 225, 'CI', 'Cote D\'Ivoire', 1, NULL, NULL, NULL),
(56, 385, 'HR', 'Croatia', 1, NULL, NULL, NULL),
(57, 53, 'CU', 'Cuba', 1, NULL, NULL, NULL),
(58, 599, 'CW', 'Curacao', 1, NULL, NULL, NULL),
(59, 357, 'CY', 'Cyprus', 1, NULL, NULL, NULL),
(60, 420, 'CZ', 'Czech Republic', 1, NULL, NULL, NULL),
(61, 45, 'DK', 'Denmark', 1, NULL, NULL, NULL),
(62, 253, 'DJ', 'Djibouti', 1, NULL, NULL, NULL),
(63, 1767, 'DM', 'Dominica', 1, NULL, NULL, NULL),
(64, 1809, 'DO', 'Dominican Republic', 1, NULL, NULL, NULL),
(65, 593, 'EC', 'Ecuador', 1, NULL, NULL, NULL),
(66, 20, 'EG', 'Egypt', 1, NULL, NULL, NULL),
(67, 503, 'SV', 'El Salvador', 1, NULL, NULL, NULL),
(68, 240, 'GQ', 'Equatorial Guinea', 1, NULL, NULL, NULL),
(69, 291, 'ER', 'Eritrea', 1, NULL, NULL, NULL),
(70, 372, 'EE', 'Estonia', 1, NULL, NULL, NULL),
(71, 251, 'ET', 'Ethiopia', 1, NULL, NULL, NULL),
(72, 500, 'FK', 'Falkland Islands (Malvinas)', 1, NULL, NULL, NULL),
(73, 298, 'FO', 'Faroe Islands', 1, NULL, NULL, NULL),
(74, 679, 'FJ', 'Fiji', 1, NULL, NULL, NULL),
(75, 358, 'FI', 'Finland', 1, NULL, NULL, NULL),
(76, 33, 'FR', 'France', 1, NULL, NULL, NULL),
(77, 594, 'GF', 'French Guiana', 1, NULL, NULL, NULL),
(78, 689, 'PF', 'French Polynesia', 1, NULL, NULL, NULL),
(79, 262, 'TF', 'French Southern Territories', 1, NULL, NULL, NULL),
(80, 241, 'GA', 'Gabon', 1, NULL, NULL, NULL),
(81, 220, 'GM', 'Gambia', 1, NULL, NULL, NULL),
(82, 995, 'GE', 'Georgia', 1, NULL, NULL, NULL),
(83, 49, 'DE', 'Germany', 1, NULL, NULL, NULL),
(84, 233, 'GH', 'Ghana', 1, NULL, NULL, NULL),
(85, 350, 'GI', 'Gibraltar', 1, NULL, NULL, NULL),
(86, 30, 'GR', 'Greece', 1, NULL, NULL, NULL),
(87, 299, 'GL', 'Greenland', 1, NULL, NULL, NULL),
(88, 1473, 'GD', 'Grenada', 1, NULL, NULL, NULL),
(89, 590, 'GP', 'Guadeloupe', 1, NULL, NULL, NULL),
(90, 1671, 'GU', 'Guam', 1, NULL, NULL, NULL),
(91, 502, 'GT', 'Guatemala', 1, NULL, NULL, NULL),
(92, 44, 'GG', 'Guernsey', 1, NULL, NULL, NULL),
(93, 224, 'GN', 'Guinea', 1, NULL, NULL, NULL),
(94, 245, 'GW', 'Guinea-Bissau', 1, NULL, NULL, NULL),
(95, 592, 'GY', 'Guyana', 1, NULL, NULL, NULL),
(96, 509, 'HT', 'Haiti', 1, NULL, NULL, NULL),
(97, 0, 'HM', 'Heard Island and Mcdonald Islands', 1, NULL, NULL, NULL),
(98, 39, 'VA', 'Holy See (Vatican City State)', 1, NULL, NULL, NULL),
(99, 504, 'HN', 'Honduras', 1, NULL, NULL, NULL),
(100, 852, 'HK', 'Hong Kong', 1, NULL, NULL, NULL),
(101, 36, 'HU', 'Hungary', 1, NULL, NULL, NULL),
(102, 354, 'IS', 'Iceland', 1, NULL, NULL, NULL),
(103, 91, 'IN', 'India', 1, NULL, NULL, NULL),
(104, 62, 'ID', 'Indonesia', 1, NULL, NULL, NULL),
(105, 98, 'IR', 'Iran, Islamic Republic of', 1, NULL, NULL, NULL),
(106, 964, 'IQ', 'Iraq', 1, NULL, NULL, NULL),
(107, 353, 'IE', 'Ireland', 1, NULL, NULL, NULL),
(108, 44, 'IM', 'Isle of Man', 1, NULL, NULL, NULL),
(109, 972, 'IL', 'Israel', 1, NULL, NULL, NULL),
(110, 39, 'IT', 'Italy', 1, NULL, NULL, NULL),
(111, 1876, 'JM', 'Jamaica', 1, NULL, NULL, NULL),
(112, 81, 'JP', 'Japan', 1, NULL, NULL, NULL),
(113, 44, 'JE', 'Jersey', 1, NULL, NULL, NULL),
(114, 962, 'JO', 'Jordan', 1, NULL, NULL, NULL),
(115, 7, 'KZ', 'Kazakhstan', 1, NULL, NULL, NULL),
(116, 254, 'KE', 'Kenya', 1, NULL, NULL, NULL),
(117, 686, 'KI', 'Kiribati', 1, NULL, NULL, NULL),
(118, 850, 'KP', 'Korea, Democratic People\'s Republic of', 1, NULL, NULL, NULL),
(119, 82, 'KR', 'Korea, Republic of', 1, NULL, NULL, NULL),
(120, 381, 'XK', 'Kosovo', 1, NULL, NULL, NULL),
(121, 965, 'KW', 'Kuwait', 1, NULL, NULL, NULL),
(122, 996, 'KG', 'Kyrgyzstan', 1, NULL, NULL, NULL),
(123, 856, 'LA', 'Lao People\'s Democratic Republic', 1, NULL, NULL, NULL),
(124, 371, 'LV', 'Latvia', 1, NULL, NULL, NULL),
(125, 961, 'LB', 'Lebanon', 1, NULL, NULL, NULL),
(126, 266, 'LS', 'Lesotho', 1, NULL, NULL, NULL),
(127, 231, 'LR', 'Liberia', 1, NULL, NULL, NULL),
(128, 218, 'LY', 'Libyan Arab Jamahiriya', 1, NULL, NULL, NULL),
(129, 423, 'LI', 'Liechtenstein', 1, NULL, NULL, NULL),
(130, 370, 'LT', 'Lithuania', 1, NULL, NULL, NULL),
(131, 352, 'LU', 'Luxembourg', 1, NULL, NULL, NULL),
(132, 853, 'MO', 'Macao', 1, NULL, NULL, NULL),
(133, 389, 'MK', 'Macedonia, the Former Yugoslav Republic of', 1, NULL, NULL, NULL),
(134, 261, 'MG', 'Madagascar', 1, NULL, NULL, NULL),
(135, 265, 'MW', 'Malawi', 1, NULL, NULL, NULL),
(136, 60, 'MY', 'Malaysia', 1, NULL, NULL, NULL),
(137, 960, 'MV', 'Maldives', 1, NULL, NULL, NULL),
(138, 223, 'ML', 'Mali', 1, NULL, NULL, NULL),
(139, 356, 'MT', 'Malta', 1, NULL, NULL, NULL),
(140, 692, 'MH', 'Marshall Islands', 1, NULL, NULL, NULL),
(141, 596, 'MQ', 'Martinique', 1, NULL, NULL, NULL),
(142, 222, 'MR', 'Mauritania', 1, NULL, NULL, NULL),
(143, 230, 'MU', 'Mauritius', 1, NULL, NULL, NULL),
(144, 269, 'YT', 'Mayotte', 1, NULL, NULL, NULL),
(145, 52, 'MX', 'Mexico', 1, NULL, NULL, NULL),
(146, 691, 'FM', 'Micronesia, Federated States of', 1, NULL, NULL, NULL),
(147, 373, 'MD', 'Moldova, Republic of', 1, NULL, NULL, NULL),
(148, 377, 'MC', 'Monaco', 1, NULL, NULL, NULL),
(149, 976, 'MN', 'Mongolia', 1, NULL, NULL, NULL),
(150, 382, 'ME', 'Montenegro', 1, NULL, NULL, NULL),
(151, 1664, 'MS', 'Montserrat', 1, NULL, NULL, NULL),
(152, 212, 'MA', 'Morocco', 1, NULL, NULL, NULL),
(153, 258, 'MZ', 'Mozambique', 1, NULL, NULL, NULL),
(154, 95, 'MM', 'Myanmar', 1, NULL, NULL, NULL),
(155, 264, 'NA', 'Namibia', 1, NULL, NULL, NULL),
(156, 674, 'NR', 'Nauru', 1, NULL, NULL, NULL),
(157, 977, 'NP', 'Nepal', 1, NULL, NULL, NULL),
(158, 31, 'NL', 'Netherlands', 1, NULL, NULL, NULL),
(159, 599, 'AN', 'Netherlands Antilles', 1, NULL, NULL, NULL),
(160, 687, 'NC', 'New Caledonia', 1, NULL, NULL, NULL),
(161, 64, 'NZ', 'New Zealand', 1, NULL, NULL, NULL),
(162, 505, 'NI', 'Nicaragua', 1, NULL, NULL, NULL),
(163, 227, 'NE', 'Niger', 1, NULL, NULL, NULL),
(164, 234, 'NG', 'Nigeria', 1, NULL, NULL, NULL),
(165, 683, 'NU', 'Niue', 1, NULL, NULL, NULL),
(166, 672, 'NF', 'Norfolk Island', 1, NULL, NULL, NULL),
(167, 1670, 'MP', 'Northern Mariana Islands', 1, NULL, NULL, NULL),
(168, 47, 'NO', 'Norway', 1, NULL, NULL, NULL),
(169, 968, 'OM', 'Oman', 1, NULL, NULL, NULL),
(170, 92, 'PK', 'Pakistan', 1, NULL, NULL, NULL),
(171, 680, 'PW', 'Palau', 1, NULL, NULL, NULL),
(172, 970, 'PS', 'Palestinian Territory, Occupied', 1, NULL, NULL, NULL),
(173, 507, 'PA', 'Panama', 1, NULL, NULL, NULL),
(174, 675, 'PG', 'Papua New Guinea', 1, NULL, NULL, NULL),
(175, 595, 'PY', 'Paraguay', 1, NULL, NULL, NULL),
(176, 51, 'PE', 'Peru', 1, NULL, NULL, NULL),
(177, 63, 'PH', 'Philippines', 1, NULL, NULL, NULL),
(178, 64, 'PN', 'Pitcairn', 1, NULL, NULL, NULL),
(179, 48, 'PL', 'Poland', 1, NULL, NULL, NULL),
(180, 351, 'PT', 'Portugal', 1, NULL, NULL, NULL),
(181, 1787, 'PR', 'Puerto Rico', 1, NULL, NULL, NULL),
(182, 974, 'QA', 'Qatar', 1, NULL, NULL, NULL),
(183, 262, 'RE', 'Reunion', 1, NULL, NULL, NULL),
(184, 40, 'RO', 'Romania', 1, NULL, NULL, NULL),
(185, 70, 'RU', 'Russian Federation', 1, NULL, NULL, NULL),
(186, 250, 'RW', 'Rwanda', 1, NULL, NULL, NULL),
(187, 590, 'BL', 'Saint Barthelemy', 1, NULL, NULL, NULL),
(188, 290, 'SH', 'Saint Helena', 1, NULL, NULL, NULL),
(189, 1869, 'KN', 'Saint Kitts and Nevis', 1, NULL, NULL, NULL),
(190, 1758, 'LC', 'Saint Lucia', 1, NULL, NULL, NULL),
(191, 590, 'MF', 'Saint Martin', 1, NULL, NULL, NULL),
(192, 508, 'PM', 'Saint Pierre and Miquelon', 1, NULL, NULL, NULL),
(193, 1784, 'VC', 'Saint Vincent and the Grenadines', 1, NULL, NULL, NULL),
(194, 684, 'WS', 'Samoa', 1, NULL, NULL, NULL),
(195, 378, 'SM', 'San Marino', 1, NULL, NULL, NULL),
(196, 239, 'ST', 'Sao Tome and Principe', 1, NULL, NULL, NULL),
(197, 966, 'SA', 'Saudi Arabia', 1, NULL, NULL, NULL),
(198, 221, 'SN', 'Senegal', 1, NULL, NULL, NULL),
(199, 381, 'RS', 'Serbia', 1, NULL, NULL, NULL),
(200, 381, 'CS', 'Serbia and Montenegro', 1, NULL, NULL, NULL),
(201, 248, 'SC', 'Seychelles', 1, NULL, NULL, NULL),
(202, 232, 'SL', 'Sierra Leone', 1, NULL, NULL, NULL),
(203, 65, 'SG', 'Singapore', 1, NULL, NULL, NULL),
(204, 1, 'SX', 'Sint Maarten', 1, NULL, NULL, NULL),
(205, 421, 'SK', 'Slovakia', 1, NULL, NULL, NULL),
(206, 386, 'SI', 'Slovenia', 1, NULL, NULL, NULL),
(207, 677, 'SB', 'Solomon Islands', 1, NULL, NULL, NULL),
(208, 252, 'SO', 'Somalia', 1, NULL, NULL, NULL),
(209, 27, 'ZA', 'South Africa', 1, NULL, NULL, NULL),
(210, 500, 'GS', 'South Georgia and the South Sandwich Islands', 1, NULL, NULL, NULL),
(211, 211, 'SS', 'South Sudan', 1, NULL, NULL, NULL),
(212, 34, 'ES', 'Spain', 1, NULL, NULL, NULL),
(213, 94, 'LK', 'Sri Lanka', 1, NULL, NULL, NULL),
(214, 249, 'SD', 'Sudan', 1, NULL, NULL, NULL),
(215, 597, 'SR', 'Suriname', 1, NULL, NULL, NULL),
(216, 47, 'SJ', 'Svalbard and Jan Mayen', 1, NULL, NULL, NULL),
(217, 268, 'SZ', 'Swaziland', 1, NULL, NULL, NULL),
(218, 46, 'SE', 'Sweden', 1, NULL, NULL, NULL),
(219, 41, 'CH', 'Switzerland', 1, NULL, NULL, NULL),
(220, 963, 'SY', 'Syrian Arab Republic', 1, NULL, NULL, NULL),
(221, 886, 'TW', 'Taiwan, Province of China', 1, NULL, NULL, NULL),
(222, 992, 'TJ', 'Tajikistan', 1, NULL, NULL, NULL),
(223, 255, 'TZ', 'Tanzania, United Republic of', 1, NULL, NULL, NULL),
(224, 66, 'TH', 'Thailand', 1, NULL, NULL, NULL),
(225, 670, 'TL', 'Timor-Leste', 1, NULL, NULL, NULL),
(226, 228, 'TG', 'Togo', 1, NULL, NULL, NULL),
(227, 690, 'TK', 'Tokelau', 1, NULL, NULL, NULL),
(228, 676, 'TO', 'Tonga', 1, NULL, NULL, NULL),
(229, 1868, 'TT', 'Trinidad and Tobago', 1, NULL, NULL, NULL),
(230, 216, 'TN', 'Tunisia', 1, NULL, NULL, NULL),
(231, 90, 'TR', 'Turkey', 1, NULL, NULL, NULL),
(232, 7370, 'TM', 'Turkmenistan', 1, NULL, NULL, NULL),
(233, 1649, 'TC', 'Turks and Caicos Islands', 1, NULL, NULL, NULL),
(234, 688, 'TV', 'Tuvalu', 1, NULL, NULL, NULL),
(235, 256, 'UG', 'Uganda', 1, NULL, NULL, NULL),
(236, 380, 'UA', 'Ukraine', 1, NULL, NULL, NULL),
(237, 971, 'AE', 'United Arab Emirates', 1, NULL, NULL, NULL),
(238, 44, 'GB', 'United Kingdom', 1, NULL, NULL, NULL),
(239, 1, 'US', 'United States', 1, NULL, NULL, NULL),
(240, 1, 'UM', 'United States Minor Outlying Islands', 1, NULL, NULL, NULL),
(241, 598, 'UY', 'Uruguay', 1, NULL, NULL, NULL),
(242, 998, 'UZ', 'Uzbekistan', 1, NULL, NULL, NULL),
(243, 678, 'VU', 'Vanuatu', 1, NULL, NULL, NULL),
(244, 58, 'VE', 'Venezuela', 1, NULL, NULL, NULL),
(245, 84, 'VN', 'Viet Nam', 1, NULL, NULL, NULL),
(246, 1284, 'VG', 'Virgin Islands, British', 1, NULL, NULL, NULL),
(247, 1340, 'VI', 'Virgin Islands, U.s.', 1, NULL, NULL, NULL),
(248, 681, 'WF', 'Wallis and Futuna', 1, NULL, NULL, NULL),
(249, 212, 'EH', 'Western Sahara', 1, NULL, NULL, NULL),
(250, 967, 'YE', 'Yemen', 1, NULL, NULL, NULL),
(251, 260, 'ZM', 'Zambia', 1, NULL, NULL, NULL),
(252, 263, 'ZW', 'Zimbabwe', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `symbol` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Leke', 'ALL', 'Lek', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(2, 'Dollars', 'USD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(3, 'Afghanis', 'AFN', '؋', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(4, 'Pesos', 'ARS', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(5, 'Guilders', 'AWG', 'ƒ', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(6, 'Dollars', 'AUD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(7, 'New Manats', 'AZN', 'ман', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(8, 'Dollars', 'BSD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(9, 'Dollars', 'BBD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(10, 'Rubles', 'BYR', 'p.', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(11, 'Euro', 'EUR', '€', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(12, 'Dollars', 'BZD', 'BZ$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(13, 'Dollars', 'BMD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(14, 'Bolivianos', 'BOB', '$b', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(15, 'Convertible Marka', 'BAM', 'KM', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(16, 'Pula', 'BWP', 'P', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(17, 'Leva', 'BGN', 'лв', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(18, 'Reais', 'BRL', 'R$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(19, 'Pounds', 'GBP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(20, 'Dollars', 'BND', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(21, 'Riels', 'KHR', '៛', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(22, 'Dollars', 'CAD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(23, 'Dollars', 'KYD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(24, 'Pesos', 'CLP', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(25, 'Yuan Renminbi', 'CNY', '¥', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(26, 'Pesos', 'COP', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(27, 'Colón', 'CRC', '₡', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(28, 'Kuna', 'HRK', 'kn', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(29, 'Pesos', 'CUP', '₱', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(30, 'Koruny', 'CZK', 'Kč', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(31, 'Kroner', 'DKK', 'kr', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(32, 'Pesos', 'DOP ', 'RD$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(33, 'Dollars', 'XCD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(34, 'Pounds', 'EGP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(35, 'Colones', 'SVC', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(36, 'Pounds', 'FKP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(37, 'Dollars', 'FJD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(38, 'Cedis', 'GHC', '¢', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(39, 'Pounds', 'GIP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(40, 'Quetzales', 'GTQ', 'Q', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(41, 'Pounds', 'GGP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(42, 'Dollars', 'GYD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(43, 'Lempiras', 'HNL', 'L', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(44, 'Dollars', 'HKD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(45, 'Forint', 'HUF', 'Ft', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(46, 'Kronur', 'ISK', 'kr', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(47, 'Rupees', 'INR', 'Rp', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(48, 'Rupiahs', 'IDR', 'Rp', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(49, 'Rials', 'IRR', '﷼', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(50, 'Pounds', 'IMP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(51, 'New Shekels', 'ILS', '₪', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(52, 'Dollars', 'JMD', 'J$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(53, 'Yen', 'JPY', '¥', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(54, 'Pounds', 'JEP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(55, 'Tenge', 'KZT', 'лв', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(56, 'Won', 'KPW', '₩', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(57, 'Won', 'KRW', '₩', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(58, 'Soms', 'KGS', 'лв', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(59, 'Kips', 'LAK', '₭', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(60, 'Lati', 'LVL', 'Ls', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(61, 'Pounds', 'LBP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(62, 'Dollars', 'LRD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(63, 'Switzerland Francs', 'CHF', 'CHF', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(64, 'Litai', 'LTL', 'Lt', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(65, 'Denars', 'MKD', 'ден', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(66, 'Ringgits', 'MYR', 'RM', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(67, 'Rupees', 'MUR', '₨', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(68, 'Pesos', 'MXN', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(69, 'Tugriks', 'MNT', '₮', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(70, 'Meticais', 'MZN', 'MT', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(71, 'Dollars', 'NAD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(72, 'Rupees', 'NPR', '₨', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(73, 'Guilders', 'ANG', 'ƒ', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(74, 'Dollars', 'NZD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(75, 'Cordobas', 'NIO', 'C$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(76, 'Nairas', 'NGN', '₦', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(77, 'Krone', 'NOK', 'kr', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(78, 'Rials', 'OMR', '﷼', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(79, 'Rupees', 'PKR', '₨', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(80, 'Balboa', 'PAB', 'B/.', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(81, 'Guarani', 'PYG', 'Gs', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(82, 'Nuevos Soles', 'PEN', 'S/.', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(83, 'Pesos', 'PHP', 'Php', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(84, 'Zlotych', 'PLN', 'zł', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(85, 'Rials', 'QAR', '﷼', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(86, 'New Lei', 'RON', 'lei', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(87, 'Rubles', 'RUB', 'руб', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(88, 'Pounds', 'SHP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(89, 'Riyals', 'SAR', '﷼', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(90, 'Dinars', 'RSD', 'Дин.', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(91, 'Rupees', 'SCR', '₨', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(92, 'Dollars', 'SGD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(93, 'Dollars', 'SBD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(94, 'Shillings', 'SOS', 'S', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(95, 'Rand', 'ZAR', 'R', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(96, 'Rupees', 'LKR', '₨', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(97, 'Kronor', 'SEK', 'kr', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(98, 'Dollars', 'SRD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(99, 'Pounds', 'SYP', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(100, 'New Dollars', 'TWD', 'NT$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(101, 'Baht', 'THB', '฿', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(102, 'Dollars', 'TTD', 'TT$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(103, 'Lira', 'TRY', '₺', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(104, 'Liras', 'TRL', '£', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(105, 'Dollars', 'TVD', '$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(106, 'Hryvnia', 'UAH', '₴', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(107, 'Pesos', 'UYU', '$U', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(108, 'Sums', 'UZS', 'лв', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(109, 'Bolivares Fuertes', 'VEF', 'Bs', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(110, 'Dong', 'VND', '₫', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(111, 'Rials', 'YER', '﷼', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(112, 'Zimbabwe Dollars', 'ZWD', 'Z$', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL),
(113, 'Rupees', 'INR', '₹', 1, '2021-07-02 16:08:02', '2021-07-02 16:08:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deal`
--

CREATE TABLE `deal` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `deal_amount` decimal(15,2) NOT NULL,
  `deal_probability` varchar(255) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `domain_detail`
--

CREATE TABLE `domain_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `domain_url` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `facebook_url` varchar(255) DEFAULT NULL,
  `linkedin_url` varchar(255) DEFAULT NULL,
  `twitter_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `domain_detail`
--

INSERT INTO `domain_detail` (`id`, `name`, `domain_url`, `logo`, `facebook_url`, `linkedin_url`, `twitter_url`, `created_at`, `updated_at`) VALUES
(1, 'MultiTenent', 'http://test1.localhost.com/', 'test1/user/zDm6eDWywxUjK0TLK7tj3GqUTSD5IVvBnCUG9rNf.jpg', 'https://www.facebook.com', 'https://www.linkedin.com', 'https://www.twitter.com', '2021-07-07 06:24:53', '2021-07-07 06:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `lead_id`, `name`, `description`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'jfklsdjfkljklj', 'ljlkjlksdjlksjflkdsjflk', '2021-07-06 00:00:00', '2021-07-21 00:00:00', 1, '2021-07-06 11:04:31', '2021-07-06 11:38:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lead`
--

CREATE TABLE `lead` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `salutation` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `title` text NOT NULL,
  `department` varchar(255) DEFAULT NULL,
  `address1` text NOT NULL,
  `address2` text DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `phone1` varchar(15) NOT NULL,
  `phone2` varchar(15) DEFAULT NULL,
  `primary_email` varchar(150) NOT NULL,
  `secondry_email` varchar(150) DEFAULT NULL,
  `facebook_url` varchar(255) DEFAULT NULL,
  `twitter_url` varchar(255) DEFAULT NULL,
  `linkedin_url` varchar(255) DEFAULT NULL,
  `instagram_url` varchar(255) DEFAULT NULL,
  `organization_name` varchar(255) DEFAULT NULL,
  `organization_email` varchar(150) DEFAULT NULL,
  `organization_phone1` varchar(15) DEFAULT NULL,
  `organization_phone2` varchar(15) DEFAULT NULL,
  `organization_website` varchar(150) DEFAULT NULL,
  `is_contact` tinyint(1) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lead`
--

INSERT INTO `lead` (`id`, `user_id`, `salutation`, `first_name`, `last_name`, `profile_pic`, `title`, `department`, `address1`, `address2`, `city`, `state`, `country_id`, `zipcode`, `phone1`, `phone2`, `primary_email`, `secondry_email`, `facebook_url`, `twitter_url`, `linkedin_url`, `instagram_url`, `organization_name`, `organization_email`, `organization_phone1`, `organization_phone2`, `organization_website`, `is_contact`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'mr', 'jignesh', 'dodiya', 'test1/lead/MIUi10cetawLG637okPB2RXCWsA2gVpEzXqfwZ3C.png', 'title', 'sales', 'Address', 'Address  2', 'Ahmedabad', 'Gujarat', 103, '380058', '9979006185', '5698523665', 'jigneshdodiya@sjainfosolutions.com', 'jigneshdodiya15@yahoo.com', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.linkedin.com', 'https://www.instagram.com', 'Organization Name', 'organization@email.com', '5621458796', '5463258964', 'https://www.google.com', 1, 1, '2021-07-05 07:13:48', '2021-07-05 11:19:46', NULL),
(2, 1, 'dearMr', 'jlkj', 'jlkj', 'test1/lead/keeVBuJrgfKFA3DElumJD8SV9nIUZuEASY2Ui6Ec.jpg', 'lkjlk', 'kljlkh', 'hkjhkj', 'hkjhkj', 'hkjh', 'kjhkj', 2, 'hkjhkjh', '5698512545', '5698521456', 'jigneshdodiya@sdsdsa.com', 'jigneshdodiya@sdsdsa.com', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.linkedin.com', 'https://www.instagram.com', 'jhghjgjgjgh', 'gjhgjgjgjgj@gmail.com', '5874569632', '5871236598', 'https://www.google.com', NULL, 0, '2021-07-05 11:37:35', '2021-07-05 11:37:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 19),
(2, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 19),
(3, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 19),
(4, 'App\\Models\\User', 1),
(4, 'App\\Models\\User', 19),
(5, 'App\\Models\\User', 1),
(5, 'App\\Models\\User', 19),
(6, 'App\\Models\\User', 1),
(7, 'App\\Models\\User', 1),
(8, 'App\\Models\\User', 1),
(9, 'App\\Models\\User', 1),
(10, 'App\\Models\\User', 1),
(11, 'App\\Models\\User', 1),
(11, 'App\\Models\\User', 17),
(11, 'App\\Models\\User', 18),
(11, 'App\\Models\\User', 19),
(12, 'App\\Models\\User', 1),
(12, 'App\\Models\\User', 17),
(12, 'App\\Models\\User', 18),
(12, 'App\\Models\\User', 19),
(13, 'App\\Models\\User', 1),
(13, 'App\\Models\\User', 17),
(13, 'App\\Models\\User', 18),
(13, 'App\\Models\\User', 19),
(14, 'App\\Models\\User', 1),
(14, 'App\\Models\\User', 17),
(14, 'App\\Models\\User', 18),
(14, 'App\\Models\\User', 19),
(15, 'App\\Models\\User', 1),
(15, 'App\\Models\\User', 17),
(15, 'App\\Models\\User', 18),
(15, 'App\\Models\\User', 19),
(16, 'App\\Models\\User', 1),
(17, 'App\\Models\\User', 1),
(18, 'App\\Models\\User', 1),
(19, 'App\\Models\\User', 1),
(20, 'App\\Models\\User', 1),
(21, 'App\\Models\\User', 19);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 3),
(6, 'App\\Models\\User', 17),
(6, 'App\\Models\\User', 18),
(6, 'App\\Models\\User', 19);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `deal_id` int(11) NOT NULL,
  `notes` text NOT NULL,
  `blob_azure_url` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel-Multi-Tenant Personal Access Client', 'vot4rymzltDuPBbZ2xMAxkY8ieQgYqWSX4R0XZz8', NULL, 'http://localhost', 1, 0, 0, '2021-06-22 06:44:17', '2021-06-22 06:44:17'),
(2, NULL, 'Laravel-Multi-Tenant Password Grant Client', '39ogyidQmR4JvgJ5LrW50tguGgjJTsBATmzmBMgn', 'users', 'http://localhost', 0, 1, 0, '2021-06-22 06:44:17', '2021-06-22 06:44:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-06-22 06:44:17', '2021-06-22 06:44:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jigneshdodiya14@yahoo.com', '$2y$10$GXuuYyoeZ15HCjo2ZzNZQeRICJ3zuu0W1wrHyKbRgS6peCVW0Nx4q', '2021-06-18 12:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'user_list', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(2, 'user_create', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(3, 'user_edit', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(4, 'user_view', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(5, 'user_delete', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(6, 'tenant_list', 'web', '2021-06-16 06:02:47', '2021-06-16 06:02:47'),
(7, 'tenant_create', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(8, 'tenant_edit', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(9, 'tenant_view', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(10, 'tenant_delete', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(11, 'role_list', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(12, 'role_create', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(13, 'role_edit', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(14, 'role_view', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(15, 'role_delete', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(16, 'permission_list', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(17, 'permission_create', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(18, 'permission_edit', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(19, 'permission_view', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(20, 'permission_delete', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(21, 'user_see', 'web', '2021-06-22 01:00:12', '2021-06-22 01:14:06');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `user_id`, `name`, `description`, `product_code`, `price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'producyt', 'sadasdasdsa', 'hkjds86876hjhkj', '256.00', 1, '2021-07-04 09:53:15', '2021-07-04 10:43:36', NULL),
(2, 1, 'Product', 'sfsafdsfsdfsdfdsf', 'JHGJ456', '895.00', 0, '2021-07-04 10:04:07', '2021-07-04 11:06:29', NULL),
(3, 19, 'sdfds', 'sdfds', 'fsdf', '43543.00', 1, '2021-07-04 10:43:29', '2021-07-04 10:43:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_interested_dir`
--

CREATE TABLE `products_interested_dir` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Admin', 'web', 1, '2021-06-15 06:25:44', '2021-06-16 01:59:55', NULL),
(2, 'Admin', 'web', 1, '2021-06-16 02:00:14', '2021-06-16 02:00:14', NULL),
(3, 'sales Managers', 'web', 1, '2021-06-16 02:00:44', '2021-06-22 00:41:09', NULL),
(4, 'jjk', 'web', 1, '2021-06-16 02:02:20', '2021-06-16 02:02:28', '2021-06-16 07:32:28'),
(5, 'Stock Manager', 'web', 1, '2021-06-22 00:42:48', '2021-06-22 00:42:48', NULL),
(6, 'Branch Manager', 'web', 1, '2021-06-29 00:10:35', '2021-06-29 00:10:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(11, 6),
(12, 6),
(13, 6),
(14, 6),
(15, 6);

-- --------------------------------------------------------

--
-- Table structure for table `stages`
--

CREATE TABLE `stages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hierarchy` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `lead_id`, `title`, `description`, `due_date`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Title Title', 'Description Description', '2007-06-20 00:00:00', 1, '2021-07-06 05:37:36', '2021-07-06 06:12:43', NULL),
(2, 1, 'sdfsdfds', 'fdsfdsf', '2021-07-16 00:00:00', 1, '2021-07-06 06:13:38', '2021-07-06 06:39:39', '2021-07-06 06:39:39'),
(3, 1, 'fdsfdgfh', 'gdfg kfhkjsdhfjhskd fhkjh fkjds fkfkkdsfkdfhflf ds f fjd fdj fsdf dsf f', '2021-07-23 00:00:00', 1, '2021-07-06 06:13:55', '2021-07-06 06:14:14', NULL),
(4, 2, 'sdasdasdasdas', 'dasdasdasdasd', '2021-07-20 00:00:00', 1, '2021-07-06 07:49:16', '2021-07-06 07:49:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `secondry_email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `zipcode` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `preferred_timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_date_format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `secondry_email`, `profile_image`, `address1`, `address2`, `city`, `state`, `country_id`, `zipcode`, `phone1`, `phone2`, `password`, `is_admin`, `preferred_timezone`, `preferred_currency`, `preferred_date_format`, `remember_token`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'jignesh dodiya', 'jigneshdodiya14@yahoo.com', '2021-06-15 01:15:53', 'jigneshdodiya15@yahoo.com', 'test1/user/0fqUu6PK3nAFcgX8mubHL2QoOPai4cepXfXxeqoI.jpg', 'Ahmedabad', 'Ahmedabad', 'Ahmedabad', 'Gujarat', 103, '380058', '9979006185', '5698523665', '$2y$10$k2ETGBonihGzbXpyT0CR9eansVrUp/sUjIWOFTUbI7fC2E58AnHra', 1, 'America/Los_Angeles', '18', 'ydm', NULL, 1, '2021-06-15 01:15:53', '2021-07-03 04:57:22', NULL),
(6, 'jignesh dodiya', 'jigneshdodiya10@gmail.com', '2021-06-18 00:51:13', '', '', '', '', '', '', 0, '', '', '', '$2y$10$5CPgD6W6jOHPCkB9HcqzqeJgUG8kuV7l0E7rWomCCEmZySX0iBVfm', NULL, '', '', '', 'ZvhTtEESleVuyGZQfIdT45jsqsDeRDb8udA7GlhUZdM53FUUcFu62hFfE6CQ', 1, '2021-06-17 22:45:12', '2021-06-22 00:45:56', NULL),
(7, 'jignesh dodiya', 'jigneshdodiya@sjainfosolutions.com', NULL, '', '', '', '', '', '', 0, '', '', '', '$2y$10$GAeIdq4G5zxL8fsK/oGg0et8Yyg..feF5cFJecUMBpekRx66CVOnW', NULL, '', '', '', NULL, 1, '2021-06-23 04:35:07', '2021-06-23 04:35:07', NULL),
(8, 'jignesh dodiya', 'jigneshdodiyaq@sjainfosolutions.com', NULL, '', '', '', '', '', '', 0, '', '', '', '$2y$10$ZOA3JrpzkdEErEUrrs.rTOlXeii0ppFhfjKD0iVIUEnE8h1/SZ02.', NULL, '', '', '', NULL, 1, '2021-06-23 04:51:04', '2021-06-23 04:51:04', NULL),
(9, 'jignesh dodiya', 'jigneshdodiyaa@sjainfosolutions.com', NULL, '', '', '', '', '', '', 0, '', '', '', '$2y$10$TNnfnvtx45Zh7VYOaxIw4ulj7Ej58UmQDU15RKkgz1UUG4WfTxZqm', NULL, '', '', '', NULL, 1, '2021-06-23 04:53:57', '2021-06-23 04:53:57', NULL),
(10, 'jignesh dodiya', 'jigneshdodiyga@sjainfosolutions.com', NULL, '', '', '', '', '', '', 0, '', '', '', '$2y$10$obv44mpFn.ro5IxapAhZwe/8ar9dMQag8cC24scPLX8VC5aqCXuPy', NULL, '', '', '', NULL, 1, '2021-06-23 05:05:46', '2021-06-23 05:05:46', NULL),
(11, 'jignesh dodiya', 'jigneshdodiyf@sjainfosolutions.com', NULL, '', '', '', '', '', '', 0, '', '', '', '$2y$10$qNx1Qj5C.SK64oGE4xUD/unR2PH/JVFXQg9lI.UJoDYiPHk/zb126', NULL, '', '', '', NULL, 1, '2021-06-23 05:07:52', '2021-06-23 05:07:52', NULL),
(16, 'jignesh dodiya', 'jigneshdodiya85@gmail.com', '2021-06-24 01:24:08', '', '', '', '', '', '', 0, '', '', '', '$2y$10$lKtQr2YgrkQ1WiEnxrctG.qeqX/YPcePtfFL3ZXSBpcnO3nucTT4K', NULL, '', '', '', 'Rf3rqh3cNT3HFry2Tl33UjnwMJDJjEfNpQ1cTR4s2xSW3ftlf8cy6UKCIyAK', 1, '2021-06-24 00:59:07', '2021-06-24 02:36:26', NULL),
(17, 'jignesh dodiya', 'jigneshdodiya@gmail.com', '2021-06-29 00:27:46', '', '', '', '', '', '', 0, '', '', '', '$2y$10$hxPvS9LLblYUtMnk8hKKlOXcxxyZHTSqzySIw958y2wRsdSrv1tAW', NULL, '', '', '', NULL, 0, '2021-06-29 00:27:46', '2021-06-29 00:27:46', NULL),
(18, 'jignesh dodiya', 'jigneshdodiya15@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$32p4FPM.k3sQceMipjHJGO9UZ140yYnWmyQa4O4vd0zn2iHYVVgf2', NULL, NULL, NULL, NULL, NULL, 0, '2021-07-03 00:59:28', '2021-07-03 00:59:28', NULL),
(19, 'jignesh dodiya', 'jigneshdodiya16@yahoo.com', '2021-07-03 04:14:02', 'jigneshdodiya16@yahoo.com', 'test1/user/7MSl5jQkeWDFYZDPzFZATUTM8xTlOLwW5OhzEgK8.jpg', 'Address', 'Address', 'Ahmedabad', 'Gujarat', 103, '380058', '9979006185', '5698523665', '$2y$10$oZj1uuCQ9rkBQdOshFzcWekxMlnA7nduGmTEXvTFAEEoK0DLKastS', NULL, 'America/Atka', '4', 'ydmhis', NULL, 1, '2021-07-03 01:37:10', '2021-07-03 04:14:02', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deal`
--
ALTER TABLE `deal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domain_detail`
--
ALTER TABLE `domain_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `lead`
--
ALTER TABLE `lead`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `deal`
--
ALTER TABLE `deal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domain_detail`
--
ALTER TABLE `domain_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead`
--
ALTER TABLE `lead`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stages`
--
ALTER TABLE `stages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
