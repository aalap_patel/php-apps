-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2021 at 09:01 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multitenant_landlord`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_05_29_074137_create_landlord_tenants_table', 1),
(2, '2014_10_12_000000_create_users_table', 2),
(3, '2014_10_12_100000_create_password_resets_table', 2),
(4, '2014_10_12_200000_add_two_factor_columns_to_users_table', 2),
(5, '2019_08_19_000000_create_failed_jobs_table', 2),
(6, '2019_12_14_000001_create_personal_access_tokens_table', 2),
(7, '2021_05_31_050437_create_sessions_table', 2),
(8, '2021_06_15_095833_create_permission_tables', 3),
(9, '2016_06_01_000001_create_oauth_auth_codes_table', 4),
(10, '2016_06_01_000002_create_oauth_access_tokens_table', 4),
(11, '2016_06_01_000003_create_oauth_refresh_tokens_table', 4),
(12, '2016_06_01_000004_create_oauth_clients_table', 4),
(13, '2016_06_01_000005_create_oauth_personal_access_clients_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 3),
(2, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 3),
(3, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 1),
(4, 'App\\Models\\User', 3),
(5, 'App\\Models\\User', 1),
(5, 'App\\Models\\User', 3),
(6, 'App\\Models\\User', 1),
(6, 'App\\Models\\User', 3),
(7, 'App\\Models\\User', 1),
(7, 'App\\Models\\User', 3),
(8, 'App\\Models\\User', 1),
(8, 'App\\Models\\User', 3),
(9, 'App\\Models\\User', 1),
(9, 'App\\Models\\User', 3),
(10, 'App\\Models\\User', 1),
(10, 'App\\Models\\User', 3),
(11, 'App\\Models\\User', 1),
(11, 'App\\Models\\User', 3),
(12, 'App\\Models\\User', 1),
(12, 'App\\Models\\User', 3),
(13, 'App\\Models\\User', 1),
(13, 'App\\Models\\User', 3),
(14, 'App\\Models\\User', 1),
(14, 'App\\Models\\User', 3),
(15, 'App\\Models\\User', 1),
(15, 'App\\Models\\User', 3),
(16, 'App\\Models\\User', 1),
(16, 'App\\Models\\User', 3),
(17, 'App\\Models\\User', 1),
(17, 'App\\Models\\User', 3),
(18, 'App\\Models\\User', 1),
(18, 'App\\Models\\User', 3),
(19, 'App\\Models\\User', 1),
(19, 'App\\Models\\User', 3),
(20, 'App\\Models\\User', 1),
(20, 'App\\Models\\User', 3),
(27, 'App\\Models\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 3),
(5, 'App\\Models\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('30625c302d7f8b09a82bf0f5b7b2d694f67e4bcf20b7498db23f5ab44e669950a6b323a602ec0018', 12, 3, 'authToken', '[]', 0, '2021-06-24 00:45:51', '2021-06-24 00:45:51', '2021-12-24 06:15:03'),
('a86c8ef15b2de0ba98f8390d76654cb761b3cdc0d9d667ded078850c562676d0f64c649b27700f82', 11, 3, 'authToken', '[]', 0, '2021-06-23 05:07:52', '2021-06-23 05:07:52', '2021-12-23 10:37:52'),
('d1383265ed867f41dddc195d4e6bac6e40a484e37cefc365ae54389a5fc6e8e3cf462a81c83d2bcd', 7, 3, 'authToken', '[]', 0, '2021-06-23 23:32:35', '2021-06-23 23:32:35', '2021-12-24 05:02:22'),
('f878501b34641abd5edc8aaef3f19ea9ff616a80c939f0bf088226ffe1b3cf3ee0eb92cd2bf41bb6', 10, 3, 'authToken', '[]', 0, '2021-06-23 05:05:53', '2021-06-23 05:05:53', '2021-12-23 10:35:49');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel-Multi-Tenant Personal Access Client', 'vot4rymzltDuPBbZ2xMAxkY8ieQgYqWSX4R0XZz8', NULL, 'http://localhost', 1, 0, 0, '2021-06-22 06:44:17', '2021-06-22 06:44:17'),
(2, NULL, 'Laravel-Multi-Tenant Password Grant Client', '39ogyidQmR4JvgJ5LrW50tguGgjJTsBATmzmBMgn', 'users', 'http://localhost', 0, 1, 0, '2021-06-22 06:44:17', '2021-06-22 06:44:17'),
(3, NULL, 'Laravel-Multi-Tenant Personal Access Client', 'R7pPhwsxW3iPxyFMNd2CnWgMxhhFO2twTgktEAj1', NULL, 'http://localhost', 1, 0, 0, '2021-06-23 05:04:25', '2021-06-23 05:04:25'),
(4, NULL, 'Laravel-Multi-Tenant Password Grant Client', 'KCMrpIChL7odvHGpGtRlrrlV5FmkCC9qumXoLPNI', 'users', 'http://localhost', 0, 1, 0, '2021-06-23 05:04:26', '2021-06-23 05:04:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-06-22 06:44:17', '2021-06-22 06:44:17'),
(2, 3, '2021-06-23 05:04:26', '2021-06-23 05:04:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'user_list', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(2, 'user_create', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(3, 'user_edit', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(4, 'user_view', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(5, 'user_delete', 'web', '2021-06-16 06:02:00', '2021-06-16 06:02:00'),
(6, 'tenant_list', 'web', '2021-06-16 06:02:47', '2021-06-16 06:02:47'),
(7, 'tenant_create', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(8, 'tenant_edit', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(9, 'tenant_view', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(10, 'tenant_delete', 'web', '2021-06-16 06:02:48', '2021-06-16 06:02:48'),
(11, 'role_list', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(12, 'role_create', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(13, 'role_edit', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(14, 'role_view', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(15, 'role_delete', 'web', '2021-06-16 06:03:26', '2021-06-16 06:03:26'),
(16, 'permission_list', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(17, 'permission_create', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(18, 'permission_edit', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(19, 'permission_view', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(20, 'permission_delete', 'web', '2021-06-16 06:03:59', '2021-06-16 06:03:59'),
(27, 'user_add', 'web', '2021-06-25 01:52:49', '2021-06-25 01:52:49');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Admin', 'web', 1, '2021-06-15 06:25:44', '2021-06-26 23:21:27', NULL),
(2, 'Admin', 'web', 1, '2021-06-16 02:00:14', '2021-06-26 23:21:51', NULL),
(3, 'sales Manager', 'web', 1, '2021-06-16 02:00:44', '2021-06-26 23:22:22', NULL),
(4, 'jjk', 'web', 1, '2021-06-16 02:02:20', '2021-06-16 02:02:28', '2021-06-16 07:32:28'),
(5, 'jignesh', 'web', 1, '2021-06-25 02:28:30', '2021-06-26 23:22:55', NULL),
(6, 'manager', 'web', 1, '2021-06-28 07:00:18', '2021-06-28 07:00:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 4),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(7, 6),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(11, 5),
(12, 1),
(12, 2),
(12, 5),
(12, 6),
(13, 1),
(13, 2),
(13, 5),
(14, 1),
(14, 2),
(14, 5),
(15, 1),
(15, 2),
(15, 5),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(27, 1),
(27, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('PalEAxu4JlrfHER0di19Lib2Uwgqy9PC1ghC7Gxu', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiYUFqdDFybnNaVnhDc1d4ZWZZd2R3Z0NnMTR5R3J5RHN5a3BkZTNKVyI7czozNzoiZW5zdXJlX3ZhbGlkX3RlbmFudF9zZXNzaW9uX3RlbmFudF9pZCI7aToyO3M6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjQzOiJodHRwOi8vbG9jYWxob3N0L211bHRpLXRlbmVudC1zcGF0aWUvcHVibGljIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1622442479);

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `database` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `database_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `user_id`, `name`, `domain`, `database`, `status`, `database_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'jignesh dodiya', 'test1.localhost.com', 'multitenant_jignesh14', 1, 1, '2021-06-15 01:15:04', '2021-06-16 23:59:58', NULL),
(4, 6, 'Tenant Two', 'test2.localhost.com', 'multitenant_6_CDzXJ', 1, 1, '2021-06-28 02:05:57', '2021-06-28 03:22:26', NULL),
(5, 7, 'jignesh dodiya', 'sjainfosolutions', 'multitenant_7_UDZdB', 1, 0, '2021-06-28 07:14:12', NULL, NULL),
(6, 8, 'jignesh dodiya', 'infosolutions.multi-tenent-spatie.com', 'multitenant_8_UDCpZ', 1, 0, '2021-07-06 23:22:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `is_admin`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'jignesh dodiya', 'jigneshdodiya10@gmail.com', 1, '2021-06-15 01:03:49', '$2y$10$OT39cFe0daqVLzPpvSXh7eKfzipwRBPqqmYmQZ9CKKnV3rKf6sNBC', NULL, NULL, '0uVIm1u63Mk6Pi0KW9V7XfE2ecsfTzMWzfEOOkSnANcZmxvuibQ0hzAWfHPj', NULL, NULL, 1, '2021-06-15 01:02:44', '2021-06-29 00:00:59', NULL),
(2, 'jignesh dodiya', 'jigneshdodiya14@yahoo.com', NULL, '2021-06-15 01:15:04', '$2y$10$k2ETGBonihGzbXpyT0CR9eansVrUp/sUjIWOFTUbI7fC2E58AnHra', NULL, NULL, NULL, NULL, NULL, 1, '2021-06-15 01:15:04', '2021-06-15 01:15:04', NULL),
(3, 'jignesh dodiya', 'jigneshdodiya110@gmail.com', 1, '2021-06-16 03:59:54', '$2y$10$JfkjdTXlEzDx9SWb1TKNcephJBRw./CQvFJ81Dt3dv/iIaz0F/Sd2', NULL, NULL, NULL, NULL, NULL, 1, '2021-06-16 03:59:54', '2021-06-16 06:37:44', NULL),
(6, 'Tenant Two', 'jigneshdodiya85@gmail.com', NULL, '2021-06-28 02:05:57', '$2y$10$oHaTXDJaR.qVI5g0KSkENOX9Vxd3QcEKqxjNknlvKLHY/iLVS0Gb6', NULL, NULL, NULL, NULL, NULL, 1, '2021-06-28 02:05:57', '2021-06-28 02:05:57', NULL),
(7, 'jignesh dodiya', 'jigneshdodiya@sjainfosolutions.com', NULL, NULL, '$2y$10$x.q9sxPAyI5sMJgpC0Y69.kbDfY1NDRN.9BhYq7OJPYUOmU/78BQO', NULL, NULL, '42r3q6IwuLEcbJJMOvQy0ir8jltKASRPsBia7dFqEuvr88gMtRi3Mq4AMOVX', NULL, NULL, 1, '2021-06-28 07:14:12', '2021-06-28 23:08:04', NULL),
(8, 'jignesh dodiya', 'siraxef993@noobf.com', 1, '2021-07-06 23:24:38', '$2y$10$vYzCtiI69v1WM7LLfkuIbu6ZxoMcZaf8ek32ieRWr8iYI2zpMy/Zy', NULL, NULL, 'DiFzts3DcwPDZGMiD5PoDBTPPoAXoYeDfoHLVgAEUu0ovt375IO98uNEaFS0', NULL, NULL, 1, '2021-07-06 23:22:30', '2021-07-06 23:26:32', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tenants_domain_unique` (`domain`),
  ADD UNIQUE KEY `tenants_database_unique` (`database`),
  ADD KEY `fk_user_id_tenant_user_id` (`user_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tenants`
--
ALTER TABLE `tenants`
  ADD CONSTRAINT `fk_user_tenant` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
